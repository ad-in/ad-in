class CreateProductAttribute < ActiveRecord::Migration
  def change
    
    execute <<-SQL

    CREATE TABLE product_attributes (
   product_id integer REFERENCES prod_hier(upc),
   custom_attr_id integer REFERENCES custom_attributes(id),
   value text,
   PRIMARY KEY (product_id, custom_attr_id)
           )
    SQL

  end
end
