class CreateCustomAttributes < ActiveRecord::Migration
  def change
    create_table :custom_attributes do |t|
      t.string :name
      t.string :creator
      t.text :description
      t.string :value

      t.timestamps
    end
  end
end
