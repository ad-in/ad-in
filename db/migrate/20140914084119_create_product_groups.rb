class CreateProductGroups < ActiveRecord::Migration
  def change
    create_table :product_groups do |t|
      t.text :name
      t.text :description
      t.json :product_trail
      t.json :product_attribute
      t.text :created_by
      t.text :updated_by

      t.timestamps
    end
  end
end
