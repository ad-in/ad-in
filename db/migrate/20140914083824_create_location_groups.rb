class CreateLocationGroups < ActiveRecord::Migration
  def change
    create_table :location_groups do |t|
      t.text :name
      t.text :description
      t.json :location_trail
      t.json :location_attribute
      t.text :created_by
      t.text :updated_by

      t.timestamps
    end
  end
end
