class CreatePriceGrids < ActiveRecord::Migration
  def change
    execute <<-SQL

    CREATE TABLE price_grids (
    id serial NOT NULL,
    name character varying(255),
    description text,
    price_vehicle_id integer REFERENCES price_vehicle(id),
    price_rule_id integer REFERENCES price_rules(id),
    from_price integer,
    to_price integer,
    price_candidates character varying,
    multiples character varying,
    created_by character varying(255),
    updated_by character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    PRIMARY KEY (id)
           )
    SQL
  end
end
