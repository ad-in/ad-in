class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :username, :null=> false
      t.string :first_name, :null => false
      t.string :last_name, :null => false
      t.string :email, :null => false
      t.integer :parent_id
      t.string :department
      t.integer :role
      t.timestamp :last_login_time
      t.timestamp :last_password_change_time
      t.string :password_digest
      t.string :salt
      t.string :password_reset_token
      t.boolean :status, :default => true
      t.json :user_preference

      t.timestamps
    end
  end
end
