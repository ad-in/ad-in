class CreatePriceRules < ActiveRecord::Migration

  def change
   execute <<-SQL

    CREATE TABLE price_rules (
    id serial NOT NULL,
    name character varying(255),
    description text,
    marketing_vehicle_id integer REFERENCES marketing_vehicle(id),
    category_id integer REFERENCES categories_hier(id),
    banner_id integer REFERENCES location_hier(id),
    min_margin integer,
    max_margin integer,
    min_price_change integer,
    max_price_change integer,
    min_passthrough integer,
    max_passthrough integer,
    optimization_goal character varying(255),
    PRIMARY KEY (id)
           )
    SQL
    
  end
end
