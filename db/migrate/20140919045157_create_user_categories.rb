class CreateUserCategories < ActiveRecord::Migration
  def change
    create_table :user_categories do |t|
      t.integer :user_id
      t.integer :categories_hier_id
      t.integer :parent_id
      t.integer :depth
      t.json :user_category_trail

      t.timestamps
    end
  end
end
