class CreateUserLocations < ActiveRecord::Migration
  def change
    create_table :user_locations do |t|
      t.integer :user_id, :null=> false
      t.integer :location_hier_id, :null => false
      t.integer :parent_id
      t.integer :depth
      t.json :user_location_trail

      t.timestamps
    end
  end
end
