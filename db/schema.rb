# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140918135926) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "ad_in_properties", id: false, force: true do |t|
    t.integer "id",    null: false
    t.text    "name"
    t.text    "key"
    t.text    "value"
  end

  create_table "base_cost_product_store", force: true do |t|
    t.integer "sku",                   limit: 8, null: false
    t.integer "store_id",              limit: 8, null: false
    t.float   "base_cost"
    t.text    "effective_from_dt",               null: false
    t.text    "effective_to_dt"
    t.date    "effective_from_dt_sas"
    t.date    "effective_to_dt_sas"
  end

  create_table "base_price_product_store", force: true do |t|
    t.integer "sku",                   limit: 8
    t.integer "store_id",              limit: 8
    t.float   "base_price"
    t.text    "effective_from_dt"
    t.text    "effective_to_dt"
    t.date    "effective_from_dt_sas"
    t.date    "effective_to_dt_sas"
  end

  create_table "categories_hier", force: true do |t|
    t.string   "category_name"
    t.text     "description"
    t.integer  "parent_id"
    t.integer  "depth"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "category_trail", array: true
  end

  create_table "contract_attributes", id: false, force: true do |t|
    t.integer "id",                       null: false
    t.integer "contract_id"
    t.integer "attribute_id"
    t.text    "value"
    t.integer "product_id",   limit: 8
    t.string  "created_by",   limit: nil
    t.string  "updated_by",   limit: nil
    t.time    "created_at"
    t.time    "updated_at"
  end

  create_table "contract_product_store_revenues", id: false, force: true do |t|
    t.integer "id",                                                   null: false
    t.integer "trial_id"
    t.integer "product_id",        limit: 8
    t.integer "store_id",          limit: 8
    t.integer "version_id"
    t.integer "marketing_vehicle"
    t.decimal "base_cost",                   precision: 19, scale: 2
    t.decimal "base_price",                  precision: 19, scale: 2
    t.decimal "margin",                      precision: 19, scale: 2
    t.decimal "vendor_funding",              precision: 19, scale: 2
    t.decimal "subsidy_funding",             precision: 19, scale: 2
  end

  create_table "contracts", primary_key: "trial_id", force: true do |t|
    t.integer   "sequence"
    t.string    "name"
    t.text      "desc"
    t.daterange "contract_window"
    t.integer   "duration"
    t.daterange "promotion_window"
    t.daterange "sale_window"
    t.string    "vendor_name"
    t.string    "workflow_state"
    t.integer   "contract_id",        null: false
    t.integer   "promotion_duration"
    t.integer   "store_ids",                       array: true
  end

  add_index "contracts", ["contract_id", "sequence"], name: "contract_contract_id_sequence_key", unique: true, using: :btree

  create_table "core_categories", force: true do |t|
    t.string   "categoryName"
    t.text     "description"
    t.integer  "parent_id"
    t.integer  "lft"
    t.integer  "rgt"
    t.integer  "depth"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "custom_attributes", force: true do |t|
    t.string   "name"
    t.string   "creator"
    t.text     "description"
    t.text     "value"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "attribute_type", limit: nil
  end

  create_table "espinita_audits", force: true do |t|
    t.integer  "auditable_id"
    t.string   "auditable_type"
    t.integer  "user_id"
    t.string   "user_type"
    t.text     "audited_changes"
    t.string   "comment"
    t.integer  "version"
    t.string   "action"
    t.string   "remote_address"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "espinita_audits", ["auditable_id", "auditable_type"], name: "index_espinita_audits_on_auditable_id_and_auditable_type", using: :btree
  add_index "espinita_audits", ["user_id", "user_type"], name: "index_espinita_audits_on_user_id_and_user_type", using: :btree

  create_table "event_managements", force: true do |t|
    t.text     "name"
    t.text     "description"
    t.date     "start_date"
    t.date     "end_date"
    t.text     "created_by"
    t.json     "product_scope_trail"
    t.json     "location_scope_trail"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "updated_by"
  end

  create_table "geo_prod_listing", id: false, force: true do |t|
    t.text "STORE"
    t.text "SKU"
  end

  create_table "hierarchy_descr", id: false, force: true do |t|
    t.text "level"
    t.text "value"
    t.text "description"
  end

  create_table "location_attributes", id: false, force: true do |t|
    t.integer "location_id",    null: false
    t.integer "custom_attr_id", null: false
    t.text    "value"
  end

  create_table "location_groups", force: true do |t|
    t.text     "name"
    t.text     "description"
    t.json     "location_trail"
    t.json     "location_attribute"
    t.text     "created_by"
    t.text     "updated_by"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "location_hier", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "parent_id"
    t.integer  "depth"
    t.json     "location_trail"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "marketing_vehicle", force: true do |t|
    t.string   "marketing_vehicle_name"
    t.text     "description"
    t.string   "marketing_vehicle_group"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "model_marketing_group",   limit: nil
  end

  create_table "notes", force: true do |t|
    t.integer  "user_id"
    t.integer  "contract_id"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "price_grids", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "price_vehicle_id"
    t.integer  "price_rule_id"
    t.integer  "from_price"
    t.integer  "to_price"
    t.string   "created_by"
    t.string   "updated_by"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.json     "location_trail"
    t.json     "category_trail"
    t.string   "multiples",        limit: nil
    t.string   "price_candidates", limit: nil
  end

  create_table "price_rules", force: true do |t|
    t.string  "name"
    t.text    "description"
    t.integer "marketing_vehicle_id"
    t.integer "banner_id"
    t.integer "min_margin"
    t.integer "max_margin"
    t.integer "min_price_change"
    t.integer "max_price_change"
    t.integer "min_passthrough"
    t.integer "max_passthrough"
    t.string  "optimization_goal"
    t.string  "created_by",           limit: nil
    t.string  "updated_by",           limit: nil
    t.integer "category_id",          limit: 8
  end

  create_table "price_vehicle", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "created_by"
    t.string   "updated_by"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "product_attributes", id: false, force: true do |t|
    t.integer "product_id",               null: false
    t.integer "custom_attr_id",           null: false
    t.text    "value"
    t.integer "category_id",    limit: 8
  end

  create_table "product_groups", force: true do |t|
    t.text     "name"
    t.text     "description"
    t.json     "product_trail"
    t.json     "product_attribute"
    t.text     "created_by"
    t.text     "updated_by"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "products", id: false, force: true do |t|
    t.integer "product_id", limit: 8,   null: false
    t.text    "upc_desc"
    t.integer "item"
    t.text    "item_desc"
    t.integer "sku",        limit: 8
    t.text    "sku_desc"
    t.text    "seg"
    t.text    "seg_desc"
    t.text    "scat"
    t.text    "scat_desc"
    t.string  "cat",        limit: nil
    t.text    "cat_desc"
    t.integer "sdept"
    t.text    "sdept_desc"
    t.string  "dept",       limit: nil
    t.text    "dept_desc"
    t.string  "brand",      limit: nil
    t.text    "brand_desc"
    t.text    "uom"
  end

  add_index "products", ["product_id"], name: "product_id", using: :btree

  create_table "stores", id: false, force: true do |t|
    t.integer "store_id",               null: false
    t.integer "store_pos_id"
    t.text    "banner_abbr"
    t.integer "banner_id",    limit: 8
  end

  create_table "tier_products", force: true do |t|
    t.integer "tier_id"
    t.integer "trial_id"
    t.integer "product_id"
    t.decimal "selected_price",                 precision: 19, scale: 2
    t.decimal "vendor_funding",                 precision: 19, scale: 2
    t.decimal "subsidy_funding",                precision: 19, scale: 2
    t.integer "units",                limit: 8
    t.decimal "revenue",                        precision: 19, scale: 2
    t.decimal "cost",                           precision: 19, scale: 2
    t.decimal "margin",                         precision: 19, scale: 2
    t.json    "price_candidate_list"
    t.json    "product_funding"
    t.integer "marketing_vehicle_id"
    t.decimal "margin_percent",                 precision: 10, scale: 2
  end

  create_table "tiers", primary_key: "tier_id", force: true do |t|
    t.integer   "version_id"
    t.daterange "tier_dates"
    t.integer   "price_vehicle_id"
    t.integer   "sequence"
    t.json      "funding"
  end

  add_index "tiers", ["version_id", "tier_dates"], name: "Tier_version_id_tier_dates_key", unique: true, using: :btree

  create_table "time_mapping", id: false, force: true do |t|
    t.integer "ad_wk_id"
    t.date    "ad_wk_start_dt"
    t.date    "ad_wk_end_dt"
    t.integer "ad_weekend_date"
    t.integer "period"
  end

  create_table "users", force: true do |t|
    t.string   "username",                                             null: false
    t.string   "first_name",                                           null: false
    t.string   "last_name",                                            null: false
    t.string   "email",                                                null: false
    t.integer  "parent_id"
    t.string   "department"
    t.integer  "role"
    t.datetime "last_login_time"
    t.datetime "last_password_change_time"
    t.string   "password_digest"
    t.boolean  "status",                                default: true
    t.json     "user_preference"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "salt",                      limit: nil
    t.string   "password_reset_token",      limit: nil
  end

  create_table "version_stores", id: false, force: true do |t|
    t.integer "trial_id"
    t.integer "version_id"
    t.integer "store_id",   limit: 8
    t.integer "id",                   null: false
  end

  create_table "versions", primary_key: "version_id", force: true do |t|
    t.integer "trial_id"
    t.integer "marketing_vehicle_id"
    t.string  "ad_size"
  end

end
