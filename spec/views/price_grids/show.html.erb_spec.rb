require 'rails_helper'

RSpec.describe "price_grids/show", :type => :view do
  before(:each) do
    @price_grid = assign(:price_grid, PriceGrid.create!(
      :name => "Name",
      :description => "Description",
      :price_vehicle_id => 1,
      :price_rule_id => 2,
      :from_price => 3,
      :to_price => 4,
      :price_candidates => "Price Candidates",
      :multiples => "Multiples",
      :created_by => "Created By",
      :updated_by => "Updated By"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Description/)
    expect(rendered).to match(/1/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/4/)
    expect(rendered).to match(/Price Candidates/)
    expect(rendered).to match(/Multiples/)
    expect(rendered).to match(/Created By/)
    expect(rendered).to match(/Updated By/)
  end
end
