require 'rails_helper'

RSpec.describe "price_grids/new", :type => :view do
  before(:each) do
    assign(:price_grid, PriceGrid.new(
      :name => "MyString",
      :description => "MyString",
      :price_vehicle_id => 1,
      :price_rule_id => 1,
      :from_price => 1,
      :to_price => 1,
      :price_candidates => "MyString",
      :multiples => "MyString",
      :created_by => "MyString",
      :updated_by => "MyString"
    ))
  end

  it "renders new price_grid form" do
    render

    assert_select "form[action=?][method=?]", price_grids_path, "post" do

      assert_select "input#price_grid_name[name=?]", "price_grid[name]"

      assert_select "input#price_grid_description[name=?]", "price_grid[description]"

      assert_select "input#price_grid_price_vehicle_id[name=?]", "price_grid[price_vehicle_id]"

      assert_select "input#price_grid_price_rule_id[name=?]", "price_grid[price_rule_id]"

      assert_select "input#price_grid_from_price[name=?]", "price_grid[from_price]"

      assert_select "input#price_grid_to_price[name=?]", "price_grid[to_price]"

      assert_select "input#price_grid_price_candidates[name=?]", "price_grid[price_candidates]"

      assert_select "input#price_grid_multiples[name=?]", "price_grid[multiples]"

      assert_select "input#price_grid_created_by[name=?]", "price_grid[created_by]"

      assert_select "input#price_grid_updated_by[name=?]", "price_grid[updated_by]"
    end
  end
end
