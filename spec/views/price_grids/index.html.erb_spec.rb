require 'rails_helper'

RSpec.describe "price_grids/index", :type => :view do
  before(:each) do
    assign(:price_grids, [
      PriceGrid.create!(
        :name => "Name",
        :description => "Description",
        :price_vehicle_id => 1,
        :price_rule_id => 2,
        :from_price => 3,
        :to_price => 4,
        :price_candidates => "Price Candidates",
        :multiples => "Multiples",
        :created_by => "Created By",
        :updated_by => "Updated By"
      ),
      PriceGrid.create!(
        :name => "Name",
        :description => "Description",
        :price_vehicle_id => 1,
        :price_rule_id => 2,
        :from_price => 3,
        :to_price => 4,
        :price_candidates => "Price Candidates",
        :multiples => "Multiples",
        :created_by => "Created By",
        :updated_by => "Updated By"
      )
    ])
  end

  it "renders a list of price_grids" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
    assert_select "tr>td", :text => "Price Candidates".to_s, :count => 2
    assert_select "tr>td", :text => "Multiples".to_s, :count => 2
    assert_select "tr>td", :text => "Created By".to_s, :count => 2
    assert_select "tr>td", :text => "Updated By".to_s, :count => 2
  end
end
