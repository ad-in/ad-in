require 'rails_helper'

RSpec.describe "product_attributes/show", :type => :view do
  before(:each) do
    @product_attribute = assign(:product_attribute, ProductAttribute.create!(
      :product_id => "Product",
      :custom_attr_id => "Custom Attr",
      :value => "Value"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Product/)
    expect(rendered).to match(/Custom Attr/)
    expect(rendered).to match(/Value/)
  end
end
