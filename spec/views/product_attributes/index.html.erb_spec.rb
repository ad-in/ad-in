require 'rails_helper'

RSpec.describe "product_attributes/index", :type => :view do
  before(:each) do
    assign(:product_attributes, [
      ProductAttribute.create!(
        :product_id => "Product",
        :custom_attr_id => "Custom Attr",
        :value => "Value"
      ),
      ProductAttribute.create!(
        :product_id => "Product",
        :custom_attr_id => "Custom Attr",
        :value => "Value"
      )
    ])
  end

  it "renders a list of product_attributes" do
    render
    assert_select "tr>td", :text => "Product".to_s, :count => 2
    assert_select "tr>td", :text => "Custom Attr".to_s, :count => 2
    assert_select "tr>td", :text => "Value".to_s, :count => 2
  end
end
