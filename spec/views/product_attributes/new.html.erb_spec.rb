require 'rails_helper'

RSpec.describe "product_attributes/new", :type => :view do
  before(:each) do
    assign(:product_attribute, ProductAttribute.new(
      :product_id => "MyString",
      :custom_attr_id => "MyString",
      :value => "MyString"
    ))
  end

  it "renders new product_attribute form" do
    render

    assert_select "form[action=?][method=?]", product_attributes_path, "post" do

      assert_select "input#product_attribute_product_id[name=?]", "product_attribute[product_id]"

      assert_select "input#product_attribute_custom_attr_id[name=?]", "product_attribute[custom_attr_id]"

      assert_select "input#product_attribute_value[name=?]", "product_attribute[value]"
    end
  end
end
