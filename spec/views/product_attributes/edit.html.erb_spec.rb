require 'rails_helper'

RSpec.describe "product_attributes/edit", :type => :view do
  before(:each) do
    @product_attribute = assign(:product_attribute, ProductAttribute.create!(
      :product_id => "MyString",
      :custom_attr_id => "MyString",
      :value => "MyString"
    ))
  end

  it "renders the edit product_attribute form" do
    render

    assert_select "form[action=?][method=?]", product_attribute_path(@product_attribute), "post" do

      assert_select "input#product_attribute_product_id[name=?]", "product_attribute[product_id]"

      assert_select "input#product_attribute_custom_attr_id[name=?]", "product_attribute[custom_attr_id]"

      assert_select "input#product_attribute_value[name=?]", "product_attribute[value]"
    end
  end
end
