require 'rails_helper'

RSpec.describe "demos/edit", :type => :view do
  before(:each) do
    @demo = assign(:demo, Demo.create!(
      :price => ""
    ))
  end

  it "renders the edit demo form" do
    render

    assert_select "form[action=?][method=?]", demo_path(@demo), "post" do

      assert_select "input#demo_price[name=?]", "demo[price]"
    end
  end
end
