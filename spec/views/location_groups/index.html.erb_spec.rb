require 'rails_helper'

RSpec.describe "location_groups/index", :type => :view do
  before(:each) do
    assign(:location_groups, [
      LocationGroup.create!(
        :name => "MyText",
        :description => "MyText",
        :location_trail => "",
        :location_attribute => "",
        :created_by => "MyText",
        :updated_by => "MyText"
      ),
      LocationGroup.create!(
        :name => "MyText",
        :description => "MyText",
        :location_trail => "",
        :location_attribute => "",
        :created_by => "MyText",
        :updated_by => "MyText"
      )
    ])
  end

  it "renders a list of location_groups" do
    render
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
