require 'rails_helper'

RSpec.describe "location_groups/edit", :type => :view do
  before(:each) do
    @location_group = assign(:location_group, LocationGroup.create!(
      :name => "MyText",
      :description => "MyText",
      :location_trail => "",
      :location_attribute => "",
      :created_by => "MyText",
      :updated_by => "MyText"
    ))
  end

  it "renders the edit location_group form" do
    render

    assert_select "form[action=?][method=?]", location_group_path(@location_group), "post" do

      assert_select "textarea#location_group_name[name=?]", "location_group[name]"

      assert_select "textarea#location_group_description[name=?]", "location_group[description]"

      assert_select "input#location_group_location_trail[name=?]", "location_group[location_trail]"

      assert_select "input#location_group_location_attribute[name=?]", "location_group[location_attribute]"

      assert_select "textarea#location_group_created_by[name=?]", "location_group[created_by]"

      assert_select "textarea#location_group_updated_by[name=?]", "location_group[updated_by]"
    end
  end
end
