require 'rails_helper'

RSpec.describe "location_groups/show", :type => :view do
  before(:each) do
    @location_group = assign(:location_group, LocationGroup.create!(
      :name => "MyText",
      :description => "MyText",
      :location_trail => "",
      :location_attribute => "",
      :created_by => "MyText",
      :updated_by => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/MyText/)
  end
end
