require 'rails_helper'

RSpec.describe "notes/new", :type => :view do
  before(:each) do
    assign(:note, Note.new(
      :user_id => 1,
      :contract_id => 1,
      :description => "MyText"
    ))
  end

  it "renders new note form" do
    render

    assert_select "form[action=?][method=?]", notes_path, "post" do

      assert_select "input#note_user_id[name=?]", "note[user_id]"

      assert_select "input#note_contract_id[name=?]", "note[contract_id]"

      assert_select "textarea#note_description[name=?]", "note[description]"
    end
  end
end
