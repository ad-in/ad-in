require 'rails_helper'

RSpec.describe "price_rules/index", :type => :view do
  before(:each) do
    assign(:price_rules, [
      PriceRule.create!(
        :name => "Name",
        :description => "Description",
        :marketing_vehicle => "",
        :min_margin => 1,
        :max_margin => 2,
        :min_price_change => 3,
        :max_price_change => 4,
        :min_passthrough => 5,
        :max_passthrough => 6,
        :optimization_goal  => "Optimization Goal "
      ),
      PriceRule.create!(
        :name => "Name",
        :description => "Description",
        :marketing_vehicle => "",
        :min_margin => 1,
        :max_margin => 2,
        :min_price_change => 3,
        :max_price_change => 4,
        :min_passthrough => 5,
        :max_passthrough => 6,
        :optimization_goal  => "Optimization Goal "
      )
    ])
  end

  it "renders a list of price_rules" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
    assert_select "tr>td", :text => 5.to_s, :count => 2
    assert_select "tr>td", :text => 6.to_s, :count => 2
    assert_select "tr>td", :text => "Optimization Goal ".to_s, :count => 2
  end
end
