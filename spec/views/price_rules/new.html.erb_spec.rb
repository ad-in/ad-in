require 'rails_helper'

RSpec.describe "price_rules/new", :type => :view do
  before(:each) do
    assign(:price_rule, PriceRule.new(
      :name => "MyString",
      :description => "MyString",
      :marketing_vehicle => "",
      :min_margin => 1,
      :max_margin => 1,
      :min_price_change => 1,
      :max_price_change => 1,
      :min_passthrough => 1,
      :max_passthrough => 1,
      :optimization_goal  => "MyString"
    ))
  end

  it "renders new price_rule form" do
    render

    assert_select "form[action=?][method=?]", price_rules_path, "post" do

      assert_select "input#price_rule_name[name=?]", "price_rule[name]"

      assert_select "input#price_rule_description[name=?]", "price_rule[description]"

      assert_select "input#price_rule_marketing_vehicle[name=?]", "price_rule[marketing_vehicle]"

      assert_select "input#price_rule_min_margin[name=?]", "price_rule[min_margin]"

      assert_select "input#price_rule_max_margin[name=?]", "price_rule[max_margin]"

      assert_select "input#price_rule_min_price_change[name=?]", "price_rule[min_price_change]"

      assert_select "input#price_rule_max_price_change[name=?]", "price_rule[max_price_change]"

      assert_select "input#price_rule_min_passthrough[name=?]", "price_rule[min_passthrough]"

      assert_select "input#price_rule_max_passthrough[name=?]", "price_rule[max_passthrough]"

      assert_select "input#price_rule_optimization_goal [name=?]", "price_rule[optimization_goal ]"
    end
  end
end
