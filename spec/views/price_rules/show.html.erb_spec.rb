require 'rails_helper'

RSpec.describe "price_rules/show", :type => :view do
  before(:each) do
    @price_rule = assign(:price_rule, PriceRule.create!(
      :name => "Name",
      :description => "Description",
      :marketing_vehicle => "",
      :min_margin => 1,
      :max_margin => 2,
      :min_price_change => 3,
      :max_price_change => 4,
      :min_passthrough => 5,
      :max_passthrough => 6,
      :optimization_goal  => "Optimization Goal "
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Description/)
    expect(rendered).to match(//)
    expect(rendered).to match(/1/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/4/)
    expect(rendered).to match(/5/)
    expect(rendered).to match(/6/)
    expect(rendered).to match(/Optimization Goal /)
  end
end
