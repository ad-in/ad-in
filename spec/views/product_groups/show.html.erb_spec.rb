require 'rails_helper'

RSpec.describe "product_groups/show", :type => :view do
  before(:each) do
    @product_group = assign(:product_group, ProductGroup.create!(
      :name => "MyText",
      :description => "MyText",
      :product_trail => "",
      :product_attribute => "",
      :created_by => "MyText",
      :updated_by => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/MyText/)
  end
end
