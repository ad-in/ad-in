require "rails_helper"

RSpec.describe LocationGroupsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/location_groups").to route_to("location_groups#index")
    end

    it "routes to #new" do
      expect(:get => "/location_groups/new").to route_to("location_groups#new")
    end

    it "routes to #show" do
      expect(:get => "/location_groups/1").to route_to("location_groups#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/location_groups/1/edit").to route_to("location_groups#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/location_groups").to route_to("location_groups#create")
    end

    it "routes to #update" do
      expect(:put => "/location_groups/1").to route_to("location_groups#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/location_groups/1").to route_to("location_groups#destroy", :id => "1")
    end

  end
end
