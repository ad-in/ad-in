require "rails_helper"

RSpec.describe PriceGridsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/price_grids").to route_to("price_grids#index")
    end

    it "routes to #new" do
      expect(:get => "/price_grids/new").to route_to("price_grids#new")
    end

    it "routes to #show" do
      expect(:get => "/price_grids/1").to route_to("price_grids#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/price_grids/1/edit").to route_to("price_grids#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/price_grids").to route_to("price_grids#create")
    end

    it "routes to #update" do
      expect(:put => "/price_grids/1").to route_to("price_grids#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/price_grids/1").to route_to("price_grids#destroy", :id => "1")
    end

  end
end
