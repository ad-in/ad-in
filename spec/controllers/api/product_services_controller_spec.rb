require 'rails_helper'

RSpec.describe Api::ProductServicesController, :type => :controller do

  describe "GET getAllDepartments" do
    it "returns http success" do
      get :getAllDepartments
      expect(response).to be_success
    end
  end

  describe "GET getCategoriesByDepartment" do
    it "returns http success" do
      get :getCategoriesByDepartment
      expect(response).to be_success
    end
  end

  describe "GET getSubCategoriesForCategory" do
    it "returns http success" do
      get :getSubCategoriesForCategory
      expect(response).to be_success
    end
  end

  describe "GET getSegmentsForSubCategory" do
    it "returns http success" do
      get :getSegmentsForSubCategory
      expect(response).to be_success
    end
  end

  describe "GET getProductsByDepartment" do
    it "returns http success" do
      get :getProductsByDepartment
      expect(response).to be_success
    end
  end

  describe "GET getProductsByCategory" do
    it "returns http success" do
      get :getProductsByCategory
      expect(response).to be_success
    end
  end

  describe "GET getProductsBySubCategory" do
    it "returns http success" do
      get :getProductsBySubCategory
      expect(response).to be_success
    end
  end

  describe "GET getProductsBySegment" do
    it "returns http success" do
      get :getProductsBySegment
      expect(response).to be_success
    end
  end

end
