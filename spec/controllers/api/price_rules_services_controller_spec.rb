require 'rails_helper'

RSpec.describe Api::PriceRulesServicesController, :type => :controller do

  describe "GET addPriceRule" do
    it "returns http success" do
      get :addPriceRule
      expect(response).to be_success
    end
  end

  describe "GET updatePriceRule" do
    it "returns http success" do
      get :updatePriceRule
      expect(response).to be_success
    end
  end

  describe "GET removePriceRule" do
    it "returns http success" do
      get :removePriceRule
      expect(response).to be_success
    end
  end

  describe "GET getAllPriceRulesForCategories" do
    it "returns http success" do
      get :getAllPriceRulesForCategories
      expect(response).to be_success
    end
  end

  describe "GET getAllPriceRuleForProductIdAndLocationId" do
    it "returns http success" do
      get :getAllPriceRuleForProductIdAndLocationId
      expect(response).to be_success
    end
  end

  describe "GET getAllPriceRulesForCategoriesAndAttributes" do
    it "returns http success" do
      get :getAllPriceRulesForCategoriesAndAttributes
      expect(response).to be_success
    end
  end

  describe "GET getAllPriceRulesForLocationsAndAttributes" do
    it "returns http success" do
      get :getAllPriceRulesForLocationsAndAttributes
      expect(response).to be_success
    end
  end

end
