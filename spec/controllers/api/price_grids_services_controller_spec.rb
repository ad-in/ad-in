require 'rails_helper'

RSpec.describe Api::PriceGridsServicesController, :type => :controller do

  describe "GET addPriceGrid" do
    it "returns http success" do
      get :addPriceGrid
      expect(response).to be_success
    end
  end

  describe "GET updatePriceGrid" do
    it "returns http success" do
      get :updatePriceGrid
      expect(response).to be_success
    end
  end

  describe "GET removePriceGrid" do
    it "returns http success" do
      get :removePriceGrid
      expect(response).to be_success
    end
  end

  describe "GET getPriceGridByCategories" do
    it "returns http success" do
      get :getPriceGridByCategories
      expect(response).to be_success
    end
  end

  describe "GET getPriceGridByLocation" do
    it "returns http success" do
      get :getPriceGridByLocation
      expect(response).to be_success
    end
  end

  describe "GET getPriceGridByCategoryAndLocation" do
    it "returns http success" do
      get :getPriceGridByCategoryAndLocation
      expect(response).to be_success
    end
  end

  describe "GET getPriceGridByProductIdAndLocationId" do
    it "returns http success" do
      get :getPriceGridByProductIdAndLocationId
      expect(response).to be_success
    end
  end

end
