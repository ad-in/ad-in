require 'rails_helper'

RSpec.describe Api::ContractServicesController, :type => :controller do

  describe "GET addContract" do
    it "returns http success" do
      get :addContract
      expect(response).to be_success
    end
  end

  describe "GET modifyContract" do
    it "returns http success" do
      get :modifyContract
      expect(response).to be_success
    end
  end

  describe "GET sendForOptimize" do
    it "returns http success" do
      get :sendForOptimize
      expect(response).to be_success
    end
  end

  describe "GET approveContract" do
    it "returns http success" do
      get :approveContract
      expect(response).to be_success
    end
  end

  describe "GET rejectContract" do
    it "returns http success" do
      get :rejectContract
      expect(response).to be_success
    end
  end

  describe "GET getAllContracts" do
    it "returns http success" do
      get :getAllContracts
      expect(response).to be_success
    end
  end

end
