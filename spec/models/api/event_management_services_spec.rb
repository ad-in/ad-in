require 'rails_helper'

RSpec.describe EventManagementServices, :type => :model do

  describe "POST addEvent" do
    it "creates a new EventManagement" do
      expect {
        event_management = EventManagementServices.addEvent('The Summer Fest2','The Summer Fest2', {"12"=>{"31"=>{}}} ,{"12"=>{"31"=>{}}} , '2014-09-06',  '2014-09-06' ,  'shashi')
      }
    end
  end
#######################################################################################################################################################################################################
  describe "PUT update" do
    it "updates the requested event_management" do
      expect {
        event_management = EventManagementServices.updateEvent('The Summer Fest2','The Summer Fest2', {"12"=>{"31"=>{}}} ,{"12"=>{"31"=>{}}} , '2014-09-06',  '2014-09-06' ,  'shashi' '4')
      }
    end
  end
#######################################################################################################################################################################################################
  describe "DELETE :removeEvent" do
    it "destroys the requested event_management" do
      event_management = EventManagementServices.addEvent('The Summer Fest3','The Summer Fest3', {"12"=>{"31"=>{}}} ,{"12"=>{"31"=>{}}} , '2014-09-06',  '2014-09-06' ,  'shashi')
      expect {
        delete :removeEvent, {:id => event_management.to_param}
      }
    end
  end
end
#######################################################################################################################################################################################################