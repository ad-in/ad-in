require 'spec_helper'
require 'rails_helper'

describe BaseCostService, :type => :model do
  it "is valid with a productId, storeId" do
   product= BaseCostService.findProdutsBaseCostByLocation(  '1234567', '3456')
  expect(product)
 end
 ######################################################################################################### 
 it "is valid with a productId" do
   product1= BaseCostService.findProdutsBaseCostByProductId(1234567)
  expect(product1)
 end
 ######################################################################################################### 
 it "is valid with a productId, storeId, to ,from" do
   product2= BaseCostService.findProdutsBaseCostByDate(  '1234567','3456', '1222', '1222')
   expect(product2)
 end
 ######################################################################################################### 
 
  it "has no Product in the database" do
    
    expect(BaseCostService).to eq 0
  end
 ######################################################################################################### 
 it "has one record" do
    expect(BaseCostService).to eq 1
  end
  ######################################################################################################### 
  it "counts only records that match a query" do
    expect(BaseCostService.where(:store_id => "3651")).to eq 1
    expect(BaseCostService.where(:store_id => "1234")).to eq 0
  end
 ######################################################################################################### 
end
