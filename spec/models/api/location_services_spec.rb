require 'rails_helper'

RSpec.describe LocationServices, :type => :model do
 
 ############################################################################################################################  
  it "should not be valid when banner id is empty" do
    stores=LocationServices.getAllStores(nil)
    expect(nil).to eq(stores)
    
  end
 ############################################################################################################################ 
   it "should be valid when banner is not empty" do
    stores=LocationServices.getAllStores(13)
    if(stores!= nil)
     
    expect( ["2725"]).to eq(stores)
    end
  end
 ############################################################################################################################
 
 it "should not be valid when super banner is empty" do
    LeafBanners=LocationServices.getLeafBanners(nil)
    expect([13]).to eq(LeafBanners)
    
  end
 ############################################################################################################################ 
   it "should be valid when super banner is not empty" do
    LeafBanners=LocationServices.getLeafBanners(13)
    if(LeafBanners!= nil)
    expect([13, 13]).to eq(LeafBanners)
    end
  end
 ############################################################################################################################
 
 it "should not be valid when parent banner is empty" do
    stores=LocationServices.getAllSubBanners(nil)
    expect(nil).to eq(stores)
    
  end
 ############################################################################################################################ 
   it "should be valid when parent banner is not empty" do
    stores=LocationServices.getAllSubBanners(13)
    if(stores!= nil)
    expect( {13=>"leaf"}).to eq(stores)
    end
  end
 ############################################################################################################################
end
