require 'rails_helper'

RSpec.describe GeographyServices, :type => :model do

  it "is valid with a bannerId" do
    banner= GeographyServices.getBanner(3160)
    expect(banner)
  end
######################################################################################################### 
  it "is valid with a storeId" do
    store= GeographyServices.getStore(3160)
    expect(store)
  end
######################################################################################################### 
  it "is valid with a superBannerId" do
    superBanner=GeographyServices.getSuperBanner(3160)
    expect(superBanner)
  end
  
######################################################################################################### 
  it "has no record in the database" do

    expect(GeographyServices).to eq 0
  end
  
######################################################################################################### 
  it "has one record" do
    expect(GeographyServices).to eq 1
  end
######################################################################################################### 

  it "counts only records that match a query" do
    expect(GeographyServices.where(:store_id => "3651")).to eq 1
    expect(GeographyServices.where(:store_id => "1234")).to eq 0
  end
######################################################################################################### 
end
