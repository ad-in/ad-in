require 'rails_helper'

RSpec.describe Api::PriceRulesServices, :type => :model do
  
  describe Api::PriceRulesServices.addPriceRule do
    it "should return success message if saved" do
      price_rule = Factory(:price_rule)
      res = Api::PriceRulesServices.addPriceRule(price_rule)
      expect(res).to_not eq 0
    end
  end
end
