require 'spec_helper'
require 'rails_helper'

describe BasePriceService, :type => :model do
  it "is valid with a productId, storeId" do
   product= BasePriceService.findProdutsBasePriceByLocation(  '1234567', '3456')
  expect(product).to have(1).record
 end
 ######################################################################################################### 
 it "is valid with a productId" do
   product1= BasePriceService.findProdutsBasePriceByProductId(1234567)
  expect(product1).to have(1).record
 end
 ######################################################################################################### 
 it "is valid with a productId, storeId, to ,from" do
   product2= BasePriceService.findProdutsBasePriceByDate(  '1234567','3456', '1222', '1222')
  expect(product2).to have(1).record
 end
 ######################################################################################################### 
end
