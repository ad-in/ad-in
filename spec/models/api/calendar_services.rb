require 'rails_helper'

RSpec.describe Calendar_Services, :type => :model do

############################################################################################################################
  it "should not be valid when banner id is empty" do
    weeks=Calendar_Services.getWeekByDate(nil,nil)
    expect(nil).to eq(weeks)

  end
############################################################################################################################
  it "should be valid when banner is not empty" do
    weeks=Calendar_Services.getWeekByDate('20-12-2009','31-12-2009')
    if(weeks!= nil)
      weeks.each { |row|
        expect( "2009-12-25").to eq(row['ad_wk_start_dt'])
      }

    end
  end
############################################################################################################################
  it "should not be valid when banner id is empty" do
    weeks=Calendar_Services.getWeekByPeriod(nil)
    expect(nil).to eq(weeks)

  end
############################################################################################################################
  it "should be valid when banner is not empty" do
    weeks=Calendar_Services.getWeekByPeriod(1)
    if(weeks!= nil)
      weeks.each { |row|
        expect( "2009-12-25").to eq(row['ad_wk_start_dt'])
      }

    end
  end
############################################################################################################################
end
