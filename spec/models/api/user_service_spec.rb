require 'rails_helper'

RSpec.describe Api::UserServices, :type => :model do
  before { @user = User.new(username: "Example User", email: "user@example.com") }

  subject { @user }
  it { should respond_to(:username) }
  it { should respond_to(:email) }
  it { should respond_to(:encrypted_password) }
  it { should respond_to(:password) }
  it { should respond_to(:password_confirmation) }

  it "should respond to 'username'" do
   expect(@user).to respond_to(:username)
  end
  it "should respond to 'email'" do
   expect(@user).to respond_to(:email)
  end
  
  describe "when username is not present" do
    before { @user.username = " " }
    it { should_not be_valid }
  end
  
  describe "when email is not present" do
    before { @user.email = " " }
    it { should_not be_valid }
  end
  
  describe "when email address is already taken" do
    before do
      user_with_same_email = @user.dup
      user_with_same_email.save
    end

    it { should_not be_valid }
  end
  
  describe "when password is not present" do
    before do
      @user = User.new(username: "Example User", email: "user@example.com",
                       password: " ", password_confirmation: " ")
    end
    it { should_not be_valid }
  end
  
  describe "when password doesn't match confirmation" do
    before { @user.password_confirmation = "mismatch" }
    it { should_not be_valid }
  end
  
end
