class Version < ActiveRecord::Base
  has_many :tiers
  
  def self.createVersions(attribute_id , store_list , versionObject)
    conn = ActiveRecord::Base.connection
    res = conn.execute("select value, array_agg(location_id) as stores from location_attributes where custom_attr_id = #{attribute_id} and location_id in (#{store_list.join(",")}) group by 1")
    versionIdsArray = Array.new
    if  !res.nil?
      sequence = 0
      res.each{|row|
        if sequence == 0
          @version_prev = versionObject
          @version_prev.zone_type = attribute_id
          @version_prev.zone_value = row["value"]
        @version_prev.save!
        versionIdsArray.push(versionObject.version_id)
        sequence = sequence + 1
        else
          @version = Version.new
          @version.trial_id = versionObject.trial_id
          @version.sequence = sequence
          @version.zone_type = attribute_id
          @version.zone_value = row["value"]
          @version.save!
          versionIdsArray.push(@version.version_id)

          retrievedVersionStores = VersionStore.where(version_id: versionObject.version_id)
          retrievedVersionStores.each{|version_stores|
            storeArray = row["stores"]

            p storeArray

            if storeArray != nil
              storeArray = storeArray.gsub("{","")
              storeArray = storeArray.gsub("}","")
              storeArray = storeArray.split(",").map {|s| s.to_i}
            end

            if  storeArray.include?version_stores.store_id
            version_stores.version_id = @version.version_id
            version_stores.save!
            end
          }

          sequence = sequence + 1
          version_tiers = Tier.where(version_id: versionObject.version_id)
          version_tiers.each{|tier|
            @newtier = Tier.new
            @newtier.version_id = @version.version_id
            @newtier.tier_dates = tier.tier_dates
            @newtier.price_vehicle_id = tier.price_vehicle_id
            @newtier.funding = tier.funding
            @newtier.vendor_info = tier.vendor_info
            @newtier.contract_id = tier.contract_id
            @newtier.unit_funding = tier.unit_funding
            @newtier.lumpsum_funding = tier.lumpsum_funding
            @newtier.sequence =  tier.sequence
            @newtier.save!

            #copy main tier product info to new tier product info in tier_product table
            @tier_products=Contract.get_tier_product_byTierId(tier.tier_id)
            @tier_products.each do |tier_products|
              @tierPro = TierProduct.new
              @tierPro.tier_id = @newtier.tier_id
              @tierPro.trial_id = tier_products.trial_id
              @tierPro.product_id = tier_products.product_id
              @tierPro.cost = tier_products.cost
              @tierPro.contract_id = tier_products.contract_id
              @tierPro.unit_funding = tier_products.unit_funding
              @tierPro.save!
            end
          }
        end
      }
    else
    versionIdsArray.push(versionObject.version_id)
    end
    return versionIdsArray
  end
  
end
