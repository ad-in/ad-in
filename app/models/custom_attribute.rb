class CustomAttribute < ActiveRecord::Base
  # include ActiveModel::Validations
  # validates_presence_of :name
  # validates_presence_of :value
  # validates_presence_of :description
  # validates_uniqueness_of :name
  # validates_with CustomValidator
  
  def self.getAllVersionableCustomAttributes
    @versionableCustomAttributes = CustomAttribute.where(versionable: "TRUE")
    return @versionableCustomAttributes
  end
  
end
