class TierProduct < ActiveRecord::Base
  belongs_to :tier
  belongs_to :product
end
