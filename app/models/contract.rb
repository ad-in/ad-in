require '../AdInServices/app/models/api/location_services.rb'
require '../AdInServices/app/models/api/product_services.rb'
require '../AdInServices/app/models/api/marketing_vehicle_services.rb'
require '../AdInServices/app/models/api/location_group_services.rb'
require '../AdInServices/app/models/api/base_cost_service.rb'
require '../AdInServices/app/models/api/base_price_service.rb'

# require '../AdInServices/app/models/api/product_group_services.rb
require '../AdInServices/app/models/location_group.rb'
require '../AdInServices/app/models/tier.rb'
require '../AdInServices/app/models/tier_product.rb'
require '../AdInServices/app/models/product.rb'
require '../AdInServices/app/models/note.rb'
require '../AdInServices/app/models/version.rb'
require '../AdInServices/app/models/version_store.rb'
require '../AdInServices/app/models/api/contract_services.rb'


class Contract < ActiveRecord::Base
  scope :by_promotion_window, lambda{|promotion_window| where(:promotion_window=> promotion_window) unless promotion_window.blank?}   
  
  include Workflow
  workflow do
    

    state :draft do
      event :approve, :transitions_to => :approved
      event :reject, :transitions_to => :rejected
    end

    state :approved do
      
    end

    state :rejected do
      
    end

  end
  
  def self.get_record(params)
      begin
          promotionDate = params[:filter_daterange]
         if !promotionDate.blank?
         #    promotion_window = "[#{Time.zone.now.to_date.beginning_of_week},#{Time.zone.now.to_date.end_of_week})"
         # else 
            dates = promotionDate.split("-")
            puts dates
            start_date= Date.strptime(dates[0].gsub(" ",""), '%m/%d/%Y')
            end_date= Date.strptime(dates[1].gsub(" ",""), '%m/%d/%Y')
          promotion_window = "[#{start_date},#{end_date})"
          end
          contracts = Contract.by_promotion_window(promotion_window)
      rescue Exception => e
        contracts = nil        
      end
      contracts
    end
    
    
  def  self.get_tier(contract_id)
    @contract_id = contract_id
    if(@contract_id!=nil)
      @tier= Tier.where("contract_id=#{@contract_id}")
      logger.info { "contract get_tier =#{@tier}" }
      return @tier 
   
      else
    return nil
     end
end

 def self.get_tier_product(trial_id)
    @trial_id= trial_id
    if(@trial_id!=nil)
      @tier_product = TierProduct.where("trial_id=#{@trial_id}")
      logger.info { "contract get_tier_product =#{@tier_product}" }
      return @tier_product
    else
    return nils
    end
  end

  def self.get_tier_product_info(trial_id)
    @trial_id= trial_id
    @product_data=Array.new
    if(@trial_id!=nil)
      @tier_product = TierProduct.where("trial_id=#{@trial_id}")
      logger.info { "contract get_tier_product =#{@tier_product}" }
      
      
      
      @tier_product.each do |tier_product|
        
        @product = Product.where("product_id=#{tier_product.product_id}")
               @product.each do |product|
                 @product_info=Hash.new()
                   @product_info.store('product',product.product_id)
                   @product_info.store('product_name',product.upc_desc)
                   @product_info.store('product_desc',product.sku_desc)
                   if(tier_product.product_funding!=nil)
                      @product_info.store('off_invoice',tier_product.product_funding['off_invoice'])
                      @product_info.store('bill_back',tier_product.product_funding['bill_back'])
                      @product_info.store('scan',tier_product.product_funding['scan'])
                      @product_info.store('subsidy_bill_back',tier_product.product_funding['subsidy_bill_back'])
                      @product_info.store('subsidy_off_invoice',tier_product.product_funding['subsidy_off_invoice'])
                      @product_info.store('subsidy_scan',tier_product.product_funding['subsidy_scan'])
                      @product_info.store('unit_funding',tier_product.unit_funding)
                      @product_info.store('tier_products',tier_product.cost)
                    else
                      @product_info.store('off_invoice',"")
                      @product_info.store('bill_back',"")
                      @product_info.store('scan',"")
                      @product_info.store('subsidy_bill_back',"")
                      @product_info.store('subsidy_off_invoice',"")
                      @product_info.store('subsidy_scan',"")
                      @product_info.store('unit_funding',"")
                      @product_info.store('tier_products',"")
                      
                   end
                   @product_data.push(@product_info)
                   logger.info { "1product_data =#{@product_data}" }
              end
      end
       logger.info { "contract @product_data =#{@product_data}" }
      return @product_data
      
    else
    return nils
    end
  end
  
  def self.get_product(product_id)
    @product_id= product_id
    if(@product_id!=nil)
      @product = Product.where("product_id=#{@product_id}")
      return @product
    else
    return nils
    end
  end


  def  self.get_tier0(version_id)
    @version_id = version_id
    if(@version_id!=nil)
      @tier= Tier.where("version_id in (#{@version_id}) and sequence = 0")
      logger.info { "contract get_tier0 =#{@tier}" }
      return @tier 
   
      else
    return nil
     end
  end
  
  def  self.get_tier_byTierId(tier_id)
    @tier_id = tier_id
    if(@tier_id!=nil)
      @tier= Tier.find_by_tier_id(@tier_id)
      logger.info { "contract get_tier_byTierId =#{@tier}" }
      return @tier 
      else
    return nil
     end
  end

  def self.get_product_base_cost(sku,storeList)
     if(sku!=nil)
      @product_base_cost = Api::BaseCostService.findProdutsBaseCost(sku,storeList)
      logger.info { "@product_base_cost =#{@product_base_cost}" }
      return @product_base_cost
    else
    return nils
    end
  end
  
  def self.get_product_base_price(sku,storeList)
    if(sku!=nil)
      @product_base_price =  Api::BasePriceService.findProdutsBasePrice(sku,storeList)
      logger.info { "@product_base_price =#{@product_base_price}" }
      return @product_base_price
    else
    return nils
    end
  end

  def self.get_tier_product_byTierId(tier_id)
    @tier_id= tier_id
    if(@tier_id!=nil)
      @tier_product = TierProduct.where("tier_id=#{@tier_id}")
      logger.info { "contract get_tier_product =#{@tier_product}" }
      return @tier_product
    else
    return nils
    end
  end


  def self.get_tier_product_info_byTierId(tier_id)
   @tier_id= tier_id
    @product_data=Array.new
    if(@tier_id!=nil)
      @tier_products = TierProduct.where("tier_id=#{@tier_id}")
      trial_id = @tier_products[0].trial_id
      contract = Contract.find_by_trial_id(trial_id)
      logger.info { "contract get_tier_product_info_byTierId =#{@tier_id}" }
      
      @tier_products.each do |tier_product|
        
          product = Product.find_by_product_id(tier_product.product_id)
              
           @product_info=Hash.new()
           base_cost=Contract.get_product_base_cost(product.sku ,contract.store_ids)
           logger.info { "0product_data =#{@product_data}" }
           base_price=Contract.get_product_base_price(product.sku ,contract.store_ids)
           @product_info.store('product',product.product_id)
           @product_info.store('product_name',product.upc_desc)
           @product_info.store('product_desc',product.sku_desc)
           @product_info.store('base_cost',base_cost)
           @product_info.store('base_price',base_price)
           @product_info.store('unit_funding',tier_product.unit_funding.to_f)
           @product_info.store('tier_products',tier_product.cost)
           @product_info.store('trial_id',trial_id)
           @product_info.store('contract_id',tier_product.contract_id)
           @product_data.push(@product_info)
           logger.info { "1product_data =#{@product_data}" }
             
      end
       logger.info { "contract @product_data =#{@product_data}" }
      return @product_data
      
    else
    return nils
    end
  end
  
  def  self.get_trial0(contract_id)
    @contract_id = contract_id
    if(@contract_id!=nil)
      @contract= Contract.find_by_contract_id_and_sequence(@contract_id , 0)
      logger.info { "contract get_trial0 =#{@contract}" }
      return @contract 
   
      else
    return nil
     end
  end
  
  def  self.get_tier_by_version_id(version_id)
    if(version_id!=nil)
      @tier= Tier.where("version_id=#{version_id}").order("sequence ASC")
      logger.info { "contract get_tier_by_version_id =#{@tier}" }
      return @tier 
    else
      return nil
    end
  end
  
  def  self.get_trial(trial_id)
    if(trial_id!=nil)
      contract= Contract.find_by_trial_id(trial_id)
      logger.info { "contract get_trial =#{contract}" }
      return contract
      else
    return nil
     end
  end
  
  def self.get_versions(trial_id)
    if(trial_id!=nil)
      versions = Version.where("trial_id=#{trial_id}").order("sequence ASC")
      logger.info { "contract get_version =#{versions}" }
      return versions
      else
    return nil
     end
  end
  
  def self.get_contracts(contract_id)
    if(contract_id!=nil)
      contracts = Contract.where("contract_id=#{contract_id}").order("sequence ASC")
      logger.info { "contract get_contracts =#{contracts}" }
      return contracts
      else
    return nil
     end
  end
  
end
