class CustomValidator < ActiveModel::Validator
  include ActiveModel::Validations
  def validate(record)
    if(record.attribute_type !="" )

      if(record.attribute_type=="NumberList" && !record.value.match('^([0-9]+,?)+$') )

        record.errors[:base] << "Please enter number's seprated by comma"

      elsif(record.attribute_type=="Number" && !record.value.match('^([0-9]?)+$'))
        record.errors[:base] << "Please enter a number"

      elsif(record.attribute_type=="StringList" && !record.value.match('[0-9a-zA-Z]+(,[0-9a-zA-Z]+)*'))
        record.errors[:base] << "Please enter String's seprated by comma"
      elsif(record.attribute_type=="String" && !record.value.match('[0-9a-zA-Z]+([0-9a-zA-Z]+)*'))
        record.errors[:base] << "Please enter String"
      else
        record.errors[:base]
      end
    end

  end

end