

class Api::ContractServices < ActiveRecord::Base
  
  class << self
    
    def createContract(sessionContract)
      session_contract = sessionContract
      trial_id = session_contract.fetch("trial_id")
      if session_contract.has_key?"trial_id"
        trial_id = session_contract.fetch("trial_id")
      end
      if session_contract.has_key?"versions"
        versionArray = session_contract.fetch("versions")
        if !versionArray.empty?
          versionArray.each { |version|
            versionHash = version
            if versionHash.has_key?"version_id"
              versionId = versionHash.fetch("version_id")
              Version.destroy(versionId.to_i)
              #for version  
              newVersionId = createVersion(session_contract)
              #for version stores
              if versionHash.has_key?"version_stores"
                storeIds = Array.new
                versionStores = versionHash.fetch("version_stores")
                versionStores.each{|versionStore|
                  storeIds << versionStore["store_id"]
                }
                Api::ContractServices.createVersionStores(trial_id,newVersionId,storeIds)
              end
              #for tier in version
               if versionHash.has_key?"version_tiers"
                tierArray = versionHash.fetch("version_tiers")
                logger.info"++++tierArray is = #{tierArray}"
                tier_products_array = Array.new
                prodList = Array.new
                if !tierArray.empty?
                  tierArray.each { |tier|
                    tier_dates = nil
                    price_vehicle_id = nil
                    sequence= nil
                    vendor_info = nil
                    unit_funding = nil
                    lumpsum_funding = nil
                    
                    if tier.has_key?"tier_dates"
                      tier_dates =  tier.fetch("tier_dates" )
                    end
                    if tier.has_key?"price_vehicle_id"
                      price_vehicle_id = tier.fetch("price_vehicle_id")
                    end
                    if tier.has_key?"sequence"
                      sequence = tier.fetch("sequence")
                    end 
                    if tier.has_key?"vendor_info"
                      vendor_info = tier.fetch("vendor_info")
                    end
                    if tier.has_key?"unit_funding"
                      unit_funding = tier.fetch("unit_funding")
                    end
                    if tier.has_key?"lumpsum_funding"
                      lumpsum_funding = tier.fetch("lumpsum_funding")
                    end
                                         
                    new_tier_id = createTier(newVersionId,tier_dates,price_vehicle_id,sequence,vendor_info,unit_funding,lumpsum_funding)
                    if tier.has_key?"tier_products_array"
                      tier_products_array_local = tier.fetch("tier_products_array")
                      tier_products_array_local.each{|tier_prod|
                        createTierProduct(new_tier_id, tier_prod)
                        }
                    end
                  }
                end
              end
            end
          }
        end
      end
    end
   
   
    def createVersion(session_contract)
        if session_contract.has_key?"versions"
          versionArray = session_contract.fetch("versions")
          if !versionArray.empty?
            @version = Version.new
            @version.trial_id = session_contract.fetch("trial_id")
            @version.save!
            return @version.id
          end
        end
     end
    
     
     def createVersionStores(trialId , versionId , store_ids)
       @contract = Contract.find_by_trial_id(trialId)
       if @contract != nil
         arrayOfStores = Api::VersionStoreServices.addVersionStore(@contract, store_ids, versionId )
       end
     end
    
     def createTier(newVersionId,tier_dates,price_vehicle_id,sequence,vendor_info,unit_funding,lumpsum_funding)
      @tier = Tier.new
      @tier.version_id = newVersionId
      @tier.tier_dates = tier_dates
      @tier.price_vehicle_id = price_vehicle_id
      @tier.sequence = sequence
      @tier.vendor_info = vendor_info
      @tier.unit_funding = unit_funding
      @tier.lumpsum_funding = lumpsum_funding
      if @tier.save!
        return @tier.id
      else
        return false
      end
    end
    
    def createTierProduct(new_tier_id, tier_product)
      @tr_prod = TierProduct.new
      @tr_prod.tier_id = new_tier_id
      
      @tr_prod.product_id = tier_product["product_id"]
      @tr_prod.selected_price = tier_product["selected_price"]
      @tr_prod.vendor_funding = tier_product["vendor_funding"]
      @tr_prod.subsidy_funding = tier_product["subsidy_funding"]
      @tr_prod.units = tier_product["units"]
      @tr_prod.revenue = tier_product["revenue"]
      @tr_prod.cost = tier_product["cost"]
      @tr_prod.margin = tier_product["margin"]
      @tr_prod.price_candidate_list = tier_product["price_candidate_list"]
      @tr_prod.product_funding = tier_product["product_funding"]
      @tr_prod.marketing_vehicle_id = tier_product["marketing_vehicle_id"]
      @tr_prod.margin_percent = tier_product["margin_percent"]
      @tr_prod.unit_funding = tier_product["unit_funding"]
      @tr_prod.effective_price = tier_product["effective_price"]
      @tr_prod.base_cost = tier_product["base_cost"]
      @tr_prod.base_price = tier_product["base_price"]
      
      if @tr_prod.save!
        return @tr_prod.id
      else
        return false
      end
      
    end
    
    def approveContract(contract_id)
      contract = Contract.find_by(trial_id: trial_id)
      sequence = contract.sequence
      if sequence == 0
        contract.approve
      end
    end
    
    def rejectContract(contract_id)
      contracts = Contract.find_by(contract_id: contract_id)
      if contracts.nil?
        contracts.each{|contract|
          contract.reject
        }
      end
    end
    
    def changeMainContractCopy(contract_id)
      contract = Contract.find_by(contract_id: contract_id)
      if !contract.sequence = 0
        mainContractCopy = Contract.where("contract_id = ? AND sequence = ?", contract_id, 0)
        sequenceForTrialCopy = contract.sequence
        contract.update(sequence: 0)
        mainContractCopy.update(sequence: sequenceForTrialCopy)
      end
    end
    
    def getAllInformationAttributes(contract_id)
      conn = ActiveRecord::Base.connection
      res = conn.execute("SELECT * from contract_attributes where contract_id='#{contract_id}' AND product_id is null")
      h = Hash.new
      res.each {|row|
        attribute_id = row['attribute_id']
        attributeName = conn.execute("select name from custom_attributes where id = '#{attribute_id}'")
        h.store(attributeName , row['value'])
      }
      finalHash = Hash.new
      return finalHash.store(contract_id, h)
    end
    
    def getAllProductAttributesInContract(contract_id)
      conn = ActiveRecord::Base.connection
      res = conn.execute("SELECT * from contract_attributes where contract_id='#{contract_id}' AND product_id is not null")
      h = Hash.new
      res.each {|row|
        attribute_id = row['attribute_id']
        attributeName = conn.execute("select name from custom_attributes where id = '#{attribute_id}'")
        h.store(attributeName , row['value'])
      }
      finalHash = Hash.new
      return finalHash.store(contract_id, h)
    end
    
    def getAllNotes(contract_id)
      conn = ActiveRecord::Base.connection
      res = conn.execute("SELECT * from notes where contract_id='#{contract_id}' ")
      return res
    end
    
    def addNote(note)
      @note = note
      if @note.save
        return "Note was successfully created"
      else
        return false
      end
    end
    
    def getAllContracts
      conn = ActiveRecord::Base.connection
		  res = conn.execute("select * from contracts")
       if res.num_tuples.zero?
        return false
       else
        return res
       end
    end
    
    # conract by params send from filter
    def getRecordByParams(params)
      conn = ActiveRecord::Base.connection
      begin
          categoryId = params[:category_id] unless params[:category_id].blank?
          locationId = params[:location_id] unless params[:location_id].blank?
          promotionDate = params[:filter_daterange]
         if !promotionDate.blank?
            dates = promotionDate.split("-")
            puts dates
            start_date= Date.strptime(dates[0].gsub(" ",""), '%m/%d/%Y')
            end_date= Date.strptime(dates[1].gsub(" ",""), '%m/%d/%Y')
            promotion_window = "[#{start_date},#{end_date})"
           contracts = getContractsByPromotionWindow(promotion_window)
        elsif !categoryId.blank?
            contracts = getContractsByCategoryId(categoryId)
        elsif !locationId.blank?
          contracts = getContractsByLocationId(locationId)
        else    
          contracts = getAllContracts
        end
        rescue Exception => e
          contracts = nil        
        end
     return contracts
    end
    
    # contracts by promotion window
    def getContractsByPromotionWindow(promotion_window)
      if !promotion_window.blank?
      conn = ActiveRecord::Base.connection
      contracts = conn.execute("select * from contracts where promotion_window = '#{promotion_window}'")
      end
     return contracts
    end
    
    # contracts by category id
    def getContractsByCategoryId(categoryId)
      if !categoryId.blank?
      conn = ActiveRecord::Base.connection
      contracts = conn.execute("select * from contracts where trial_id in(select trial_id from tier_products where product_id in(select product_id from products where category_id = #{categoryId}))")
      end
     return contracts
   end

  # contracts by locatio id
    def getContractsByLocationId(locationId)
      if !locationId.blank?
      conn = ActiveRecord::Base.connection
      contracts = conn.execute("select * from contracts where trial_id in(select trial_id from version_stores where store_id in(select store_id from stores where location_id = #{locationId}))")
      end
     return contracts
   end
    def addContractInformationAttribute(contract_id)
      
    end
    
      def getMaxTierSequenceNo(version_Id)
      conn = ActiveRecord::Base.connection
      res = conn.execute("select max(sequence) from tiers where version_id=#{version_Id}")
      return res
    end
    
    def getMaxTrialSequenceNo(contract_id)
      conn = ActiveRecord::Base.connection
      res = conn.execute("select max(sequence) from contracts where contract_id=#{contract_id}")
      return res
    end
    
    def createTrial(contract_id)
      
      contract = Contract.get_trial0(contract_id)
      newcontract = Contract.new
      sequence =  Api::ContractServices.getMaxTrialSequenceNo(contract_id)
      logger.info"+++++++#{sequence.max['max']}"
      newcontract.sequence = sequence.max['max'].to_i + 1
      newcontract.name = contract.name
      newcontract.description = contract.description
      newcontract.contract_window = contract.contract_window
      newcontract.duration = contract.duration
      newcontract.promotion_window = contract.promotion_window
      newcontract.vendor_name = contract.vendor_name   
      newcontract.workflow_state = contract.workflow_state
      newcontract.promotion_duration = contract.promotion_duration
      newcontract.store_ids = contract.store_ids   
      newcontract.contract_id = contract.contract_id
      newcontract.save!
      
      Api::ContractServices.createVersionForTrial(contract.trial_id,newcontract.trial_id)
      
      
      return newcontract.trial_id
    end
    
    def createVersionForTrial(trial_id,new_trial_id)
      versions = Version.where("trial_id =#{trial_id}")
      main_trial_versions = []
      new_trial_versions = []
      
      versions.each do |version|
        newversion = Version.new
        newversion.trial_id = new_trial_id
        newversion.marketing_vehicle_id = version.marketing_vehicle_id   
        newversion.ad_size = version.ad_size
        newversion.save!
        main_trial_versions << version.version_id
        new_trial_versions << newversion.version_id
      end
      logger.info"main_trial_version #{main_trial_versions} new_trial_version #{new_trial_versions}"
      Api::ContractServices.createTierForTrialVersion(main_trial_versions,new_trial_versions,new_trial_id)
    end
    
    def createTierForTrialVersion(main_trial_versions,new_trial_versions,new_trial_id)
      position = 0
      main_trial_versions_tiers = []
      new_trial_versions_tiers = []
      main_trial_versions.each {|main_trial_version|
           tiers = Contract.get_tier_by_version_id(main_trial_version)
           tiers.each {|tier|
             newtier = Tier.new
             newtier.version_id = new_trial_versions[position]
             newtier.tier_dates = tier.tier_dates
             newtier.price_vehicle_id = tier.price_vehicle_id
             newtier.funding = tier.funding
             newtier.vendor_info = tier.vendor_info
             newtier.contract_id = tier.contract_id
             newtier.unit_funding = tier.unit_funding
             newtier.lumpsum_funding = tier.lumpsum_funding
             newtier.sequence = tier.sequence
             newtier.save!
             main_trial_versions_tiers << tier.tier_id
             new_trial_versions_tiers << newtier.tier_id
           }
           position += 1
        }
      logger.info"main_trial_versions_tiers #{main_trial_versions_tiers} new_trial_versions_tiers #{new_trial_versions_tiers}"
      Api::ContractServices.createTierProductForTrialVersionTier(main_trial_versions_tiers,new_trial_versions_tiers,new_trial_id)
    end
    
    def createTierProductForTrialVersionTier(main_trial_versions_tiers,new_trial_versions_tiers,new_trial_id)
      position = 0
      main_trial_versions_tiers.each {|main_trial_versions_tier|
      
         tier_products=Contract.get_tier_product_byTierId(main_trial_versions_tier)
         tier_products.each do |tier_product|
           tierPro = TierProduct.new
           tierPro.tier_id = new_trial_versions_tiers[position]
           tierPro.trial_id = new_trial_id
           tierPro.product_id = tier_product.product_id
           tierPro.cost = tier_product.cost
           tierPro.contract_id = tier_product.contract_id
           tierPro.unit_funding = tier_product.unit_funding
           tierPro.effective_price = tier_product.effective_price
           tierPro.base_cost = tier_product.base_cost
           tierPro.base_price = tier_product.base_price
           tierPro.price_candidate_list = tier_product.price_candidate_list
           tierPro.save!        
                      
        end
        position += 1
      }
      logger.info"Tier product copyed succesfully"
    end
    
    def getContrctObject(contract_id)
      contracts = Contract.get_contracts(contract_id)
      contract_info_hash = Hash.new
      trialArray = Array.new
      contract_info_hash.store("contract_id",contracts[0].contract_id)
      contract_info_hash.store("store_ids",contracts[0].store_ids)
      contracts.each {|contract|
        trial_info_hash = Hash.new
         dates=contract.promotion_window
         startDate=dates.first
         endDate=dates.end
         trial_info_hash.store("startDate",startDate)
         trial_info_hash.store("endDate",endDate)
        trial_info_hash.store("trial_id",contract.trial_id)
        trial_info_hash.store("sequence",contract.sequence)
        trial_info_hash.store("name",contract.name)
        trial_info_hash.store("description",contract.description)
        trial_info_hash.store("versionArray",Api::ContractServices.getVersionArrayObject(contract.trial_id))
         numberOfVersions=(Api::ContractServices.getVersionArrayObject(contract.trial_id)).count
          trial_info_hash.store("numberOfVersions",numberOfVersions)
        trialArray.push(trial_info_hash)
      }
      contract_info_hash.store("trialArray",trialArray)
      product_attributes=getContractProductAttributes(contract_id)
      information_attributes=getContractInformationAttributes(contract_id)
      contract_info_hash.store("product_attributes",product_attributes)
      contract_info_hash.store("information_attributes",information_attributes)
      return contract_info_hash
    end
    
    
    def getVersionArrayObject(trial_id)
      versionArray = Array.new
      versions = Contract.get_versions(trial_id)
      versions.each {|version|
        version_info_hash = Hash.new
        version_info_hash.store("version_id",version.version_id)
         marketing_vehicle_name=Api::ContractServices.getMarketingVehicleName(version.marketing_vehicle_id)
        version_info_hash.store("marketing_vehicle_name",marketing_vehicle_name)
         version_info_hash.store("sequence",version.sequence)
        version_info_hash.store("marketing_vehicle_id",version.marketing_vehicle_id)
        version_info_hash.store("ad_size",version.ad_size)
        version_info_hash.store("zone_type",version.zone_type)
        version_info_hash.store("zone_value",version.zone_value)
        version_info_hash.store("version_store",Api::ContractServices.getVersionsStore(version.version_id))
        version_stores=Api::ContractServices.getVersionsStore(version.version_id)
        
        
        logger.info"@version_stores #{Api::ContractServices.getVersionsStore(version.version_id)}"
        version_info_hash.store("tierArray", Api::ContractServices.getTierArrayObject(version.version_id))
        versionArray.push(version_info_hash)
      }
      return versionArray
    end
    
    def getVersionsStore(version_id)
         
          conn = ActiveRecord::Base.connection
          version_stores = conn.execute("SELECT banner_abbr ,description from stores where store_id IN(SELECT store_id FROM version_stores WHERE version_id ='#{version_id}')" )
          logger.info"@version_stores #{version_stores}"
          return version_stores
     
    end
    
    def getTierArrayObject(version_id)
      tierArray = Array.new
      tiers = Contract.get_tier_by_version_id(version_id)
      tiers.each {|tier|
        tier_info_hash = Hash.new
        dates=tier.tier_dates
        if(!dates.blank?)
          startDate=dates.first
          endDate=dates.end
        else
          startDate=""
          endDate=""
        end
        tier_info_hash.store("tier_id",tier.tier_id)
        tier_info_hash.store("start_date",startDate)
        tier_info_hash.store("end_date",endDate)
        tier_info_hash.store("price_vehicle_id",tier.price_vehicle_id)
        tier_info_hash.store("sequence",tier.sequence)
        tier_info_hash.store("funding",tier.funding)
        tier_info_hash.store("vendor_info",tier.vendor_info)
        tier_info_hash.store("unit_funding",tier.unit_funding)
        tier_info_hash.store("lumpsum_funding",tier.lumpsum_funding)
        
        data = Api::ContractServices.getMinMaxForTier(tier.tier_id)
        tier_info_hash.store("minmax_base_cost",data[0]["base_cost"])
        tier_info_hash.store("minmax_base_price",data[0]["base_price"])
        tier_info_hash.store("minmax_effective_price",data[0]["effective_price"])
        tier_info_hash.store("minmax_net_cost",data[0]["net_cost"])
        tier_info_hash.store("minmax_unit_funding",data[0]["unit_funding"])
        
        tier_info_hash.store("tierProductArray", Api::ContractServices.getTierProductInfoArrayObject(tier.tier_id))
         if(!tier.price_vehicle_id.blank?) 
             tier_info_hash.store("priceVehicleDescription", PriceVehicle.find_by_id(tier.price_vehicle_id).description) 
           else
              tier_info_hash.store("priceVehicleDescription", "") 
         end 
        tierArray.push(tier_info_hash)
      }
      return tierArray
    end
    
  def getMinMaxForTier(tier_id)
    conn = ActiveRecord::Base.connection
    res = conn.execute("select ( min(base_cost),max(base_cost)) AS base_cost,(min(base_price),max(base_price)) AS base_price, (min(effective_price),max(effective_price))AS effective_price,(min(cost),max(cost))AS net_cost,(min(unit_funding),max(unit_funding))AS unit_funding from tier_products where tier_id=#{tier_id}")
    return res
  end    

  def getTierProductInfoArrayObject(tier_id)
    tier_products_info=Array.new
    if(tier_id!=nil)
      tier_products = TierProduct.where("tier_id=#{tier_id}")     
      tier_products.each do |tier_product|
        product = Product.find_by_product_id(tier_product.product_id)
        product_info=Hash.new()
        product_info.store('product_id',product.product_id)
        product_info.store('product_name',product.upc_desc)
        product_info.store('product_desc',product.sku_desc)
        product_info.store('base_cost',tier_product.base_cost)
        product_info.store('base_price',tier_product.base_price)
        product_info.store('unit_funding',tier_product.unit_funding)
        product_info.store('cost',tier_product.cost)
        product_info.store('effective_price',tier_product.effective_price)
        product_info.store('price_candidate_list',tier_product.price_candidate_list)
        product_costs_range=getContractProcuctCostsRange(tier_product.product_id,tier_product.trial_id)
        logger.info { "product_costs_range =#{product_costs_range}" }
        product_info.store('product_costs_range',product_costs_range)
        tier_products_info.push(product_info)
        logger.info { "1product_data =#{product_info}" }
       end
       logger.info { "contract @product_data =#{tier_products_info}" }
      return tier_products_info
    else
    return nils
    end
  end
##############################################################################################################
  def getAllAttributeFromCustomAttributesByTableName(table_name)
     conn = ActiveRecord::Base.connection
     attributes = conn.execute("select * from custom_attributes where table_name = '#{table_name}'")
     return attributes
  end

##############################################################################################################

def getContractProcuctCostsRange(product_id,trial_id)
     conn = ActiveRecord::Base.connection
     res = conn.execute("select ( min(base_cost),max(base_cost)) AS base_cost,(min(base_price),max(base_price)) AS base_price, (min(effective_price),max(effective_price))AS effective_price,(min(cost),max(cost))AS net_cost,(min(unit_funding),max(unit_funding))AS unit_funding from tier_products where product_id='#{product_id}' AND trial_id='#{trial_id}' ")
     res.each do |row|
      base_cost_range=row['base_cost']
      base_price_range=row['base_price']
      effective_price_range=row['effective_price']
      net_cost_range=row['net_cost']
      unit_funding_range=row['unit_funding']
      product_costs_range=Hash.new()
       product_costs_range.store('base_cost_range',base_cost_range)
       product_costs_range.store('base_price_range',base_price_range)
       product_costs_range.store('effective_price_range',effective_price_range)
       product_costs_range.store('net_cost_range',net_cost_range)
       product_costs_range.store('unit_funding_range',unit_funding_range)
      logger.info { " product_costs_range =#{  product_costs_range}" }
       return product_costs_range
    end
     
    
end

##############################################################################################################

    def getMarketingVehicleName(marketing_vehicle_id)
      if(!marketing_vehicle_id.blank?)
      conn = ActiveRecord::Base.connection
      res = conn.execute("select marketing_vehicle_name from marketing_vehicle where id=#{marketing_vehicle_id}")
     
      res.each {|row|
        @marketing_vehicle_name=row['marketing_vehicle_name']
      }
      
      return @marketing_vehicle_name
      else
      return ''
      end
  end
#####################################
    def getContractProductAttributes(contract_id)
      conn = ActiveRecord::Base.connection
      res = conn.execute("select ca.contract_id,ca.product_id, ca.value,c.name from contract_attributes ca,custom_attributes c where c.id=ca.attribute_id and product_id IS NOT NULL and ca.contract_id=#{contract_id} ")
      product_attributes = Array.new
      res.each {|row|
        
        product_attributes_info = Hash.new
        product_attributes_info.store("contract_id",row['contract_id'])
        product_attributes_info.store("product_id",row['product_id'])
        product=Product.find_by_product_id(row['product_id'])
        product_attributes_info.store('product_name',product.upc_desc)
        product_attributes_info.store("value",row['value'])
        product_attributes_info.store("name",row['name'])
        product_attributes.push(product_attributes_info)
        logger.info"product_attributes1 #{product_attributes}"
        }
      
      return product_attributes
    end
 ########################################  
   def getContractInformationAttributes(contract_id)
      conn = ActiveRecord::Base.connection
      res = conn.execute("select ca.contract_id, ca.value,c.name from contract_attributes ca,custom_attributes c where c.id=ca.attribute_id and product_id IS  NULL and ca.contract_id=#{contract_id} ")
      information_attributes = Array.new
      res.each {|row|
       
        information_attributes_info = Hash.new
        information_attributes_info.store("contract_id",row['contract_id'])
        information_attributes_info.store("value",row['value'])
        information_attributes_info.store("name",row['name'])
        information_attributes.push(information_attributes_info)
        
        logger.info"information_attributes1 #{information_attributes}"
      }
      
      return information_attributes
  end
   
   ######################################## 
     

  def addContractAttribute(params)
      conn = ActiveRecord::Base.connection
    contract_id = params[:contract_id]
    column_count = params[:column_count]
    product_count = params[:product_count]
    
    params[:contract_attributes].each {|contract_attribute|
      if !contract_attribute["value"].blank?
        conn.execute("insert into contract_attributes(contract_id,attribute_id,value) values (#{contract_id},#{contract_attribute["attribute_id"]},'#{contract_attribute["value"]}')")
      end
    }
    
     i = 0
    j = 1
    params[:contract_product_attribute].each {|contract_product|
       if !contract_product["value"].blank?
        conn.execute("insert into contract_attributes(contract_id,attribute_id,product_id,value) values (#{contract_id},#{contract_product["attribute_id"]},#{params[:product_id][i]},'#{contract_product["value"]}')")
       end
       if(j%column_count.to_i == 0)
        i += 1
      end 
      j +=1
    }  
  
   
    
  end

##############################################################################################################
  def addPriceVehicalInfoToContract(params)
    logger.info"in price vehicle"
    logger.info"in price vehicle1"
     count = 0
     demoArray =  Array.new
     demoArray1 =  Array.new
     if !params[:version_sequence].blank?
       params[:version_sequence].each {|data|
         demoArray.push(data.split("-").first.to_i)
         demoArray1.push(data.split("-").last.to_i)
       }
     end
     flag = 0
    params[:tier_id].each {|id|
      logger.info"in price vehicle2 $#{id} $#{count}"
      temp = params[:priceVehicle][count]["name"].split("-")
      name = temp.last
      version_no = temp.first.to_i
      tier_no = temp[1].to_i
      price_vehicle_id = temp[2]
      type = params[:priceAssignmentType][count]["name"].split("-").last
      if(demoArray1.include?(tier_no))
        position = demoArray1.index(tier_no)
        version_seq = demoArray[position]
        tier_seq = demoArray1[position]
        if(version_seq == version_no)
          flag = 1
        else
          count +=1
          next
        end
      end
      

      tier= Tier.find_by_tier_id(id)
      tier.price_vehicle_id = price_vehicle_id
      tier.reward_points = params[:rewardPoint][count]
      tier.save!
      
      @tier_products = TierProduct.where("tier_id=#{id}").order("id ASC")
      product_count = @tier_products.count
      logger.info"tier_no#{tier_no}"
      # also use version no for calculating param_no
      param_no = count * product_count
      
      logger.info"----------$$$$$$--- #{name+type}"
      
      if name == "bogaOpt" || name == "custPricingOpt"
        type =""
      end
      
      case name+type
 
      when "flatOptimizeOptsku"
        @tier_products.each do |tier_product|
          price_hash = Hash.new()
          price_hash.store("vehical",name);
          price_hash.store("type",type);
          logger.info"In loop tier_no#{tier_no}"
          price_hash.store("flat_price",params[:flatOptimizeOpt][param_no][:flatPrice])
          tier_product.effective_price = params[:flatOptimizeOpt][param_no][:flatPrice]
          tier_product.price_candidate_list = price_hash.to_json
          tier_product.save!
          logger.info"flatprice#{price_hash}"
          param_no += 1
        end
        
      when "flatOptimizeOptcontract"
        price_hash = Hash.new()
        price_hash.store("vehical",name);
        price_hash.store("type",type);
        price_hash.store("flat_price",params[:flatOptimizeOptContract][count][:flatPrice])
        @tier_products.each do |tier_product|
          tier_product.effective_price = params[:flatOptimizeOptContract][count][:flatPrice]
          tier_product.price_candidate_list = price_hash.to_json
          tier_product.save!
          logger.info"flatpriceContract#{price_hash}"
        end
          
       #EO when flatOptimizeOpt         
      when "priceMultipleOptsku"
        @tier_products.each do |tier_product|
          price_hash = Hash.new()
          price_hash.store("vehical",name);
          price_hash.store("type",type);
          logger.info"In loop tier_no#{tier_no}"
          qty = params[:priceMultipleOpt][param_no][:qty].to_f
          price = params[:priceMultipleOpt][param_no][:price].to_f
          tier_product.effective_price = ( price / qty )
          price_hash.store("qty",qty)
          price_hash.store("price",price)
          tier_product.price_candidate_list = price_hash.to_json
          tier_product.save!
          logger.info"priceMultipleOpt#{price_hash}"
          param_no += 1
        end
        
      when "priceMultipleOptcontract"
          price_hash = Hash.new()
          price_hash.store("vehical",name);
          price_hash.store("type",type);
          logger.info"In loop tier_no#{tier_no}"
          qty = params[:priceMultipleOptContract][count][:qty].to_f
          price = params[:priceMultipleOptContract][count][:price].to_f
          price_hash.store("qty",qty)
          price_hash.store("price",price)
          price_hash = price_hash.to_json
        @tier_products.each do |tier_product|
          tier_product.effective_price = ( price / qty )
          tier_product.price_candidate_list = price_hash
          tier_product.save!
          logger.info"priceMultipleOptContract#{price_hash}"
        end
      
      when "percentOffOptsku"
        @tier_products.each do |tier_product|
          price_hash = Hash.new()
          price_hash.store("vehical",name);
          price_hash.store("type",type);
          logger.info"In loop tier_no#{tier_no}"
          percentOff = params[:percentOffOpt][param_no][:percentOff].to_f
          if !tier_product.base_price.blank?
            tier_product.effective_price = tier_product.base_price - ( tier_product.base_price * percentOff / 100 ) 
          else
            tier_product.effective_price = ""           
          end
          price_hash.store("percentOff", percentOff)
          tier_product.price_candidate_list = price_hash.to_json
          tier_product.save!
          logger.info"percentOffOpt#{price_hash}"
          param_no += 1
        end     

      when "percentOffOptcontract"
        price_hash = Hash.new()
        price_hash.store("vehical",name);
        price_hash.store("type",type);
        logger.info"In loop tier_no#{tier_no}"
        percentOff = params[:percentOffOptContract][count][:percentOff].to_f
        price_hash.store("percentOff", percentOff) 
        price_hash = price_hash.to_json
 
        @tier_products.each do |tier_product|
          if !tier_product.base_price.blank?
            tier_product.effective_price = tier_product.base_price - ( tier_product.base_price * percentOff / 100 ) 
          else
            tier_product.effective_price = ""               
          end
          tier_product.price_candidate_list = price_hash
          tier_product.save!
          logger.info"percentOffOptContract#{price_hash}"
        end 
        
      when "AmtOffOptsku"
        @tier_products.each do |tier_product|
          price_hash = Hash.new()
          price_hash.store("vehical",name);
          price_hash.store("type",type);
          logger.info"In loop tier_no#{tier_no}"
          amtOff = params[:AmtOffOpt][param_no][:amtOff].to_f
          if !tier_product.base_price.blank?
            tier_product.effective_price = tier_product.base_price - amtOff
          else
            tier_product.effective_price = ""
          end
          price_hash.store("amtOff",amtOff)
          tier_product.price_candidate_list = price_hash.to_json
          tier_product.save!
          logger.info"AmtOffOpt#{price_hash}"
          param_no += 1
        end  
        
      when "AmtOffOptcontract"
        price_hash = Hash.new()
        price_hash.store("vehical",name);
        price_hash.store("type",type);
        logger.info"In loop tier_no#{tier_no}"
        amtOff = params[:AmtOffOptContract][count][:amtOff].to_f
        price_hash.store("amtOff",amtOff)
        price_hash = price_hash.to_json
        @tier_products.each do |tier_product|
          if !tier_product.base_price.blank?
            tier_product.effective_price = tier_product.base_price - amtOff
          else
            tier_product.effective_price = ""
          end
          tier_product.price_candidate_list = price_hash
          tier_product.save!
          logger.info"AmtOffOptContract#{price_hash}"
        end   

       
       when "bogoOptsku"
        @tier_products.each do |tier_product|
          price_hash = Hash.new()
          price_hash.store("vehical",name);
          price_hash.store("type",type);
          buy = params[:bogoOpt][param_no][:buy].to_f
          get = params[:bogoOpt][param_no][:get].to_f
          offType = params[:bogoOpt][param_no][:offType]
          price = params[:bogoOpt][param_no][:price].to_f
          price_hash.store("buy",buy)
          price_hash.store("get",get)
          price_hash.store("offType",offType)
          price_hash.store("price",price)
          effective_price = 0
          total_product = buy + get
          price_json = price_hash.to_json
          if !tier_product.base_price.blank?
            if(offType == "% off") 
              tier_product.effective_price = ( (buy * tier_product.base_price) + ( (100 - price) * tier_product.base_price * get / 100 ) ) / total_product
            elsif(offType == "Amount off") 
              tier_product.effective_price = ( (buy * tier_product.base_price) + (  get * (tier_product.base_price - price)) ) / total_product
            elsif(offType == "Flat amount") 
              tier_product.effective_price = ( (buy * tier_product.base_price) + ( get *  price) ) / total_product
            end
          else
            tier_product.effective_price = ""
          end
          
          logger.info"In loop tier_no#{tier_no}"
          tier_product.price_candidate_list = price_json
          tier_product.save!
          logger.info"bogoOpt#{price_hash}"
          param_no += 1
        end 
        
      when "bogoOptcontract"
        price_hash = Hash.new()
        price_hash.store("vehical",name);
        price_hash.store("type",type);
        buy = params[:bogoOptContract][count][:buy].to_f
        get = params[:bogoOptContract][count][:get].to_f
        offType = params[:bogoOptContract][count][:offType]
        price = params[:bogoOptContract][count][:price].to_f
        price_hash.store("buy",buy)
        price_hash.store("get",get)
        price_hash.store("offType",offType)
        price_hash.store("price",price)
        effective_price = 0
        total_product = buy + get
        price_json = price_hash.to_json
        @tier_products.each do |tier_product|
          if !tier_product.base_price.blank?
            if(offType == "% off") 
              tier_product.effective_price = ( (buy * tier_product.base_price) + ( (100 - price) * tier_product.base_price * get / 100 ) ) / total_product
            elsif(offType == "Amount off") 
              tier_product.effective_price = ( (buy * tier_product.base_price) + (  get * (tier_product.base_price - price)) ) / total_product
            elsif(offType == "Flat amount") 
              tier_product.effective_price = ( (buy * tier_product.base_price) + ( get *  price) ) / total_product
            end
          else
            tier_product.effective_price = ""
          end
          logger.info"In loop tier_no#{tier_no}"
          tier_product.price_candidate_list = price_json
          tier_product.save!
          logger.info"bogoOptContract#{price_hash}"
        end
       
       when "bogaOpt"
         buyProduct =  Array.new()
         getProduct =  Array.new()
         params["dataArrayBuy#{version_no}#{tier_no}"].each do |data|
           buyProduct << data["buy"]
         end
         params["dataArrayGet#{version_no}#{tier_no}"].each do |data|
            getProduct << data["get"]
         end    
         price_hash = Hash.new()   
         price_hash.store("vehical",name);
         price_hash.store("buyProduct",buyProduct)
         price_hash.store("getProduct",getProduct) 
         price_hash.store("buy",params[:bogaOpt][count][:buy])
         price_hash.store("get",params[:bogaOpt][count][:get])
         price_hash.store("offType",params[:bogaOpt][count][:offType])
         price_hash.store("price",params[:bogaOpt][count][:price])
         @tier_products.each do |tier_product|
            tier_product.price_candidate_list = price_hash.to_json
            tier_product.save!
            logger.info"progressiveOpt#{price_hash}"
          end         

      when "progressiveOptsku"
        prgressive_count = 0
        logger.info"prog #{params["progressiveOpt#{tier_no}0"]}"
        @tier_products.each do |tier_product|
          price_hash = Hash.new()
          price_hash.store("vehical",name);
          price_hash.store("type",type);
          logger.info"In loop tier_no#{tier_no}"
          data_array = params["progressiveOpt#{version_no}#{tier_no}#{prgressive_count}"]
          effective_price = 0
          data_array.each do |data|
            effective_price += (data["price"].to_f / data["qty"].to_f)
          end
          tier_product.effective_price = effective_price / data_array.length
          price_hash.store("data",params["progressiveOpt#{version_no}#{tier_no}#{prgressive_count}"].to_json)
          tier_product.price_candidate_list = price_hash.to_json
          tier_product.save!
          logger.info"progressiveOpt#{price_hash}"
          prgressive_count +=1
        end
        
      when "progressiveOptcontract"
        price_hash = Hash.new()
        price_hash.store("vehical",name);
        price_hash.store("type",type);
        logger.info"In loop tier_no#{tier_no}"
        data_array = params["progressiveOptContract#{version_no}#{tier_no}"]
        effective_price = 0
        data_array.each do |data|
          effective_price += (data["price"].to_f / data["qty"].to_f)
        end
        data = params["progressiveOptContract#{version_no}#{tier_no}"].to_json
        price_hash.store("data",data)
        price_hash = price_hash.to_json
        @tier_products.each do |tier_product|
          tier_product.effective_price = effective_price / data_array.length
          tier_product.price_candidate_list = price_hash
          tier_product.save!
          logger.info"progressiveOptContract#{price_hash}"
        end

      when "custPricingOpt"
        @tier_products.each do |tier_product|
          price_hash = Hash.new()
          price_hash.store("vehical",name);
          logger.info"In loop tier_no#{tier_no}"
          tier_product.effective_price = params[:custPricingOpt][param_no][:price]
          price_hash.store("price",params[:custPricingOpt][param_no][:price])
          tier_product.price_candidate_list = price_hash.to_json
          tier_product.save!
          logger.info"custPricingOpt#{price_hash}"
          param_no += 1
        end
        
      end #EO cases
      
      if(flag == 1)
         all_tiers = Tier.where("version_id in (#{params[:version_id].join(",")}) and sequence = #{tier_no}")
         all_tiers.each {|tier_object|
          if(tier_object.tier_id != tier.tier_id)
              tier_object.price_vehicle_id = price_vehicle_id
              tier_object.reward_points = params[:rewardPoint][count]
              tier_object.save!
              
              all_tier_products = TierProduct.where("tier_id=#{tier_object.tier_id}").order("id ASC")
              all_product_position = 0
              all_tier_products.each do |product|
                product.effective_price = @tier_products[all_product_position].effective_price
                product.price_candidate_list = @tier_products[all_product_position].price_candidate_list
                product.save!
                all_product_position += 1
              end          
          end  
         } 
      end  
 
      flag = 0      
      count +=1
    }
  end

##############################################################################################################
    
  end
end

