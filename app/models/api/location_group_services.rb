class Api::LocationGroupServices < ActiveRecord::Base
  ################################################################################################################################################################
 class << self
  #It adds a new GeoGroup
    def addLocationGroup(location_group, user, params)
      custom_attr_id = params[:custom_attr_id] unless params[:custom_attr_id].blank?
      custom_attr_val = params[:custom_attr_val] unless params[:custom_attr_val].blank?
      location_attribute = Hash.new
      location_attribute.store(custom_attr_id, custom_attr_val) 
      location_group.location_trail = Api::LocationGroupServices.createLocationTrail(params[:location_id]).to_json unless params[:location_id].blank?
      location_group.location_attribute = location_attribute unless location_attribute.blank?
      location_group.created_by = user.id
      location_group.lids = params[:storeIdArray] unless params[:storeIdArray].blank?
    if location_group.save
     return true
    else
     return false
    end
  end

  ################################################################################################################################################################
  #It Updates GeoGroup
  def updateLocationGroup(location_group, user, location_group_params ,params)
    custom_attr_id = params[:custom_attr_id] unless params[:custom_attr_id].blank?
    custom_attr_val = params[:custom_attr_val] unless params[:custom_attr_val].blank?
    location_attribute = Hash.new
    location_attribute.store(custom_attr_id, custom_attr_val) 
    location_group.location_trail = Api::LocationGroupServices.createLocationTrail(params[:location_id]).to_json unless params[:location_id].blank?
    location_group.location_attribute = location_attribute unless location_attribute.blank?
    location_group.updated_by = user.id
    location_group.lids = params[:storeIdArray] unless params[:storeIdArray].blank?
    if location_group.update(location_group_params)
    return true
    else
    return false
    end
  end

  ################################################################################################################################################################
  # It Removes a GeoGroup
  def removeLocationGroup(location_group)
    if location_group.destroy
    return true
    else
    return false
    end
    # conn = ActiveRecord::Base.connection
    #  if conn.execute("DELETE from location_groups where id =#{location_group.id})")
    #   return true
    #  else
    #   return false
    #  end
  end
    
  def removeBulkLocationGroup(groupIds)  
    if LocationGroup.where("id in (#{groupIds.join(",")})").destroy_all
    return true
    else
    return false
    end
   # conn = ActiveRecord::Base.connection
   # logger.info"+++inside LocationGroupServices and ids = #{groupIds}"
   #   if conn.execute("DELETE from location_groups where id in (#{groupIds.join(",")})")
   #    return true
   #   else
   #    return false
   #   end
  end

  ################################################################################################################################################################
  def getLocationGroupByIds(groupIds)
  # conn = ActiveRecord::Base.connection
  # res = conn.execute("SELECT * from location_groups where id in (#{groupIds.join(",")}) ")
  res = LocationGroup.where("id in (#{groupIds.join(",")})")
  return res
end

  # get all GeoGroups
  # def getAllLocationGroups
  #    # conn = ActiveRecord::Base.connection
  #    # res = conn.execute("SELECT * from location_groups")
  #    res = LocationGroup.all
  #    return res
  # end

   def getAllLocationsFromLocationGroup(id)
    conn = ActiveRecord::Base.connection
    res = conn.execute("select lids from location_groups where id = #{id}")
    return res
  end 
  #################################################################################################################
  def getStoresByLocationGroupId(id)
    conn = ActiveRecord::Base.connection
    # if id!=nil
      storesArray = []
      location_trails = conn.execute("select location_trail from location_groups where id = #{id}")
    # end  
      if !location_trails.blank?
        location_trails.each{|k,v|
          if v.is_a? Hash
            v.each{|i,j|
              stores = Api::LocationServices.getStoresByBanner(i)
              storesArray << stores
            }
          end
        }
      end
    return storesArray    
  end

  def getAllLocationGroupsInHash
   location_groups_array = []
   # location_groups = getAllLocationGroups
   location_groups = LocationGroup.all
    if !location_groups.blank?
    location_groups.each do |location_group|
      finalHash = Hash.new
      key = [location_group['id'], location_group['name']]
      logger.debug{"--------key-----#{key}"}
      value = []
      locationForGroup = getAllLocationsFromLocationGroup(location_group['id'])
      if !locationForGroup.blank?
        locationForGroup.each do |row|
          arrayOfLids = row['lids']
          arrayOfLids = arrayOfLids.gsub("{","")
          arrayOfLids = arrayOfLids.gsub("}","")
          arrayOfLids = arrayOfLids.split(",").map {|s| s.to_i}
          locationArrayReturn = Api::LocationServices.getStoresByIds(arrayOfLids)
          locationArrayReturn.each  do |everyRow|
            value << everyRow
          end
        end
      end
      finalHash.store(key, value);
      location_groups_array << finalHash
     end
    end
    return location_groups_array
  end

  ################################################################################################################################################################
  # get a GeoGroup
  def getLocationGroupByname(name)
    res =  $conn.connection.execute [
      "select gg.name, gg.description,gg.geo_trail, gg.custom_attribute ,hd.description from geo_groups gg ",
      "inner join geo_hier gh on true=true ",
      "inner join hierarchy_descr hd on gh.store_id = CAST(hd.value AS Int) ",
      "where gh.super_banner_id IN(CAST(gg.geo_trail#>>'{SuperBanner}' AS Int)) ",
      "and gh.banner_id IN(CAST(gg.geo_trail#>>'{Banner}' AS Int)) ",
      "and hd.level like '%Store_id%' and (gg.name='#{name}' or #{name==nil}) ",
    ].join(' ')
    puts(res)
    puts "--------------------------"
    puts "|Group|"
    puts "--------------------------"
    res.each { |row|

      puts "|  #{row}"
    }
    puts "-----------end---------------"
    return res
  end

  ################################################################################################################################################################
  # get a CreateGeoTrail
  def createLocationTrail(id)
    conn = ActiveRecord::Base.connection
    res = conn.execute("select parent_id from location_hier where id = #{id}")
    logger.info"+++res will contains #{res}"
    h = Hash.new()
    arr = []
    res.each do |r|
      parent=r['parent_id']
      if parent!=nil
        hashList = Api::LocationGroupServices.createLocationTrail(parent)
      arr << hashList
      else
        h.store(id, "parent")
      end
    end
    h.store(id, (arr.reduce Hash.new, :merge))

    return h
  end

end
# def createLids(id)
#   conn = ActiveRecord::Base.connection
#   stores = Api::LocationServices.getStoresByBanner(params[:location_id])
#   storeIdArray = []
#   if !stores.blank?
#     stores.each do |store|
#       storeIdArray << store.store_id
#     end
#   end
#   storeIdArray
# end

#----EO self class
################################################################################################################################################################
end
