class Api::TierServices < ActiveRecord::Base
  
  def self.createTier()
    
  end
  
  def self.storeProducts(contract, params)
    storeProducts
    logger.debug{"---------------in store products----------"}
    productIds = params[:productIds]
    contract_id = @@contract_id
    localcontract = Contract.find_by_contract_id(contract_id)
    @tier_id = @@tier_id
    productIds.each{|product|
      @tier_product = TierProduct.find_by_tier_id_and_product_id(@tier_id,product)
      if @tier_product.blank?
        @tier_product = TierProduct.new
      @tier_product.tier_id = @@tier_id
      @tier_product.trial_id = localcontract.trial_id
      @tier_product.contract_id = localcontract.contract_id
      @tier_product.product_id = product
      @tier_product.save!
      else
      end
    }
    prodList = Array.new
    trProdList = TierProduct.where("tier_id = #{@@tier_id}")
    trProdList.each{|row|
      logger.debug{"----------------#{(row['product_id']).to_s}"}
      prodList.push(row['product_id'])
    }

    productObjects = Api::ProductServices.getProductsByIds(prodList)
    respond_to do |format|
      format.json { render json: productObjects, status: :ok}
      format.js
    end
  end
  
  def storeProductGroups
    
    
  end
  
  def self.createTier
    
    contract_id = @@contract_id
    trial_id=@@trial_id
    by_product=params[:by_product]
    by_contract=params[:by_contract]

    contract_description=params[:contract_description]
    logger.info { "finally in the logger =#{by_product},#{by_contract}" }

    #converting dates into date range

    vender_funding_dates=params[:vender_funding_dates]

    puts vender_funding_dates
    dates = vender_funding_dates.split("-")
    logger.info { "finally dates =#{dates[0].gsub(" ","")}" }
    start_date= Date.strptime(dates[0].gsub(" ",""), '%m/%d/%Y')
    end_date= Date.strptime(dates[1].gsub(" ",""), '%m/%d/%Y')
    tier_dates=("[#{start_date},#{end_date})")
    logger.info { "finally in the loggers =#{start_date},#{end_date}" }

    #creating vendor information hash
    name=params[:vendor_name]
    id=params[:vendor_id]
    description= params[:vendor_description]
    vendor_info=Hash.new()
    vendor_info.store('vendor_name', name)
    vendor_info.store('vendor_id', id)
    vendor_info.store('vendor_description', description)

    #creating funding hash
    off_invoice=params[:off_invoice]
    bill_back=params[:bill_back]
    scan=params[:scan]
    subsidy_off_invoice=params[:subsidy_off_invoice]
    subsidy_bill_back=params[:subsidy_bill_back]
    subsidy_scan=params[:subsidy_scan]
    base_cost=0
    funding={'off_invoice' => off_invoice , 'bill_back' => bill_back, 'scan' => scan,'subsidy_bill_back' => subsidy_bill_back,'subsidy_off_invoice' => subsidy_off_invoice,'subsidy_scan' => subsidy_scan}

    #creating unit funding for tier_products
    unit_funding = ((off_invoice.to_f) - (0.2 * (base_cost - off_invoice.to_f) + bill_back.to_f + scan.to_f) + (subsidy_off_invoice.to_f + subsidy_bill_back.to_f + subsidy_scan.to_f))

    #creating vendor funding money tier_products
    vendor_funding_money=off_invoice.to_f+bill_back.to_f+scan.to_f

    #creating cost for tier_products
    net_cost=base_cost-unit_funding

    #creating  subsidy funding money tier_products
    subsidy_funding_money=subsidy_off_invoice.to_f+subsidy_bill_back.to_f+subsidy_scan.to_f

    #updating tier
    @contract=Contract.where("contract_id=#{contract_id}")
    @contract.each do |contract|
      contract.description=contract_description
      contract.save

    end

    @tier=Contract.get_tier(contract_id)
    logger.info { "finally in the logger =#{@tier}" }
    @tier.each do |tier|
      logger.info { "finally in the logger =#{tier.contract_id}" }
      tier.tier_dates=tier_dates
      tier.vendor_info=vendor_info.to_json
      tier.funding=funding.to_json
      tier.unit_funding=unit_funding.to_f
      tier.save

      #updating tier_products
      @parmeter=0
      @tier_products=Contract.get_tier_product(trial_id)
      @tier_products.each do |tier_products|
        if(by_product==nil)
        tier_products.product_funding=funding.to_json
        tier_products.unit_funding=unit_funding.to_f
        tier_products.vendor_funding=vendor_funding_money
        tier_products.subsidy_funding=subsidy_funding_money
        tier_products.cost=net_cost
        tier_products.save

        else
          logger.info { "finally in the logger =#{params[:product_funding][@parmeter]}" }

          #creating funding hash
          logger.info { "finally tier =#{params[:product_funding][@parmeter][:off_invoice]}"}
          off_invoice=params[:product_funding][@parmeter][:off_invoice]
          bill_back=params[:product_funding][@parmeter][:bill_back]
          scan=params[:product_funding][@parmeter][:scan]
          subsidy_off_invoice=params[:product_funding][@parmeter][:subsidy_off_invoice]
          subsidy_bill_back=params[:product_funding][@parmeter][:subsidy_bill_back]
          subsidy_scan=params[:product_funding][@parmeter][:subsidy_scan]
          base_cost=0
          funding={'off_invoice' => off_invoice , 'bill_back' => bill_back, 'scan' => scan,'subsidy_bill_back' => subsidy_bill_back,'subsidy_off_invoice' => subsidy_off_invoice,'subsidy_scan' => subsidy_scan}

          #creating unit funding for tier_products
          unit_funding = ((off_invoice.to_f) - (0.2 * (base_cost - off_invoice.to_f) + bill_back.to_f + scan.to_f) + (subsidy_off_invoice.to_f + subsidy_bill_back.to_f + subsidy_scan.to_f))

          #creating vendor funding money tier_products
          vendor_funding_money=off_invoice.to_f+bill_back.to_f+scan.to_f

          #creating cost for tier_products
          net_cost=base_cost-unit_funding

          #creating  subsidy funding money tier_products
          subsidy_funding_money=subsidy_off_invoice.to_f+subsidy_bill_back.to_f+subsidy_scan.to_f
          tier_products.product_funding=funding.to_json
          tier_products.unit_funding=unit_funding.to_f
          tier_products.vendor_funding=vendor_funding_money
          tier_products.subsidy_funding=subsidy_funding_money
          tier_products.cost=net_cost
          tier_products.save
          @parmeter=@parmeter+1

        end

      end

    end

    render :nothing => true
  end
  
  
  
end