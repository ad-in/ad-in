class Api::VersionServices < ActiveRecord::Base
  class << self
	def addVersion(params)
	  version = Version.new
      version.trial_id = params[:trial_id]
      version.marketing_vehicle_id = params[:marketing_vehicle]
      version.ad_size = params[:ad_size]
      if version.save!
        return true
      else
        return false
      end
	end

	def getVersionByTrialid(trial_id)
	  res = Version.find_by_trial_id("#{trial_id}")
	  return res
	end
	def getVersionByVersionid(version_id)
	  res = Version.find_by_version_id("#{version_id}")
	  return res
	end
  end
end

