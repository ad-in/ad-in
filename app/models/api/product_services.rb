class Api::ProductServices < ActiveRecord::Base

  def self.getAllDepartments
    conn = ActiveRecord::Base.connection
    res = conn.execute("SELECT distinct dept_desc from products")
    return res
  end

  def self.getCategoriesByDepartment(dept)
    puts 'hi services'
    conn = ActiveRecord::Base.connection
    res = conn.execute("SELECT distinct cat_desc from products where dept_desc='#{dept}'")
    res.each { |row|
      puts "#{dept}      |    #{row['cat_desc']}"
    }
    return res
  end
  
  def self.getProductsByIds(productIds)
    # conn = ActiveRecord::Base.connection
    # res = conn.execute("SELECT * from products where product_id in (#{productIds.join(",")}) ")
    res = Product.where("product_id in (#{productIds.join(",")})")
    return res
  end
  def self.getCategoriesHierarchyNames(categoryId)
    
    conn = ActiveRecord::Base.connection
    res = conn.execute("SELECT * from categories_hier where parent_id = (#{productIds.join(",")}) ")
    
  end
  

  def self.getSubCategoriesForCategories(category)
    puts 'hi services'
    conn = ActiveRecord::Base.connection
    res = conn.execute("SELECT distinct scat_desc from products where cat_desc='#{category}'")
    res.each { |row|
      puts "#{category}      |    #{row['scat_desc']}"
    }
    return res
  end

  def self.getSegmentsForSubCategory(sub_category)
    puts 'hi services'
    conn = ActiveRecord::Base.connection
    res = conn.execute("SELECT distinct seg_desc from products where scat_desc='#{sub_category}'")
    res.each { |row|
      puts "#{sub_category}      |    #{row['seg_desc']}"
    }
    return res
  end

  def self.getProductsByDepartment(department)
    puts 'hi services'
    conn = ActiveRecord::Base.connection
    res = conn.execute("SELECT product_id, upc_desc, item , item_desc, sku, sku_desc, seg, seg_desc,scat ,scat_desc,
      cat,cat_desc , sdept, sdept_desc from products where dept_desc='#{department}' order by product_id desc")
    res.each { |row|
      puts "#{row['product_id']}      |    #{row['upc_desc']} |    #{row['item']} |    #{row['item_desc']} |    #{row['sku']}|    #{row['sku_desc']} |    #{row['seg']} |    #{row['seg_desc']} |    #{row['scat']} |    #{row['scat_desc']} |    #{row['cat']} |    #{row['cat_desc']} |    #{row['sdept']}     |#{row['sdept_desc']}"
    }
    return res
  end

  def self.getProductsByCategory(category)
    currentTime = Time.now
    conn = ActiveRecord::Base.connection
    finalArray = Array.new
    @categories = getAllCategories
    rowsArrayOfHashes = Array.new
    @categories.each { |e|
      rowsArrayOfHashes.push(e)
    }
    if checkIfCategoryIsLeaf(category ,rowsArrayOfHashes)
      # res = conn.execute("SELECT * from products where category_id='#{category}' ")
      res = Product.where(:category_id => "#{category}")
      logger.debug{"[Api::ProductServices.getAllSubCategoriesForCategory Execution Time = [#{Time.now - currentTime}seconds]"}
    return res
    else
      finalArray = getAllSubCategoriesIds(category , rowsArrayOfHashes)
      p finalArray
      # res = conn.execute("SELECT * from products where category_id in (#{finalArray.join(",")}) ")
      res = Product.where("category_id in (#{finalArray.join(",")})")
      logger.debug{"[Api::ProductServices.getAllSubCategoriesForCategory Execution Time = [#{Time.now - currentTime}seconds]"}
    return res
    end
    logger.debug{"[Api::ProductServices.getAllSubCategoriesForCategory Execution Time = [#{Time.now - currentTime}seconds]"}
  end
  
  def self.getAllSubCategoriesIds(category , rowsArrayOfHashes)
    finalArray = Array.new
    childrensHash =  rowsArrayOfHashes.select { |hash|
      trail = hash['category_trail']
      if trail != nil
        trail = trail.gsub("{","")
        trail = trail.gsub("}","")
        trail = trail.split(",").map {|s| s.to_i}
      trail.include?(category.to_i)
      end
    }
    childrensHash.each{|childs|
      trail1 = childs['category_trail']
      if trail1 != nil
        trail1 = trail1.gsub("{","")
        trail1 = trail1.gsub("}","")
        trail1 = trail1.split(",").map {|s| s.to_i}
        finalArray = finalArray.concat(trail1)
        finalArray = finalArray.uniq
        if childs['depth'] == '4'
          finalArray = finalArray.push(childs['id'].to_i)
        finalArray = finalArray.uniq
        end
      end
    }
    if finalArray.include? category
      finalArray = finalArray.drop_while{|i| i < category }
    end
    return finalArray
  end
  
  def self.checkIfCategoryIsLeaf(category , rowsArrayOfHashes)
    childshash = rowsArrayOfHashes.select {|hash|
      hash['parent_id'].to_i == category.to_i
    }
    if childshash.length.zero?
    return true
    else
    return false
    end
  end

  def self.getProductsBySubCategory(subcategory)
  
    puts 'hi services'
    conn = ActiveRecord::Base.connection
    res = conn.execute("SELECT product_id, upc_desc, item , item_desc, sku, sku_desc, seg, seg_desc,scat ,scat_desc,
      cat,cat_desc , sdept, sdept_desc from products where scat_desc='#{subcategory}' order by product_id desc")
    res.each { |row|
      puts "#{row['product_id']}      |    #{row['upc_desc']} |    #{row['item']} |    #{row['item_desc']} |    #{row['sku']}|    #{row['sku_desc']} |    #{row['seg']} |    #{row['seg_desc']} |    #{row['scat']} |    #{row['scat_desc']} |    #{row['cat']} |    #{row['cat_desc']} |    #{row['sdept']}     |#{row['sdept_desc']}"
    }
    return res
  end
  
  

  def self.getProductsBySegment(segment)
    puts 'hi services'
    conn = ActiveRecord::Base.connection
    res = conn.execute("SELECT product_id, upc_desc, item , item_desc, sku, sku_desc, seg, seg_desc,scat ,scat_desc,
      cat,cat_desc , sdept, sdept_desc from products where seg_desc='#{segment}' order by product_id desc")
    res.each { |row|
      puts "#{row['product_id']}      |    #{row['upc_desc']} |    #{row['item']} |    #{row['item_desc']} |    #{row['sku']}|    #{row['sku_desc']} |    #{row['seg']} |    #{row['seg_desc']} |    #{row['scat']} |    #{row['scat_desc']} |    #{row['cat']} |    #{row['cat_desc']} |    #{row['sdept']}     |#{row['sdept_desc']}"
    }
    return res
  end

  def self.getProductsByCustomAttributes(custom_attr_ids)
    puts 'hi services'
    conn = ActiveRecord::Base.connection
    res = conn.execute("select * from products a join product_attributes b on a.product_id = b.product_id where custom_attr_id in (#{custom_attr_ids})")
    res.each { |row|
      puts "#{row['product_id']}      |    #{row['upc_desc']} |    #{row['item']} |    #{row['item_desc']} |    #{row['sku']}|    #{row['sku_desc']} |    #{row['seg']} |    #{row['seg_desc']} |    #{row['scat']} |    #{row['scat_desc']} |    #{row['cat']} |    #{row['cat_desc']} |    #{row['sdept']}     |#{row['sdept_desc']}"
    }
    return res
  end

  def self.filterProductsByCustomAttributes(product_ids, custom_attr_ids)
    puts 'hi services'
    conn = ActiveRecord::Base.connection
    res = conn.execute("select * from products a join product_attributes b on a.product_id = b.product_id where custom_attr_id in (#{custom_attr_ids})")
    res.each { |row|
      puts "#{row['product_id']}      |    #{row['upc_desc']} |    #{row['item']} |    #{row['item_desc']} |    #{row['sku']}|    #{row['sku_desc']} |    #{row['seg']} |    #{row['seg_desc']} |    #{row['scat']} |    #{row['scat_desc']} |    #{row['cat']} |    #{row['cat_desc']} |    #{row['sdept']}     |#{row['sdept_desc']}"
    }
    return res
  end
#######
  def self.getProductsFromCategoryHierarchy(categoryHash)
    puts 'hi services'
    conn = ActiveRecord::Base.connection
    res = conn.execute("select * from products a join product_attributes b on a.product_id = b.product_id where custom_attr_id in (#{custom_attr_ids})")
    res.each { |row|
      puts "#{row['product_id']}      |    #{row['upc_desc']} |    #{row['item']} |    #{row['item_desc']} |    #{row['sku']}|    #{row['sku_desc']} |    #{row['seg']} |    #{row['seg_desc']} |    #{row['scat']} |    #{row['scat_desc']} |    #{row['cat']} |    #{row['cat_desc']} |    #{row['sdept']}     |#{row['sdept_desc']}"
    }
    return res
  end

  def self.getDistinctProductAttributesForCategories(categories)
    allKeys  = Array.new
    logger.debug {"categories === #{categories}"}
    categories = categories.split(",")
    categories.each { |category|
      allSubCategories = Api::ProductServices.getAllSubCategoriesForCategory(category)
      allKeys.push(Api::ProductServices.iterateHASH(allSubCategories))
    }
    conn = ActiveRecord::Base.connection
    res = conn.execute("select custom_attr_id, value from product_attributes where category_id in (#{allKeys.join(",")}) group by value,custom_attr_id")
    return res
  end
  
  
  

  def self.getAllSubCategoriesForCategory(cat ,rowsArrayOfHashes)
    # conn = ActiveRecord::Base.connection
    # res = conn.execute("select * from categories_hier where parent_id = #{cat['id']}")
    currentTime = Time.now
    res = rowsArrayOfHashes.select {|localHash| localHash['parent_id'] == cat['id']}
    h = Hash.new()
    if res.length.zero?
      h.store([cat['id'],cat['description']], "leaf")
    else
      arr = Array.new
      res.each {|r|
        
        hashList = Api::ProductServices.getAllSubCategoriesForCategory(r,rowsArrayOfHashes)
        arr.push(hashList)
      }
      h.store([cat['id'],cat['description']], (arr.reduce Hash.new, :merge!))
    end
    
    logger.debug{"[Api::ProductServices.getAllSubCategoriesForCategory Execution Time = [#{Time.now - currentTime}seconds]"}
    return h
  end
  
  def self.getAllParentCategories
    conn = ActiveRecord::Base.connection
    res = conn.execute("select * from categories_hier where depth = 0 ")
    return res
  end
  
   def self.getAllCategories
    conn = ActiveRecord::Base.connection
    res = conn.execute("select * from categories_hier ")
    return res
   end
   
  def self.getAllCategoriesInHash
    currentTime = Time.now
    finalArray = Array.new
    @categories = getAllCategories
    rowsArrayOfHashes = Array.new
    @categories.each { |e|
      rowsArrayOfHashes.push(e)
    }
    if hashes = rowsArrayOfHashes.select {|hash| hash['depth'] == '0'}
     hashes = hashes.sort {|a,b| a['description'] <=> b['description']}
      if hashes.is_a?(Array)
        hashes.each { |h|
         childrensHash =  rowsArrayOfHashes.select { |hash| 
          trail = hash['category_trail']
          if trail != nil
            trail = trail.gsub("{","")
            trail = trail.gsub("}","")
            trail = trail.split(",").map {|s| s.to_i} 
            trail[0] == h['id'].to_i
          end
          }
         result = getAllSubCategoriesForCategory( h , childrensHash)
         finalArray.push(result)
        }
        else
          finalArray.push(getAllSubCategoriesForCategory( hashes , rowsArrayOfHashes))
      end
    else
      p 'not found'
    end
    logger.debug{"[Api::ProductServices.getAllCategoriesInHash Execution Time = [#{Time.now - currentTime}seconds]"}
    return finalArray
  end

   def self.iterateHASH(h)
    arr = Array.new
    h.each do |k,v|
    arr.push(k)
    value = v || k
    if value.is_a?(Hash)
      puts "evaluating: #{value} recursively..."
      arr.push(Api::ProductServices.iterateHASH(value))
    else
     puts v ? "key: #{k} value: #{v}" : "array value #{k}"
    end
  end
  return arr
  end
  
  
  
  ###################################################################################################  
  
  def self.getAllProductAttributes
    conn = ActiveRecord::Base.connection
    res = conn.execute("select id ,name ,value ,description from custom_attributes where id in(select distinct custom_attr_id from  custom_attributes c ,product_attributes p  where p.custom_attr_id=c.id)")
    return res
  end


####################################################################################################
def self.getAllProductAttributesForCategory1(products)
    conn = ActiveRecord::Base.connection
    res = conn.execute("select name ,value,id from custom_attributes where id in(select custom_attr_id from product_attributes where product_id in(#{products} ))")
    return res
end

####################################################################################################


def self.getAllProductAttributesForCategory(category)
    currentTime = Time.now
    conn = ActiveRecord::Base.connection
    finalArray = Array.new
    @categories = getAllCategories
    rowsArrayOfHashes = Array.new
    @categories.each { |e|
      rowsArrayOfHashes.push(e)
    }
    if checkIfCategoryIsLeaf(category ,rowsArrayOfHashes)
      res = conn.execute("select name ,value,id from custom_attributes where id in(select custom_attr_id from product_attributes where product_id in (SELECT product_id from products where category_id='#{category}')) ")
      logger.debug{"[Api::ProductServices.getAllSubCategoriesForCategory Execution Time = [#{Time.now - currentTime}seconds]"}
    return res
    else
      finalArray = getAllSubCategoriesIds(category , rowsArrayOfHashes)
      p finalArray
      res = conn.execute("select name ,value,id from custom_attributes where id in(select custom_attr_id from product_attributes where product_id in (SELECT  product_id from products where category_id in (#{finalArray.join(",")}) ))")
      logger.debug{"[Api::ProductServices.getAllSubCategoriesForCategory Execution Time = [#{Time.now - currentTime}seconds]"}
    return res
    end
    logger.debug{"[Api::ProductServices.getAllSubCategoriesForCategory Execution Time = [#{Time.now - currentTime}seconds]"}
  end
  
#############################################################################################################################
 def self.getCategoryNameStringFromCategoryTrail(product_trail)
    catNameArray = Api::ProductServices.getCategoryNameArrayFromCategoryTrail(product_trail)
    logger.info"catNameArray #{catNameArray}"
    catName = ""
      i = 0
      catNameArray.each do |r|
        if i==0 
          catName += r
          i +=1
        else 
          catName += " => " + r
        end
      end
      logger.info {catName}
    return catName  
  end
#############################################################################################################################  
  
  def self.getCategoryNameArrayFromCategoryTrail(product_trail)
    conn = ActiveRecord::Base.connection
    catIdArray = Api::ProductServices.getCategoryIdsFromCategoryTrail(product_trail)
      res = conn.execute("select description from categories_hier where id in (#{catIdArray.join(",")})")
      catNameArray = []
      res.each do |r|
        catNameArray << r["description"]
      end
      logger.info {catNameArray}
    return catNameArray  
  end
  
################################################################################################################################################################

  def self.getCategoryIdsFromCategoryTrail(product_trail)
    conn = ActiveRecord::Base.connection
    # if id!=nil  
    # end    
      @catArray = []
       if !product_trail.blank?
        def self.ihash(h)   
        h.each{|k,v|
          @catArray << k.to_i
          if v.is_a? Hash
            ihash(v)        
          end
        }
         end 
        ihash(product_trail)
      end
     
      logger.info {@catArray}
    return @catArray  
  end
  
################################################################################################################################################################

  def self.getProductInfoByContract_id(contract_id)
     conn = ActiveRecord::Base.connection
     product_array = Array.new()
     resultset = conn.execute("select distinct product_id from tier_products where contract_id = '#{contract_id}'")
     resultset.each do |result|
        product = Product.find_by_product_id(result["product_id"])
        product_info=Hash.new()
        product_info.store('product_id',product.product_id)
        product_info.store('product_name',product.upc_desc)
        product_info.store('product_desc',product.sku_desc)
        product_array.push(product_info)
        logger.info { "1product_data =#{product_info}" }
       end
     return product_array
  end

##############################################################################################################

end

