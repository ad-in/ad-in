class Api::PriceRulesServices < ActiveRecord::Base
  

  def self.create(price_rule)
      if price_rule.save
        return "Price Rule created successfully."
      else
       return false
    end
  end
#######################################################  
  def self.update(price_rule)
      if price_rule.update(price_rule_params)
        return "Price Rule updated successfully."
      else
       return false
    end
  end
#######################################################  
  def self.getAllPriceRulesForAllCategory
    conn = ActiveRecord::Base.connection
    res = conn.execute("select count(*),category_id from price_rules where category_id IN (select distinct id from  categories_hier) GROUP BY category_id")
    logger.debug {"Price Rule for category =#{res}"}
    return res
  end

#######################################################  
  def self.getAllPriceRulesForAllLocation
    conn = ActiveRecord::Base.connection
    res = conn.execute("select count(*),location_id from price_rules where location_id IN (select distinct id from  location_hier) GROUP BY location_id")
    logger.debug {"Price Rule for category =#{res}"}
    return res
  end
####################################################  
  

  def self.removePriceRule(price_rule)
    price_rule.destroy
    respond_to do |format|
      format.html { redirect_to price_rules_url, notice: 'Price rule was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  ####################################################  
  def self.getAllPriceVehicles
    conn = ActiveRecord::Base.connection
    res = conn.execute("select * from price_vehicle ")
    logger.debug {"All price vehicles"}
    if res.num_tuples.zero?
    return false
    else
    return res
  end
  end
  ####################################################  
  def self.getAllPriceRulesForCategory(category)
    conn = ActiveRecord::Base.connection
    res = conn.execute("select * from price_rules where category_id = #{category}")
    logger.debug {"Price Rule for category =#{res}"}
    if res.num_tuples.zero?
    return false
    else
    return res
    end
  end
  ####################################################  
  def self.getAllPriceRulesForLocation(location)
    conn = ActiveRecord::Base.connection
    res = conn.execute("select * from price_Rules where banner_id = #{location}")
    
    if res.num_tuples.zero?
    return false
    else
    return res
    end
  end
  ####################################################  
   def self.getPriceRuleForProductIdAndLocationId(product_id,location_id)
    conn = ActiveRecord::Base.connection
    res = conn.execute("select cat_id from products where upc = #{product_id}")
    productCategory = res
    res = conn.execute("select banner_id from location_hier where store_id = #{location_id}")
    location = res
    
    price_rule = getAllPriceRulesForSelectedCategoryAndLocation(productCategory,location)
     
    return price_rule
    end
  ####################################################  
  def self.getAllPriceRulesForSelectedCategoryAndLocation(category,location)
    conn = ActiveRecord::Base.connection
    array = conn.execute("select category_trail from categories_hier where id= #{category}")
    if array.empty?
      allPriceRulesForCat = getAllPriceRulesForCategory(category)
      allPriceRulesForCat.each{|price_rule|
        if price_rule["banner_id"] == location
        return price_rule
        else
          allLocationHierarchy = conn.execute("select location_trail from location_hier where id= #{location}")
          if allLocationHierarchy.empty?
          return price_rule
          else
            _found = false
            allLocationHierarchy.each{|hier_location|
              if price_rule["banner_id"] == hier_location
              _found = true
              return price_rule
              end
            }
            if _found.value? == false
              return "No rule found"
            end
          end
        end
      }
    else
      array.each{|hier_category|
        allPriceRulesForCat = getAllPriceRulesForCategory(hier_category)
        allPriceRulesForCat.each{|price_rule|
          if price_rule["banner_id"] == location
          return price_rule
          else
            allLocationHierarchy = conn.execute("select location_trail from location_hier where id= #{location}")
            if allLocationHierarchy.empty?
            return price_rule
            else
              _found = false
              allLocationHierarchy.each{|hier_location|
                if price_rule["banner_id"] == hier_location
                _found = true
                return price_rule
                end
              }
              if _found.value? == false
                return "No rule found"
              end
            end
          end
        }
      }
    end
  end
####################################################  
  def self.getAllPriceRulesForCategories(categories)
    allKeys  = Array.new
    logger.debug {"categories === #{categories}"}
    categories.each { |category|
      allSubCategories = Api::ProductServices.getAllSubCategoriesForCategory(category)
      allKeys.push(Api::ProductServices.iterateHASH(allSubCategories))
    }
    conn = ActiveRecord::Base.connection
    res = conn.execute("select * from price_rules where category_id in (#{allKeys.join(",")}) ")
    return res
  end
####################################################  
  def self.getAllSubCategoriesForCategory(cat)
    conn = ActiveRecord::Base.connection
    res = conn.execute("select id from categories_hier where parent_id = #{cat}")
    h = Hash.new()
    if res.num_tuples.zero?
      h.store(cat, "leaf")
    else
      arr = Array.new
      res.each {|r|
        hashList = Api::ProductServices.getAllSubCategoriesForCategory(r['id'])
        arr.push(hashList)
      }
      h.store(cat, (arr.reduce Hash.new, :merge))
    end
    return h
  end
####################################################  
  def self.iterateHASH(h)
    arr = Array.new
    h.each do |k,v|
      arr.push(k)
      value = v || k
      if value.is_a?(Hash)
        puts "evaluating: #{value} recursively..."
        arr.push(Api::ProductServices.iterateHASH(value))
      else
        puts v ? "key: #{k} value: #{v}" : "array value #{k}"
      end
    end
    return arr
  end
####################################################  

  def self.getAllPriceRulesForSelectedCategoriesAndAttributes(categories,attributes)
    priceRulesForAllCat = getAllPriceRulesForCategories(categories)
    priceRuleIds = Array.new
    priceRulesForAllCat.each { |priceRule|
      priceRuleIds.push(priceRule.id)
    }
    conn = ActiveRecord::Base.connection
    res = conn.execute("select * from price_rules where id in (#{attributes}) and banner_id in (#{priceRuleIds.join(",")})")
    return res
  end

  def self.getParentCategory(category)
    conn = ActiveRecord::Base.connection
    res = conn.execute("select parent_id from categories_hier where id = #{category}")
    if res.num_tuples.zero?
    return false
    else
    return res
    end
  end
####################################################  
  def self.getParentLocation(location)
    conn = ActiveRecord::Base.connection
    res = conn.execute("select parent_id from location_hier where id = #{location}")
    if res.num_tuples.zero?
    return false
    else
    return res
    end
  end
####################################################  
  def self.getPriceRuleForCategory(category)
    conn = ActiveRecord::Base.connection
    res = conn.execute("select * from price_rules where category_id = #{category}")
    if res.num_tuples.zero?
    return false
    else
    return res
    end
  end

  private
####################################################  
  def set_price_rule()
    @price_rule = PriceRule.find(params[:id])
  end
end