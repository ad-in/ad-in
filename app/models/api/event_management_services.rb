class Api::EventManagementServices < ActiveRecord::Base
  $conn = ActiveRecord::Base.connection
  ################################################################################################################################################################
  #It adds event
  def self.addEvent(event_management)

    if event_management.save
    return true
    else
    return false
    end

  end

  ################################################################################################################################################################
  # It removes event
  def self.removeEvent(event_management)
     if event_management.destroy
      return true
      else
      return false
    end
    
  end
  ################################################################################################################################################################

  # It updates event
  def self.updateEvent(event_management, event_management_params)
      if event_management.update(event_management_params)
        return true
      else
      return false
      end
  end

  ####################################################################################################################################################################################################################################################################################################################

  # It retrives events
  def self.getEvents(start_date ,end_date)
    if (start_date !=nil and end_date!=nil)
      query="select * from event_managements where (start_date between '#{start_date}' and '#{end_date}') and (end_date between '#{start_date}' and '#{end_date}') "

      res =  $conn.execute(query)
      puts(res)
      puts "--------------------------"
      puts "|Event|"
      puts "--------------------------"
      res.each { |row|

        puts "|  #{row['name']}"
      }
      puts "-----------end---------------"
    end
    return res
  end

  ####################################################################################################################################################################################################################################################################################################################

  # It retrives all events
  def self.getAllEvents()

    query="select * from event_managements"

    res =  $conn.execute(query)
    puts(res)
    puts "--------------------------"
    puts "|Event|"
    puts "--------------------------"
    res.each { |row|

      puts "|  #{row['name']}"
    }
    puts "-----------end---------------"
    return res
  end

####################################################################################################################################################################################################################################################################################################################

 #Get all event with selected trail name
 def self.getAllEventsWithTrailName()
    event_managements = EventManagement.all
    event_mngt = []
    event_managements.each { |event_management|
      event_hash = Hash.new()
      event_hash.store("id",event_management["id"])
      event_hash.store("name",event_management["name"])
      event_hash.store("description",event_management["description"])
      event_hash.store("start_date",event_management["start_date"])
      event_hash.store("end_date",event_management["end_date"])
      if !event_management["product_scope_trail"].blank?
        event_hash.store("product_scope",Api::ProductServices.getCategoryNameStringFromCategoryTrail(event_management["product_scope_trail"]))
      else
        event_hash.store("product_scope","")
      end
      if !event_management["location_scope_trail"].blank?
        event_hash.store("location_scope",Api::LocationServices.getLocationNameStringFromLocationTrail(event_management["location_scope_trail"]))
      else
        event_hash.store("location_scope","")
      end
      
      event_mngt << event_hash   
    }
  return event_mngt
 end

####################################################################################################################################################################################################################################################################################################################

end

