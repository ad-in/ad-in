class Api::GeographyServices < ActiveRecord::Base
  $conn = ActiveRecord::Base.connection
  
  def self.test
    puts "hi"
  end

  #############################################################################################################################
  def self.testActiveRecord
    res = $conn.execute("select distinct store_id from geo_hier")
    puts "--------------------------"
    puts "|'Store'"
    puts "--------------------------"
    res.each { |row|

      puts "|  #{row['store_id']}"
    }
    puts "-----------end---------------"
    return res
  end

  #############################################################################################################################

  def self.getAllSuperBanner
    res = $conn.execute("select distinct super_banner_id from geo_hier")
    puts "--------------------------"
    puts "|'Super Banner'"
    puts "--------------------------"
    res.each { |row|

      puts "|  #{row['super_banner_id']}"
    }
    puts "-----------end---------------"
    return res
  end

  #############################################################################################################################

  def self.getAllBanner
    res = $conn.execute("select distinct banner_id from geo_hier")
    puts "--------------------------"
    puts "|'Banner'"
    puts "--------------------------"
    res.each { |row|

      puts "|  #{row['banner_id']}"
    }
    puts "-----------end---------------"
    return res
  end

  #############################################################################################################################
  def self.getAllStore

    res = $conn.execute("select distinct store_id from geo_hier")
    puts "--------------------------"
    puts "|'Store'"
    puts "--------------------------"
    res.each { |row|

      puts "|  #{row['store_id']}"
    }
    puts "-----------end---------------"
    return res
  end
  #############################################################################################################################

  def self.getStore(store_id)

    res= $conn.execute [
      "select gh.store_id, hd.description from geo_hier gh inner join hierarchy_descr hd ",
      "on hd.value like '%#{store_id}%' ",
      "where gh.store_id = #{store_id} and hd.level like '%Store_id%'",
    ].join(' ')

    puts "--------------------------"
    puts "|Store |Description "
    puts "--------------------------"
    res.each { |row|

      puts "|  #{row['store_id']} | #{row['description']}"
    }
    puts "-----------end---------------"
    return res
  end

  #############################################################################################################################
  def self.getBanner(banner_id)

    res= $conn.execute [
      "select store_id from geo_hier gh",
      "where gh.banner_id ='#{banner_id}'",
    ].join(' ')

    puts "--------------------------"
    puts "|'Banner'"
    puts "--------------------------"
    res.each { |row|

      puts "|  #{row['banner_id']}"
    }
    puts "-----------end---------------"
    return res
  end

  #############################################################################################################################
  def self.getSuperBanner(super_banner_id)

    res= $conn.execute [
      "select super_banner_id from geo_hier gh",
      "where gh.super_banner_id ='#{super_banner_id}'",
    ].join(' ')

    puts "--------------------------"
    puts "|'Super Banner'"
    puts "--------------------------"
    res.each { |row|

      puts "|  #{row['super_banner_id']}"
    }
    puts "-----------end---------------"
    return res
  end

  #############################################################################################################################
  def self.getAllStoresForBanner(banner_id)

    res= $conn.execute [
      "select banner_id,store_id from geo_hier gh",
      "where gh.banner_id ='#{banner_id}'",
    ].join(' ')

    puts "--------------------------"
    puts "|'Store'"
    puts "--------------------------"
    res.each { |row|

      puts "|  #{row['store_id']}"
    }
    puts "-----------end---------------"
    return res
  end

  #############################################################################################################################
  def self.getAllBannersForSuperBanner(super_banner_id)

    res= $conn.execute [
      "select super_banner_id,banner_id from geo_hier gh",
      "where gh.super_banner_id ='#{super_banner_id}'",
    ].join(' ')

    puts "--------------------------"
    puts "|'Store'"
    puts "--------------------------"
    res.each { |row|

      puts "|  #{row['store_id']}"
    }
    puts "-----------end---------------"
    return res
  end

  ############################################################################################################################
  def self.getGeoAttributeByStore(store_id)

    res= $conn.execute [
      "select gh.store_id, hd.description,ga.value from geo_hier gh inner join hierarchy_descr hd ",
      "on hd.value like '%#{store_id}%' ",
      "Inner join geography_attributes ga  on gh.store_id =ga.geography_id",
      "where gh.store_id = #{store_id} and hd.level like '%Store_id%'",
    ].join(' ')

    puts "-----------------------------------------------------"
    puts "|Store |Description |AttributeValue"
    puts "-----------------------------------------------------"
    res.each { |row|

      puts "|  #{row['store_id']} | #{row['description']} | #{row['value']}"
    }
    puts "-----------end----------------------------------------"
    return res
  end

  #############################################################################################################################
  def self.getStoresByGeoAttribute(custom_attr_id)
    res= $conn.execute [
      "select gh.store_id, ga.geography_id,ga.value ,hd.description from geo_hier gh ",
      "Inner join geography_attributes ga  on gh.store_id =ga.geography_id",
      "inner join hierarchy_descr hd on gh.store_id = CAST(hd.value AS Int)",
      "where ga.custom_attr_id = #{custom_attr_id} and hd.level like '%Store_id%'",
    ].join(' ')

    puts "-----------------------------------------------------"
    puts "|Store |Geography_id |AttributeValue|Description"
    puts "-----------------------------------------------------"
    res.each { |row|

      puts "|  #{row['store_id']} | #{row['geography_id']} | #{row['value']} | #{row['description']}"
    }
    puts "-----------end----------------------------------------"
    return res
  end
#############################################################################################################################

end

GeographyServices.getStoresByGeoAttribute(3)
GeographyServices.getGeoAttributeByStore(3651)
