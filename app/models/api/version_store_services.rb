class Api::VersionStoreServices < ActiveRecord::Base
  # service to add new version store entries
  class << self
  def storeLocationAndGroup(contract, version_id, params)
  	 # localversion = Api::VersionServices.getVersionByVersionid(version_id)
     locationIds = params[:locationIds] unless params[:locationIds].blank?
     groupIds = params[:groupIds] unless params[:groupIds].blank?
    if locationIds
    #this part is for if condition in location group
     version_stores = Api::VersionStoreServices.addVersionStore(contract, locationIds, version_id)
	 # storeArr = Api::VersionStoreServices.updateContractEntry(contract, version_id)
  #    locationObjects = Api::LocationServices.getStoresByIds(storeArr)
    elsif groupIds 
    # this else part  if for using from location group
      location_groups = Api::LocationGroupServices.getLocationGroupByIds(groupIds)
      location_groups.each do |lg|
      	if !lg.lids.blank?
       	 Api::VersionStoreServices.addVersionStore(contract, lg.lids, localversion.version_id)		
      	end
      end  
	  storeArr = Api::VersionStoreServices.updateContractEntry(contract, localversion.version_id)
      locationObjects = location_groups
    end 	
    return version_stores
  end	

  def getVersionStoreByVersionIdandStoreId(versionId, storeId)
  	res = VersionStore.find_by_version_id_and_store_id("#{versionId}", "#{storeId}")
  	return res
  end
  def getVersionStoreByVersionId(versionId)
  	res = VersionStore.where(:version_id => "#{versionId}")
  	return res
  end
 
  def addVersionStore(contract, locationIds, versionId)
  	vesrion_stores_array = []
  	locationIds.each do |location|
      version_store = Api::VersionStoreServices.getVersionStoreByVersionIdandStoreId(versionId, location)
       if version_store.blank?
        @version_store = VersionStore.new
        @version_store.trial_id = contract.trial_id
        @version_store.version_id = versionId
        @version_store.store_id = location
        @version_store.save!
        vesrion_stores_array << @version_store
       end
    end
    return vesrion_stores_array
  end
  def updateContractEntry(contract, versionId)
  	storeArr = []
     verStrList = Api::VersionStoreServices.getVersionStoreByVersionId("#{versionId}")
     verStrList.each do |row|
       storeArr << row['store_id']
     end
     logger.info"+++ ids in storeArr is #{storeArr}"
     contract.update_attributes(:store_ids => storeArr)
     return storeArr
  end

  def removeVersionStore(contract, params)
  	storeIds = params[:storeIds] unless params[:storeIds].blank?
    localversion = Api::VersionServices.getVersionByTrialid(contract.trial_id)
    if VersionStore.where("version_id =#{localversion.version_id} and store_id in (#{storeIds.join(",")})").destroy_all
     storeArr = Api::VersionStoreServices.updateContractEntry(contract, localversion.version_id)
     locationObjects = Api::LocationServices.getStoresByIds(storeArr)
    end
    return locationObjects
  end
  
 end 
end