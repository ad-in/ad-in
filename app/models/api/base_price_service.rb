class Api::BasePriceService < ActiveRecord::Base
  def self.findProdutsBasePriceByProductId(productId)

    conn = ActiveRecord::Base.connection
    res = conn.execute("select bc.base_price from base_price_product_store bc inner join products ph on bc.sku = ph.sku where ph.product_id = #{productId}  group by  bc.base_price limit 1")
    res.each do |row|
      base_price=row['base_price']
      logger.info { "base_price =#{base_price}" }
      if(base_price!=nil)
      return base_price
      else
        return nil
      end
    end

  end
  
   def self.findProdutsBasePrice(sku,storeList)
   conn = ActiveRecord::Base.connection
    res = conn.execute("select base_price,count(*)from base_price_product_store where sku = #{sku} and effective_from_dt_sas <= current_date and effective_to_dt_sas > current_date and store_id in (#{storeList.join(",")}) group by 1 order by 2 desc limit 1")
    res.each do |row|
      base_price=row['base_price']
      logger.info { "base_price =#{base_price}" }
      if(base_price!=nil)
      return base_price
      else
        return nil
      end
    end

  end


end