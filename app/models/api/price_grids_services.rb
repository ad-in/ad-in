class Api::PriceGridsServices < ActiveRecord::Base
  
  def self.create(price_grid)
    @price_grid = price_grid
      if @price_grid.save
        return "Price grid was successfully created"
      else
        return false
    end
  end
####################################################  
  def self.update(price_grid)
     if price_grid.update
        return "Price grid was successfully updated"
      else
        return false
    end
  end
####################################################  
  def self.removePriceGrid()
    @price_grid.destroy
    respond_to do |format|
      format.html { redirect_to price_grids_url, notice: 'Price grid was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
####################################################  
  def findPriceGuardRail(price_rule,base_cost,base_price,vendor_funding)
    minPriceGuardRail = findMinPriceGuardrail(price_rule,base_cost,base_price,vendor_funding)
    maxPriceGuardRail = findMaxPriceGuradrail(price_rule,base_cost,base_price,vendor_funding)
  end
####################################################  
  def findMinPriceGuardrail(price_rule,base_cost,base_price,vendor_funding)
    maxPriceChange = ((1-(price_rule["max_price_change"]/100)) * base_price)
    minMargin = (base_cost - vendor_funding)/(1 - price_rule["min_margin"])
    maxPassthrough = (base_price -(((price_rule["max_passthrough"])/100) * vendor_funding))
    highest = 0;
    if(maxPriceChange>minMargin)
      if(maxPriceChange>maxPassthrough)
      highest = maxPriceChange
      else
      highest = maxPassthrough
      end
    else
      if(minMargin>maxPassthrough)
      highest = minMargin
      else
      highest = maxPassthrough
      end
    end
    return highest
  end
####################################################  
  def findMaxPriceGuradrail(price_rule,base_cost,base_price,vendor_funding)
    minPriceChange = ((1-(price_rule["min_price_change"]/100)) * base_price)
    maxMargin = (base_cost - vendor_funding)/(1 - price_rule["max_margin"])
    minPassthrough = (base_price -(((price_rule["min_passthrough"])/100) * vendor_funding))
    smallest = 0;
    if (minPriceChange < maxMargin)
      if (minPriceChange > minPassthrough)
      smallest = minPassthrough;
      else
      smallest = minPriceChange;
      end
    else
      if (maxMargin > minPassthrough)
      smallest = minPassthrough;
      else
      smallest = maxMargin;
      end
    end
    return smallest
  end
####################################################  
  def self.findFromAndToForPriceRule(price_rule)

  end
  ####################################################  
  def self.getAllPriceGridForCategory(category)
    logger.debug {"In the price grid method"}
    priceRules = Api::PriceRulesServices.getAllPriceRulesForCategory(category)
    priceRulesIds = Array.new
    priceRules.each { |row|  
    priceRulesIds.push(row["id"])
    }
    conn = ActiveRecord::Base.connection
    res = conn.execute("select * from price_grids where price_rule_id in ( #{priceRulesIds.join(",")}) ")
    return res
  end
  ####################################################    
   def self.getAllPriceGridsForLocation(location)
    priceRules = Api::PriceRulesServices.getAllPriceRulesForLocation(location)
    priceRulesIds = Array.new
    priceRules.each { |row|  
    priceRulesIds.push(row["id"])
    }
    conn = ActiveRecord::Base.connection
    res = conn.execute("select * from price_grids where price_rule_id in ( #{priceRulesIds.join(",")}) ")
    return res
  end
  ####################################################  
  def self.getPriceGridByProductIdAndLocationId()
    conn = ActiveRecord::Base.connection
    res = conn.execute("select cat_id from products where product_id = #{product_id}")
    productCategory = res
    res = conn.execute("select banner_id from location_hier where store_id = #{location_id}")
    location = res
    price_rule = getAllPriceRulesForSelectedCategoryAndLocation(productCategory,location)
    priceRulesIds = Array.new
    priceRules.each { |row|  
    priceRulesIds.push(row["id"])
    }
    conn = ActiveRecord::Base.connection
    res = conn.execute("select * from price_grids where price_rule_id=#{ price_rule["id"]} ")
    return res
  end
  
####################################################### 
  def self.getAllPriceGridsForAllCategory
    conn = ActiveRecord::Base.connection
    res = conn.execute("select count(*),category_id from price_grids where category_id IN (select distinct id from  categories_hier) GROUP BY category_id")
    logger.debug {"Price Grids for category =#{res}"}
    return res
  end
#######################################################  
  def self.getAllPriceGridsForAllLocation
    conn = ActiveRecord::Base.connection
    res = conn.execute("select count(*),location_id from price_grids where location_id IN (select distinct id from  categories_hier) GROUP BY location_id")
    logger.debug {"Price Grids for category =#{res}"}
    return res
  end
####################################################  
end
