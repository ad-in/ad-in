#services for location_hier
class Api::LocationServices < ActiveRecord::Base
 
  ############################################################################################################################
 class <<  self
  #It retrives all sub banners in hier
  def getAllSubBanners(banner)
    conn = ActiveRecord::Base.connection
    res = conn.execute("select * from location_hier where parent_id = #{baneer['id']}")
    h = Hash.new()
    if res.num_tuples.zero?
      h.store(baneer['description'], "leaf")
    else
      arr = Array.new
      res.each {|r|
        hashList =Api::LocationServices.getAllSubBanners(r)
        arr.push(hashList)
      }
      h.store(baneer['description'], (arr.reduce Hash.new, :merge))
    end

    return h
  end
 #############################################################################################################################
 def getStoresByBanner(banner)
    conn = ActiveRecord::Base.connection
    finalArray = Array.new
    @banners = getAllBanner
    rowsArrayOfHashes = Array.new
    @banners.each { |e|
      rowsArrayOfHashes.push(e)
    }
    if checkIfBannerIsLeaf(banner ,rowsArrayOfHashes)
      # res = conn.execute("SELECT * from stores where location_id='#{banner}' ")
      res = Store.where(:location_id=> "#{banner}")
    return res
    else
      finalArray = getAllSubBannersIds(banner , rowsArrayOfHashes)
      p finalArray
      # res = conn.execute("SELECT * from stores where location_id in (#{finalArray.join(",")}) ")
      res = Store.where("location_id in (#{finalArray.join(",")})")
    return res
    end
end

def getAllSubBannersIds(banner , rowsArrayOfHashes)
    finalArray = Array.new
    childrensHash =  rowsArrayOfHashes.select { |hash|
      trail = hash['location_trail']
      if trail != nil
        trail = trail.gsub("{","")
        trail = trail.gsub("}","")
        trail = trail.split(",").map {|s| s.to_i}
      trail.include?(banner.to_i)
      end
    }
    childrensHash.each{|childs|
      trail1 = childs['location_trail']
      if trail1 != nil
        trail1 = trail1.gsub("{","")
        trail1 = trail1.gsub("}","")
        trail1 = trail1.split(",").map {|s| s.to_i}
        finalArray = finalArray.concat(trail1)
        finalArray = finalArray.uniq
        if childs['depth'] == '3'
          finalArray = finalArray.push(childs['id'].to_i)
        finalArray = finalArray.uniq
        end
      end
    }
    if finalArray.include? banner
      finalArray = finalArray.drop_while{|i| i < banner }
    end
    return finalArray
  end
  
  def checkIfBannerIsLeaf(banner , rowsArrayOfHashes)
    childshash = rowsArrayOfHashes.select {|hash|
      hash['parent_id'].to_i == banner.to_i
    }
    if childshash.length.zero?
    return true
    else
    return false
    end
  end



#############################################################################################################
# This will fetsh stores by Ids
def getStoresByIds(locationIds)
  conn = ActiveRecord::Base.connection
  # res = conn.execute("SELECT * from stores where store_id in (#{locationIds.join(",")}) ")
  res = Store.where("store_id in (#{locationIds.join(",")})")
  return res
end

#############################################################################################################
####This will fetch all custom attr of listed stores
 def getAllCustomAttr
    # conn = ActiveRecord::Base.connection
    # res = conn.execute("SELECT * from custom_attributes")
     res = CustomAttribute.all
    return res
 end

def getCustAttrValues(custom_attr_id)
   conn = ActiveRecord::Base.connection
   location_attributes = conn.execute("select * from location_attributes where custom_attr_id = #{custom_attr_id}")
   finalArray = []
   location_attributes.each do |locAttr|
     res =  locAttr
     finalArray << res
   end
   return finalArray
end

def getStoresByCustomAttr(id, value)
  conn = ActiveRecord::Base.connection
    finalArray = Array.new
    @custom_attributes = getAllCustomAttr
    rowsArrayOfHashes = Array.new
    @custom_attributes.each { |e|
      rowsArrayOfHashes.push(e)
    }
    if hashes = rowsArrayOfHashes.select {|hash| hash['id_'] == '0'}
      
      
    end
    res = conn.execute("SELECT * from stores where store_id =(select location_id from location_attributes where custom_attr_id = '#{id}' and value= '#{value}') order by store_id desc")
    return res
end
#############################################################################################################
#############################################################################################################
####This will fetch all custom attr of listed stores
def getCustomAttrByStore(id)
  conn = ActiveRecord::Base.connection
  @stores = getStoresByBanner(id)
   @stores.each do 
    res = conn.execute("SELECT * from custom_attributes where id in(select custom_attr_id from location_attribut)")
   end
    return res
end
#############################################################################################################

  ############################################################################################################################
  #It retrives all leaf banners for particuler banner from hier

  def getLeafBanners(parent_id)
    conn = ActiveRecord::Base.connection
    if parent_id !=nil
      Api::LocationServices.getAllSubBanners(parent_id)
      res = conn.execute("select id from location_hier where parent_id = #{parent_id}")
      if res.num_tuples.zero?
      $stores.push(parent_id)
      else
        res.each {|r|
          Api::LocationServices.getLeafBanners(r['id'])

        }
      end
    end
    return $stores

  end

  ############################################################################################################################
  #It retrives all stores related to particuler banner

  def getAllStores(parent_id)
     conn = ActiveRecord::Base.connection
    if parent_id!=nil
      stores= Array.new
      Array banners=Api::LocationServices.getLeafBanners(parent_id)
      res=nil
      banners.each { |banner_id|
        res = conn.execute("select store_id from geo_hier where location_id IN(#{banner_id}) ")
        res.each { |row|

          stores.push(row['store_id'])
        }
      }
    end

    return stores
  end
  ############################################################################################################################

  def getAllBanner
     conn = ActiveRecord::Base.connection
     res = conn.execute("select * from  location_hier")
    return res
  end
  
   def getAllSubBannersForBanner(ban ,rowsArrayOfHashes)
    #res = $conn.execute("select * from location_hier where parent_id = #{ban['id']}")
    currentTime = Time.now
    res = rowsArrayOfHashes.select {|localHash| localHash['parent_id'] == ban['id']}
    h = Hash.new()
    if res.length.zero?
      h.store([ban['id'],ban['name']], "leaf")
    else
      arr = []
      res.each do |r|
        hashList = Api::LocationServices.getAllSubBannersForBanner(r,rowsArrayOfHashes)
        arr << hashList
      end
      h.store([ban['id'],ban['name']], (arr.reduce Hash.new, :merge!))
    end
    
    logger.debug{"[Api::LocationServices.getAllSubBannersForBanner Execution Time = [#{Time.now - currentTime}seconds]"}
    return h
  end
  

  def getAllBannersInHash
    currentTime = Time.now
    finalArray = []
    @banners = getAllBanner
    rowsArrayOfHashes = []
    @banners.each do |e|
      rowsArrayOfHashes << e
    end
    if hashes = rowsArrayOfHashes.select {|hash| hash['depth'] == '1'}
      hashes = hashes.sort {|a,b| a['name'] <=> b['name']}
      if hashes.is_a?(Array)
        hashes.each { |h|
          childrensHash =  rowsArrayOfHashes.select { |hash|
            trail = hash['location_trail']
            if trail != nil
              trail = trail.gsub("{","")
              trail = trail.gsub("}","")
              trail = trail.split(",").map {|s| s.to_i}
              trail[0] == h['id'].to_i
            end
          }
          result = getAllSubBannersForBanner(h, childrensHash)
          finalArray << result
        }
      else
        finalArray << getAllSubBannersForBanner(hashes, rowsArrayOfHashes)
      end
    else
      p 'not found'
    end
    logger.debug{"[Api::LocationServices.getAllBannersInHash Execution Time = [#{Time.now - currentTime}seconds]"}
    return finalArray
  end
# remove a store from contract
  # def removeStorefromContract(storeIds, versionId)
  #   if VersionStore.where("version_id =#{versionId} and store_id in (#{storeIds.join(",")})").destroy_all
  #   return true
  #   else
  #   return false
  #   end
  #   # conn = ActiveRecord::Base.connection
  #   # storeIds.each{|store|
  #   #  if conn.execute("DELETE from version_stores where version_id =#{versionId} and store_id in (#{storeIds.join(",")})")
  #   #   return true
  #   #  else
  #   #   return false
  #   #  end
  #   # }
  # end
 end

#############################################################################################################################
 def self.getLocationNameStringFromLocationTrail(location_trail)
      bannersNameArray = Api::LocationServices.getLocationNameArrayFromLocationTrail(location_trail)
      bannerName = ""
      i = 0
      bannersNameArray.each do |r|
        if i==0 
          bannerName += r
          i +=1
        else 
          bannerName += " => " + r
        end
      end
      logger.info {bannerName}
    return bannerName  
  end


#############################################################################################################################
 def self.getLocationNameArrayFromLocationTrail(location_trail)
    conn = ActiveRecord::Base.connection
    bannersIdArray = Api::LocationServices.getLocationIdsFromLocationTrail(location_trail)
      res = conn.execute("select name from location_hier where id in (#{bannersIdArray.join(",")})")
      bannersNameArray = []
      res.each do |r|
        bannersNameArray << r["name"]
      end
      logger.info {bannersNameArray}
    return bannersNameArray  
  end
#############################################################################################################################

def self.getLocationIdsFromLocationTrail(location_trail)
    conn = ActiveRecord::Base.connection
    # if id!=nil  
    # end    
      @LocationArray = []
       if !location_trail.blank?
        def self.ihash(h)   
        h.each{|k,v|
          @LocationArray << k.to_i
          if v.is_a? Hash
            ihash(v)        
          end
        }
         end 
        ihash(location_trail)
      end
      
      logger.info {@LocationArray}
    return @LocationArray  
  end
#############################################################################################################################
  
 def self.getAllLocationAttributes
    conn = ActiveRecord::Base.connection
    res = conn.execute("select id ,name ,value ,description from custom_attributes where id in(select distinct custom_attr_id from  custom_attributes c ,location_attributes l  where l.custom_attr_id=c.id)")
    return res
  end

end
