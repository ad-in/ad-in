class Api:: GeoGroupServices < ActiveRecord::Base
################################################################################################################################################################
  #It adds a new GeoGroup
  def self.addGeoGroup(geo_group)
    respond_to do |format|
      if geo_group.save
        format.html { redirect_to geo_group, notice: 'Geo group was successfully created.' }
        format.json { render :show, status: :created, location: geo_group }
      else
        format.html { render :new }
        format.json { render json: geo_group.errors, status: :unprocessable_entity }
      end
    end
  end
################################################################################################################################################################
  #It Updates GeoGroup
  def self.updateGeoGroup(geo_group)
    respond_to do |format|
      if geo_group.update(geo_group_params)
        format.html { redirect_to geo_group, notice: 'Geo group was successfully updated.' }
        format.json { render :show, status: :ok, location: geo_group }
      else
        format.html { render :edit }
        format.json { render json: geo_group.errors, status: :unprocessable_entity }
      end
    end
  end
################################################################################################################################################################
  # It Removes a GeoGroup
  def self.removeGeoGroup(geo_group)
    geo_group.destroy
    respond_to do |format|
      format.html { redirect_to geo_groups_url, notice: 'Geo group was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

################################################################################################################################################################
  # get all GeoGroups
  def self.getAllGeoGroups
    res =  $conn.connection.execute("select * from geo_groups")
    puts res
    puts "--------------------------"
    puts "|'Group'"
    puts "--------------------------"
    res.each { |row|

      puts "|  #{row}"
    }
    puts "-----------end---------------"
    return res
  end
 ################################################################################################################################################################
  # get a GeoGroup
  def self.getGeoGroupByname(name)
    res =  $conn.connection.execute [
      "select gg.name, gg.description,gg.geo_trail, gg.custom_attribute ,hd.description from geo_groups gg ",
      "inner join geo_hier gh on true=true ",
      "inner join hierarchy_descr hd on gh.store_id = CAST(hd.value AS Int) ",
      "where gh.super_banner_id IN(CAST(gg.geo_trail#>>'{SuperBanner}' AS Int)) ",
      "and gh.banner_id IN(CAST(gg.geo_trail#>>'{Banner}' AS Int)) ",
      "and hd.level like '%Store_id%' and (gg.name='#{name}' or #{name==nil}) ",
    ].join(' ')
    puts(res)
    puts "--------------------------"
    puts "|Group|"
    puts "--------------------------"
    res.each { |row|

      puts "|  #{row}"
    }
    puts "-----------end---------------"
    return res
  end
 ################################################################################################################################################################
  # get a CreateGeoTrail
  def self.createGeoTrail( id )
    res = $conn.connection.execute(" select parent_id from location_hier where id = #{id}")
    h = Hash.new()
    arr = Array.new
    res.each {|r|
      parent=r['parent_id']
      if parent!=nil
        hashList = GeoGroupServices.createGeoTrail(parent)
      arr.push(hashList)
      else

        h.store(id, "parent")
      end
    }
    h.store(id, (arr.reduce Hash.new, :merge))

    return h
  end

################################################################################################################################################################
end
