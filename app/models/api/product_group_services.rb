class Api::ProductGroupServices < ActiveRecord::Base
 class << self
  #It adds a new GeoGroup
    def addProductGroup(product_group, user, params)
      custom_attr_id = params[:custom_attr_id] unless params[:custom_attr_id].blank?
      custom_attr_val = params[:custom_attr_val] unless params[:custom_attr_val].blank?
      product_attribute = Hash.new
      product_attribute.store(custom_attr_id, custom_attr_val) 
      product_group.product_trail = Api::ProductGroupServices.getProductTrail(params[:product_id]).to_json unless params[:product_id].blank?
      product_group.product_attribute = product_attribute unless product_attribute.blank?
      product_group.created_by = user.id
      product_group.pids = params[:productIdArray] unless params[:productIdArray].blank?
    if product_group.save
     return true
    else
     return false
    end
  end

  ################################################################################################################################################################
  #It Updates GeoGroup
  def updateProductGroup(product_group, user, product_group_params ,params)
    custom_attr_id = params[:custom_attr_id] unless params[:custom_attr_id].blank?
    custom_attr_val = params[:custom_attr_val] unless params[:custom_attr_val].blank?
    product_attribute = Hash.new
    product_attribute.store(custom_attr_id, custom_attr_val) 
    product_group.product_trail = Api::ProductGroupServices.getProductTrail(params[:product_id]).to_json unless params[:product_id].blank?
    product_group.product_attribute = product_attribute unless product_attribute.blank?
    product_group.updated_by = user.id
    product_group.pids = params[:productIdArray] unless params[:productIdArray].blank?
    if product_group.update(product_group_params)
    return true
    else
    return false
    end
  end

  ################################################################################################################################################################
  # It Removes a GeoGroup
  def removeProductGroup(product_group)
    if product_group.destroy
    return true
    else
    return false
    end
    # conn = ActiveRecord::Base.connection
    #  if conn.execute("DELETE from product_groups where id =#{product_group.id})")
    #   return true
    #  else
    #   return false
    #  end
  end
    
  def removeBulkProductGroup(groupIds)  
    if ProductGroup.where("id in (#{groupIds.join(",")})").destroy_all
    return true
    else
    return false
    end
   # conn = ActiveRecord::Base.connection
   # logger.info"+++inside ProductGroupServices and ids = #{groupIds}"
   #   if conn.execute("DELETE from product_groups where id in (#{groupIds.join(",")})")
   #    return true
   #   else
   #    return false
   #   end
  end

  def getAllProductGroups
    # conn = ActiveRecord::Base.connection
    # res = conn.execute("select * from product_groups")
    res = ProductGroup.all
    return res
  end
  
  def getProductGroupByIds(groupIds)
  # conn = ActiveRecord::Base.connection
  # res = conn.execute("SELECT * from product_groups where id in (#{groupIds.join(",")}) ")
  res = ProductGroup.where("id in (#{groupIds.join(",")})")
  return res
end
  
  def getAllProductsFromProductGroup(id)
    conn = ActiveRecord::Base.connection
    res = conn.execute("select pids from product_groups where id = #{id}")
    return res
  end 
  
  def getAllProductsGroupsInHash
   allProductGroupsArray = []
    # allProductGroups = getAllProductGroups
    allProductGroups = ProductGroup.all
    if !allProductGroups.blank?
    allProductGroups.each do |productGroup|
      finalHash = Hash.new
      key = [productGroup['id'],productGroup['name']]
      logger.debug{"--------key-----#{key}"}
      value = []
      productsForGroup = getAllProductsFromProductGroup(productGroup['id'])
      if !productsForGroup.blank?
        productsForGroup.each do |everyList|
          arrayOfPids = everyList['pids']
          arrayOfPids = arrayOfPids.gsub("{","")
          arrayOfPids = arrayOfPids.gsub("}","")
          arrayOfPids = arrayOfPids.split(",").map {|s| s.to_i}
          productsArrayReturned = Api::ProductServices.getProductsByIds(arrayOfPids)
          productsArrayReturned.each do |everyRow|
            value << everyRow
          end
        end
      else
      end
      finalHash.store(key, value);
     allProductGroupsArray << finalHash
    end
    end
    return allProductGroupsArray
  end
    
  
 ################################################################################################################################################################
  # def getAllProductGroupsByname(name)
# 
    # res =$conn.execute("select * from product_groups where name='#{name}'")
    # put(res)
    # puts "--------------------------"
    # puts "|Group|"
    # puts "--------------------------"
    # res.each { |row|
# 
      # puts "|  #{row['name']}"
    # }
    # puts "-----------end---------------"
    # return res
  # end
################################################################################################################################################################

  def getProductTrail(id)
    conn = ActiveRecord::Base.connection
    res = conn.execute(" select parent_id from categories_hier where id = #{id}")
    h = Hash.new()
    arr = []
    res.each {|r|
      parent=r['parent_id']
      if parent!=nil
          hashList = Api::ProductGroupServices.getProductTrail(parent)
        arr<< hashList
      else
          h.store(id, "parent")
      end
    }
    h.store(id, (arr.reduce Hash.new, :merge))
  return h
  end
################################################################################################################################################################
end  

end
