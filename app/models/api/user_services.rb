require 'digest/sha1'
class Api::UserServices < ActiveRecord::Base
$conn = ActiveRecord::Base.connection
 class << self  
  def addUser(user, params)
      user.user_preference = User::GetAllCols
      if user.save
        if !params[:selectedLocHierArray].blank?
          selected_groups =  params[:selectedLocHierArray]
          selected_groups.each do |location|
           Api::UserServices.setUserLocation(user.id, location)
          end
         end
         if !params[:selectedCateHierArray].blank?
          selected_groups =  params[:selectedCateHierArray]
          selected_groups.each do |category|
           Api::UserServices.setUserCategory(user.id, category)
          end
         end
        return "User created successfully."
      else
       return false
    end
  end

  def updateUser(user, user_params, params)
      user.user_preference = user.user_preference.blank? ? User::GetAllCols : user.user_preference
    if user.update(user_params)
      if !params[:selectedLocHierArray].blank?
          selected_groups =  params[:selectedLocHierArray]
          selected_groups.each do |location|
           Api::UserServices.setUserLocation(user.id, location)
          end
         end
         if !params[:selectedCateHierArray].blank?
          selected_groups =  params[:selectedCateHierArray]
          selected_groups.each do |category|
           Api::UserServices.setUserCategory(user.id, category)
          end
         end
        return true
      else
      return false
      end
  end

  def removeUser(user)
    if user.destroy
      UserLocations.where(:user_id => user.id).destroy_all
      UserCategories.where(:user_id => user.id).destroy_all
      return true
    else
      return false
    end
  end
  def getAllUsers
    # res = $conn.execute("select * from users")
    res = User.all
  end
   def getAllUserLocations(id)
     user_locations = UserLocations.where(:user_id => id)
     return user_locations
   end
   def getUserLocationName(location_id)
    location_trail = Api::LocationGroupServices.createLocationTrail(location_id)
    location_name = Api::LocationServices.getLocationNameStringFromLocationTrail(location_trail)
    return location_name
   end
  def setUserLocation(id, location_id)
    user_location = UserLocations.find_by_user_id_and_location_hier_id(id, location_id)
    if user_location.blank?
      UserLocations.create(:user_id => id, :location_hier_id => location_id)
    else
      # user_location.update_attributes()
    end
  end

   def getAllUserCategories(id)
     user_categories = UserCategories.where(:user_id => id)
     return user_categories
   end
   def getUserCategoryName(product_id)
    product_trail = Api::ProductGroupServices.getProductTrail(product_id)
    product_name = Api::ProductServices.getCategoryNameStringFromCategoryTrail(product_trail)
    return product_name
   end
  def setUserCategory(id, product_id)
    user_category = UserCategories.find_by_user_id_and_categories_hier_id(id, product_id)
    if user_category.blank?
      UserCategories.create(:user_id => id, :categories_hier_id => product_id)
    else
      # user_location.update_attributes()
    end
  end

  def authenticate(username, password)
     user = User.find_by_username(username)
   if user
      expected_password = User.encrypt_password(password, user.salt)
      logger.info"+++ uspw= #{user.password_digest} and expected_password = #{expected_password}"
      if user.password_digest != expected_password
        user = nil
      end
    end
    user
  end
  
 end
 
  end