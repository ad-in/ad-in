class Api::BaseCostService < ActiveRecord::Base



  def self.findProdutsBaseCostByProductId(productId)
   conn = ActiveRecord::Base.connection
    res = conn.execute("select bc.base_cost from base_cost_product_store bc inner join products ph on bc.sku = ph.sku where ph.product_id = #{productId}  group by  bc.base_cost limit 1")
    res.each do |row|
      base_cost=row['base_cost']
      logger.info { "base_cost =#{base_cost}" }
      if(base_cost!=nil)
      return base_cost
      else
        return nil
      end
    end

  end
  
 ##################################################################################################################### 
  
  
  def self.findProdutsBaseCost(sku,storeList)
   conn = ActiveRecord::Base.connection
    res = conn.execute("select base_cost,count(*)from base_cost_product_store where sku = #{sku} and effective_from_dt_sas <= current_date and effective_to_dt_sas > current_date and store_id in (#{storeList.join(",")}) group by 1 order by 2 desc limit 1")
    res.each do |row|
      base_cost=row['base_cost']
      logger.info { "base_cost =#{base_cost}" }
      if(base_cost!=nil)
      return base_cost
      else
        return nil
      end
    end

  end


end
