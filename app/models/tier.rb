class Tier < ActiveRecord::Base
  belongs_to :version
  has_many :tier_products
  has_many :products, :through => :tier_products
end
