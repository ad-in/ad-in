require 'digest/sha1'
class User < ActiveRecord::Base
  ##Auditable gem to tract user activity
  #auditable 
  #####________________________
  
  has_many :user_locations
  has_many :location_hier, :through=> :user_locations
  has_many :user_categories
  has_many :categories_hier, :through => :user_categories
  
  EMAIL_REGEX = /\A[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\Z/i

#  validates :first_name, :presence => true
#  validates :last_name, :presence => true
  validates :username, :presence => true, :uniqueness => true, :length => { :in => 6..20 }
  validates :email, :presence => true, :uniqueness => true, :format => EMAIL_REGEX
  validates_presence_of :password, :on => :create
  validates_confirmation_of :password, :on => :create
  
  
  
  
  GetAllCols = {"customFields"=> [ "Item Description", "Category Manager", "Banner", "Link/L2", "# of Weeks", "Page & Ad Size", "Retail", "List Cost", "Unit Funding", 
              "Net Net Cost", "Appr Retail", "Appr Est Sales Units", "Appr Est Sales Dollars", "Appr Est Margin Dollars", "Sugg Retail", 
              "Est Sales Unit", "Est Sales Dollars", "Est Margin Dollars", "CRS Sales Rank", "CRS Household Rank", "CRS Incremental Basket Rank" ]}
  
  
  def generate_token(column)
    begin
      self[column] = SecureRandom.urlsafe_base64
    end while User.exists?(column => self[column])
  end
  
  #'password' is a virtual attribute
  def password
    @password
  end
  
  def password= (pwd)
    @password =pwd
    return if pwd.blank?
    create_new_salt
    self.password_digest = User.encrypt_password( self.password, self.salt)
  end

   
   def get_role_name
     if role == 1
       "Admin"
     elsif role == 2
       "Category Manager"
     end
   end

  private

   def create_new_salt
    self.salt = self.object_id.to_s + rand.to_s
   end
  
  def self.encrypt_password(password, salt)
    string_to_hash = password +"prognos" + salt
    Digest::SHA1.hexdigest(string_to_hash)
  end
  
end
