json.array!(@product_groups) do |product_group|
  json.extract! product_group, :id, :name, :description, :product_trail, :product_attribute, :created_by, :updated_by
  json.url product_group_url(product_group, format: :json)
end
