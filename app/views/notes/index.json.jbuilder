json.array!(@notes) do |note|
  json.extract! note, :id, :user_id, :contract_id, :description
  json.url note_url(note, format: :json)
end
