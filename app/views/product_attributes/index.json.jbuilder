json.array!(@product_attributes) do |product_attribute|
  json.extract! product_attribute, :id, :product_id, :custom_attr_id, :value
  json.url product_attribute_url(product_attribute, format: :json)
end
