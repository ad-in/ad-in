json.array!(@location_groups) do |location_group|
  json.extract! location_group, :id, :name, :description, :location_trail, :location_attribute, :created_by, :updated_by
  json.url location_group_url(location_group, format: :json)
end
