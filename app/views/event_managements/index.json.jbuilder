json.array!(@event_managements) do |event_management|
  json.extract! event_management, :id, :name, :description, :start_date, :end_date, :created_by, :product_scope_trail, :location_scope_trail
  json.url event_management_url(event_management, format: :json)
end
