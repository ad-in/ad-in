json.array!(@custom_attributes) do |custom_attributes|
  json.extract! custom_attributes, :id, :name, :creator, :description, :value, :type, :created_at, :updated_at
  json.url custom_attributes_url(custom_attributes, format: :json)
  
end
