json.extract! @price_grid, :id, :name, :description, :price_vehicle_id, :price_rule_id, :from_price, :to_price, :price_candidates, :multiples, :created_by, :updated_by, :created_at, :updated_at
