json.array!(@price_rules) do |price_rule|
  json.extract! price_rule, :id, :name, :description, :marketing_vehicle, :min_margin, :max_margin, :min_price_change, :max_price_change, :min_passthrough, :max_passthrough, :optimization_goal 
  json.url price_rule_url(price_rule, format: :json)
end
