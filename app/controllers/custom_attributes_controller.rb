class CustomAttributesController < ApplicationController
 
  def index
    @custom_attributes= CustomAttribute.all
  end
 
  def new
    @custom_attributes = CustomAttribute.new

  end

  def add
    @custom_attributes = CustomAttribute.new(custom_attributes_params)

    if @custom_attributes.save
      render 'custom_attributes/add'
    else
      render 'new'
    end
  end

  def update
    @custom_attributes = CustomAttribute.find(params[:id])
    respond_to do |format|

      if @custom_attributes.update(custom_attribute_params)
        format.html { redirect_to(@custom_attributes, :notice => 'name was successfully updated.') }
        format.json { head :ok }
      else
        @custom_attributes= CustomAttribute.all.paginate(page: params[:page], per_page:5)
        format.html { render :action => "index" }
        format.json { respond_with_bip(@custom_attributes) }
      end
    end

  end

  def destroy
    @custom_attributes = CustomAttribute.find(params[:id])
    @custom_attributes.destroy
    respond_to do |format|
      format.html { redirect_to(@custom_attributes, :notice => 'name was successfully Deleted.') }
      format.json { head :no_content }
    end
  end


  private

  def set_custom_attributes
    @custom_attributes = CustomAttribute.find(params[:name])
  end

 
  def custom_attributes_params
    params.require(:custom_attributes).permit(:name,:description,:value,:attribute_type,:id)
  end

  def custom_attribute_params
    params.require(:custom_attribute).permit(:name,:description,:value,:attribute_type,:id)
  end
end
