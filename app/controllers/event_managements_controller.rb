class EventManagementsController < ApplicationController
  before_action :set_event_management, only: [:show, :edit, :update, :destroy]

  # GET /event_managements
  # GET /event_managements.json
  def index
    @event_managements = EventManagement.all
  end

  # GET /event_managements/1
  # GET /event_managements/1.json
  def show
  end

  # GET /event_managements/new
  def new
    @event_management = EventManagement.new
  end

  # GET /event_managements/1/edit
  def edit
  end

  # POST /event_managements
  # POST /event_managements.json
  def create
    @event_management = EventManagement.new(event_management_params)

    respond_to do |format|
      if Api::EventManagementServices.addEvent(@event_management)
        format.html { redirect_to @event_management, notice: 'Event management was successfully created.' }
        format.json { render :show, status: :created, location: @event_management }
      else
        format.html { render :new }
        format.json { render json: @event_management.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /event_managements/1
  # PATCH/PUT /event_managements/1.json
  def update
    respond_to do |format|
      if Api::EventManagementServices.updateEvent(@event_management,event_management_params)
        format.html { redirect_to @event_management, notice: 'Event management was successfully updated.' }
        format.json { render :show, status: :ok, location: @event_management }
      else
        format.html { render :edit }
        format.json { render json: @event_management.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /event_managements/1
  # DELETE /event_managements/1.json
  def destroy
    respond_to do |format|
     if Api::EventManagementServices.removeEvent(@event_management)
    
      format.html { redirect_to event_managements_url, notice: 'Event management was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event_management
      @event_management = EventManagement.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_management_params
      params.require(:event_management).permit(:name, :description, :start_date, :end_date, :created_by, :product_scope_trail, :location_scope_trail)
    end
end
