class GeoGroupsController < ApplicationController
  before_action :set_geo_group, only: [:show, :edit, :update, :destroy]

  # GET /geo_groups
  # GET /geo_groups.json
  def index
    @geo_groups = GeoGroup.all
  end

  # GET /geo_groups/1
  # GET /geo_groups/1.json
  def show
  end

  # GET /geo_groups/new
  def new
    @geo_group = GeoGroup.new
  end

  # GET /geo_groups/1/edit
  def edit
  end

  # POST /geo_groups
  # POST /geo_groups.json
  def create
    @geo_group = GeoGroup.new(geo_group_params)

    respond_to do |format|
      if @geo_group.save
        format.html { redirect_to @geo_group, notice: 'Geo group was successfully created.' }
        format.json { render :show, status: :created, location: @geo_group }
      else
        format.html { render :new }
        format.json { render json: @geo_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /geo_groups/1
  # PATCH/PUT /geo_groups/1.json
  def update
    respond_to do |format|
      if @geo_group.update(geo_group_params)
        format.html { redirect_to @geo_group, notice: 'Geo group was successfully updated.' }
        format.json { render :show, status: :ok, location: @geo_group }
      else
        format.html { render :edit }
        format.json { render json: @geo_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /geo_groups/1
  # DELETE /geo_groups/1.json
  def destroy
    @geo_group.destroy
    respond_to do |format|
      format.html { redirect_to geo_groups_url, notice: 'Geo group was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_geo_group
      @geo_group = GeoGroup.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def geo_group_params
      params.require(:geo_group).permit(:id,:name, :description, :geo_trail, :custom_attribute, :location_ids)
    end
end
