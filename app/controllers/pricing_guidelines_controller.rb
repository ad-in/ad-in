require '../AdInServices/app/models/api/product_services.rb'
require '../AdInServices/app/models/api/location_services.rb'
require '../AdInServices/app/models/product_attribute.rb'
require '../AdInServices/app/models/product_attribute.rb'

class PricingGuidelinesController < ApplicationController
  skip_before_filter :verify_authenticity_token
  respond_to :html, :json
  ActionController::Parameters.permit_all_parameters = true
  def index
    @allCategoriesHierarchy = ALL_CATEGORIES_IN_HASH
    @allLocationsHierarchy = ALL_BANNERS_IN_HASH
    @allProductAttribute=ALL_PRODUCT_ATTRIBUTE
    @allLocationAttribute=ALL_LOCATION_ATTRIBUTE
    @allPriceRulesForAllCategory=Api::PriceRulesServices.getAllPriceRulesForAllCategory
    @allPriceGridsForAllCategory=Api::PriceGridsServices.getAllPriceGridsForAllCategory
    @allPriceRulesForAllLocation=Api::PriceRulesServices.getAllPriceRulesForAllLocation
    @allPriceGridsForAllLocation=Api::PriceGridsServices.getAllPriceGridsForAllLocation
    logger.info { "ALL_PRODUCT_ATTRIBUTE =#{@allPriceRulesForAllCategory}" }

  end

  #################################################################################################################################

  # POST /pricing_guidelines
  # POST /pricing_guidelines.json
  def create_price_rules
    @price_rules=  params[:price_rule]
    category_id=params[:price_rule][0][:category_id]
    location_id=params[:price_rule][0][:location_id]
    params[:price_rule].each do |price_rule|
      price_rule=PriceRule.create(price_rule)
    end
    @price_rules=PriceRule.get_price_rules(category_id,location_id)

    respond_to do |format|
      format.html { render partial: 'priceRule', status: :ok  }
      format.json { render json: @price_rules, status: :ok }
      format.js
    end
  end

  #############################################################################################################################

  # POST /pricing_guidelines
  # POST /pricing_guidelines.json
  def create_price_grids
    @price_grid=  params[:price_grid]
    category_id=params[:price_grid][0][:category_id]
    location_id=params[:price_grid][0][:location_id]
    @flatPriceMultiple = Array.new
    @multiplePriceMultiples=Array.new
    params[:price_grid].each do |price_grid|
      price_grid['category_id']= category_id
      price_grid['location_id']= location_id
      to_price=price_grid['to_price']
      from_price=price_grid['from_price']

      if((!to_price.blank? && !from_price.blank? ) && (to_price.to_f < from_price.to_f) )
        flash.now[:error]="Please enter valid To Price and From Price"
      else if(price_grid['price_vehicle_id'].to_i==1)
          @flatPrice=price_grid
          multiple=price_grid['multiples']
          logger.info { "finally multiple =#{multiple}" }
          @flatPriceMultiple.push(multiple)

        else

          logger.info { "finally @flatPriceMultiple =#{@flatPriceMultiple}" }
          price_grid=PriceGrid.create(price_grid)

          logger.info { "finally location_id =#{location_id}" }

        end

      end
    end
    @flatPrice['multiples']=@flatPriceMultiple.to_s.delete!'"'
    price_grid=PriceGrid.create(@flatPrice)

    @price_grids=PriceGrid.get_price_grids(category_id,location_id)
    respond_to do |format|
      format.html { render partial: 'priceGrid' ,status: :ok }
      format.json { render json: @price_grids, status: :ok }
      format.js

    end
  end

  ##############################################################################################################################
  def get_price_rules
    category_id=params[:category_id]
    location_id=params[:location_id]
    product_attribute=params[:product_attribute_hash]
    location_attribute=params[:location_attribute_hash]
    @price_rules=PriceRule.get_price_rules(category_id,location_id)
    respond_to do |format|
      format.html { render partial: 'priceRule', status: :ok  }
      format.json { render json: @price_rules, status: :ok }
      format.js
    end

  end

  #################################################################################################################

  def get_price_rules_attributes
    category_id=params[:category_id]
    location_id=params[:location_id]

    if (!category_id.blank?)

      @allProductAttribute= Api::ProductServices.getAllProductAttributesForCategory(category_id)
      respond_to do |format|
        format.html { render partial: 'getProductAttribute', status: :ok  }
        format.json { render json: @allProductAttribute, status: :ok }
        format.js
      end
    else

      @allProductAttribute=ALL_PRODUCT_ATTRIBUTE
      respond_to do |format|
        format.html { render partial: 'getProductAttribute', status: :ok  }
        format.json { render json: @allProductAttribute, status: :ok }
        format.js
      end
    end
  end

  ####################################################################################################
  def get_price_grids
    category_id=params[:category_id]
    location_id=params[:location_id]
    product_attribute=params[:product_attribute_hash]
    location_attribute=params[:location_attribute_hash]
    @price_grids=PriceGrid.get_price_grids(category_id,location_id)
    respond_to do |format|
      format.html { render partial: 'priceGrid' ,status: :ok }
      format.json { render json: @price_grids, status: :ok }
      format.js

    end
  end

  ###########################################################################################

  # DELETE /pricing_guidelines/1

  # DELETE /pricing_guidelines/1.json
  def destroy
    @price_grid.destroy
    @price_rules=PriceRule.get_price_rules(category_id,location_id)

    logger.info { "finally in destroy method" }
  end

  #########################################################################################

  def show
    @price_rule = PriceRule.where("id=#{params[:id]}")
    category_id=@price_rule[0].category_id
    location_id=@price_rule[0].location_id
    @price_rule.destroy(params[:id])
    redirect_to pricing_guidelines_url
    logger.info { "finally in destroy method" }
  end
end
