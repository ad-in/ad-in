class PriceGridsController < ApplicationController
  before_action :set_price_grid, only: [:show, :edit, :update, :destroy]

  # GET /price_grids
  # GET /price_grids.json
  def index
    @price_grids = PriceGrid.all
    logger.info { "finally in the logger" }
    
  end

  # GET /price_grids/1
  # GET /price_grids/1.json
  def show
  end

  # GET /price_grids/new
  def new
    @price_grid = PriceGrid.new
  end

  # GET /price_grids/1/edit
  def edit
  end

  # POST /price_grids
  # POST /price_grids.json
  def create
    @price_grid = PriceGrid.new(price_grid_params)

    respond_to do |format|
      if @price_grid.save
        format.html { redirect_to @price_grid, notice: 'Price grid was successfully created.' }
        format.json { render :show, status: :created, location: @price_grid }
      else
        format.html { render :new }
        format.json { render json: @price_grid.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /price_grids/1
  # PATCH/PUT /price_grids/1.json
  def update
    respond_to do |format|
      if @price_grid.update(price_grid_params)
        format.html { redirect_to @price_grid, notice: 'Price grid was successfully updated.' }
        format.json { render :show, status: :ok, location: @price_grid }
      else
        format.html { render :edit }
        format.json { render json: @price_grid.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /price_grids/1
  # DELETE /price_grids/1.json
  def destroy
    @price_grid.destroy
    respond_to do |format|
      format.html { redirect_to price_grids_url, notice: 'Price grid was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_price_grid
      @price_grid = PriceGrid.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def price_grid_params
      params.require(:price_grid).permit(:name, :description, :price_vehicle_id, :price_rule_id, :from_price, :to_price, :price_candidates, :multiples, :created_by, :updated_by)
    end
end
