require '../AdInServices/app/models/api/location_services.rb'
require '../AdInServices/app/models/api/product_services.rb'
require '../AdInServices/app/models/api/marketing_vehicle_services.rb'
require '../AdInServices/app/models/api/location_group_services.rb'
require '../AdInServices/app/models/api/version_store_services.rb'
require '../AdInServices/app/models/api/version_services.rb'
require '../AdInServices/app/models/api/product_group_services.rb'
require '../AdInServices/app/models/contract.rb'
require '../AdInServices/app/models/location_group.rb'
require '../AdInServices/app/models/product_group.rb'
require '../AdInServices/app/models/tier.rb'
require '../AdInServices/app/models/tier_product.rb'
require '../AdInServices/app/models/note.rb'
require '../AdInServices/app/models/version.rb'
require '../AdInServices/app/models/version_store.rb'
require '../AdInServices/app/models/api/contract_services.rb'
require '../AdInServices/app/models/contract.rb'
require '../AdInServices/app/models/price_vehicle.rb'
require "json"

class ContractController < ApplicationController
  skip_before_action :verify_authenticity_token
  @@contract_id = "";
  @@tier_id = "";
  @@version_id ="";
  @@trial_id = "";
  def new
  end

  def create
  end

  def modify
  end

  def createNewContract

    session_contract = Hash.new
    @contract = Contract.new
    @contract.sequence = 0
    @contract.save
    @contract = Contract.find_by_trial_id(@contract.id)
    session_contract.store("contract_id", @contract.contract_id)

    #temporary - remove it
    @@contract_id = @contract.contract_id

    contract_versions_array = Array.new
    contract_version = Hash.new
    @version = Version.new
    @version.trial_id = @contract.trial_id
    @version.save!

    #temporary - remove it
    @@version_id = @version.id
    contract_version.store("version_id", @version.version_id)

    version_tiers = Array.new
    version_tier = Hash.new

    @tier = Tier.new
    @tier.version_id = @version.id
    @tier.sequence = 0
    @tier.contract_id = @@contract_id
    @tier.save!
    version_tier.store("tier_id", @tier.tier_id)
    version_tiers.push(version_tier)

    #temporary - remove it
    @@tier_id = @tier.id
    # session[:tier_id] = @tier.tier_id
    contract_version.store("version_tiers", version_tiers)
    # contract_version.store("contract_versions_array", contract_versions_array)
    contract_versions_array.push(contract_version)
    session_contract.store("versions",contract_versions_array)
    session[:contract] = session_contract
    logger.info"+++ session in scratch contract = #{session[:contract]}"
    # logger.info"++++ current session tier set to = #{session[:tier_id]} in contract controller"

    # temporarty remove it
    @@trial_id=@contract.trial_id

    @allCategoriesHierarchy = ALL_CATEGORIES_IN_HASH
    @marketingVehicles = Api::MarketingVehicleServices.getAllMarketingVehicles

    redirect_to contract_newFromScratch_path
  #
  end

  def newFromScratch
    # localTrialId  = Api::ContractServices.createNewContract(nil)
    # logger.debug{"---in scratch----localTrialId------#{localTrialId}"}
    @allCategoriesHierarchy = ALL_CATEGORIES_IN_HASH
    @marketingVehicles = Api::MarketingVehicleServices.getAllMarketingVehicles
    logger.debug{"---in scratch-----------#{@@contract_id}"}
    # @productGroups = Api::ProductGroupServices.getAllProductGroups
    @productGroupsHash = Api::ProductGroupServices.getAllProductsGroupsInHash
    @allBannersHierarchy = ALL_BANNERS_IN_HASH
    @custom_location_attributes = Api::LocationServices.getAllCustomAttr
    @location_groups = Api:: LocationGroupServices.getAllLocationGroupsInHash
    #@location_groups = LocationGroup.all
    #@location_group = LocationGroup.new
    @contract_id = @@contract_id
    @trial_id=@@trial_id
    @tier_id = @@tier_id
    @tier=Contract.get_tier_byTierId(@tier_id)
    @tier_product=Contract.get_tier_product(@trial_id)
    @veriosnableAttributes = CustomAttribute.getAllVersionableCustomAttributes
  end

  def create_tier
    contract_id = @@contract_id
    trial_id=@@trial_id
    by_product=params[:by_product]
    by_contract=params[:by_contract]

    contract_description=params[:contract_description]
    logger.info { "finally in the logger =#{by_product},#{by_contract}" }

    #converting dates into date range

    vender_funding_dates=params[:vender_funding_dates]

    puts vender_funding_dates
    dates = vender_funding_dates.split("-")
    logger.info { "finally dates =#{dates[0].gsub(" ","")}" }
    start_date= Date.strptime(dates[0].gsub(" ",""), '%m/%d/%Y')
    end_date= Date.strptime(dates[1].gsub(" ",""), '%m/%d/%Y')
    tier_dates=("[#{start_date},#{end_date})")
    logger.info { "finally in the loggers =#{start_date},#{end_date}" }

    #creating vendor information hash
    name=params[:vendor_name]
    id=params[:vendor_id]
    description= params[:vendor_description]
    vendor_info=Hash.new()
    vendor_info.store('vendor_name', name)
    vendor_info.store('vendor_id', id)
    vendor_info.store('vendor_description', description)

    #creating funding hash
    off_invoice=params[:off_invoice]
    bill_back=params[:bill_back]
    scan=params[:scan]
    subsidy_off_invoice=params[:subsidy_off_invoice]
    subsidy_bill_back=params[:subsidy_bill_back]
    subsidy_scan=params[:subsidy_scan]
    base_cost=0
    funding={'off_invoice' => off_invoice , 'bill_back' => bill_back, 'scan' => scan,'subsidy_bill_back' => subsidy_bill_back,'subsidy_off_invoice' => subsidy_off_invoice,'subsidy_scan' => subsidy_scan}

    #creating unit funding for tier_products
    unit_funding = ((off_invoice.to_f) - (0.2 * (base_cost - off_invoice.to_f) + bill_back.to_f + scan.to_f) + (subsidy_off_invoice.to_f + subsidy_bill_back.to_f + subsidy_scan.to_f))

    #creating vendor funding money tier_products
    vendor_funding_money=off_invoice.to_f+bill_back.to_f+scan.to_f

    #creating cost for tier_products
    net_cost=base_cost-unit_funding

    #creating  subsidy funding money tier_products
    subsidy_funding_money=subsidy_off_invoice.to_f+subsidy_bill_back.to_f+subsidy_scan.to_f

    #updating tier
    @contract=Contract.where("contract_id=#{contract_id}")
    @contract.each do |contract|
      contract.description=contract_description
      contract.save

    end

    @tier=Contract.get_tier(contract_id)
    logger.info { "finally in the logger =#{@tier}" }
    @tier.each do |tier|
      logger.info { "finally in the logger =#{tier.contract_id}" }
      tier.tier_dates=tier_dates
      tier.vendor_info=vendor_info.to_json
      tier.funding=funding.to_json
      tier.unit_funding=unit_funding.to_f
      tier.save

      #updating tier_products
      @parmeter=0
      @tier_products=Contract.get_tier_product(trial_id)
      @tier_products.each do |tier_products|
        if(by_product==nil)
        tier_products.product_funding=funding.to_json
        tier_products.unit_funding=unit_funding.to_f
        tier_products.vendor_funding=vendor_funding_money
        tier_products.subsidy_funding=subsidy_funding_money
        tier_products.cost=net_cost
        tier_products.save

        else
          logger.info { "finally in the logger =#{params[:product_funding][@parmeter]}" }

          #creating funding hash
          logger.info { "finally tier =#{params[:product_funding][@parmeter][:off_invoice]}"}
          off_invoice=params[:product_funding][@parmeter][:off_invoice]
          bill_back=params[:product_funding][@parmeter][:bill_back]
          scan=params[:product_funding][@parmeter][:scan]
          subsidy_off_invoice=params[:product_funding][@parmeter][:subsidy_off_invoice]
          subsidy_bill_back=params[:product_funding][@parmeter][:subsidy_bill_back]
          subsidy_scan=params[:product_funding][@parmeter][:subsidy_scan]
          base_cost=0
          funding={'off_invoice' => off_invoice , 'bill_back' => bill_back, 'scan' => scan,'subsidy_bill_back' => subsidy_bill_back,'subsidy_off_invoice' => subsidy_off_invoice,'subsidy_scan' => subsidy_scan}

          #creating unit funding for tier_products
          unit_funding = ((off_invoice.to_f) - (0.2 * (base_cost - off_invoice.to_f) + bill_back.to_f + scan.to_f) + (subsidy_off_invoice.to_f + subsidy_bill_back.to_f + subsidy_scan.to_f))

          #creating vendor funding money tier_products
          vendor_funding_money=off_invoice.to_f+bill_back.to_f+scan.to_f

          #creating cost for tier_products
          net_cost=base_cost-unit_funding

          #creating  subsidy funding money tier_products
          subsidy_funding_money=subsidy_off_invoice.to_f+subsidy_bill_back.to_f+subsidy_scan.to_f
          tier_products.product_funding=funding.to_json
          tier_products.unit_funding=unit_funding.to_f
          tier_products.vendor_funding=vendor_funding_money
          tier_products.subsidy_funding=subsidy_funding_money
          tier_products.cost=net_cost
          tier_products.save
          @parmeter=@parmeter+1

        end

      end

    end

    render :nothing => true
  end

  def getProducts
    @products = Api::ProductServices.getProductsByCategory(params[:category])
    respond_to do |format|
      format.json { render json: @products, status: :ok}
      format.js
    end
  end

  def storeProducts
    # debugger
    # logger.debug{"---------------in store products----------"}
    productIds = params[:productIds]
    productObjects = nil
    # contract_id = @current_contract.contract_id
    session_contract = session[:contract]
    if session_contract.has_key?"versions"
      versionArray = session_contract.fetch("versions")
      logger.info"++++versionArray is = #{versionArray.size}"
      if !versionArray.empty?
        versionArray.each do |version|
          versionHash = version
          logger.info"++++versionHash is = #{versionHash}"
          if versionHash.has_key?"version_tiers"
            tierArray = versionHash.fetch("version_tiers")
            logger.info"++++tierArray is = #{tierArray}"
            tier_products_array = Array.new
            prodList = Array.new
            if !tierArray.empty?
              tierArray.each do |tier|
                tierHash = tier
                tier_id = tier.fetch('tier_id')
                contract_id = session_contract.fetch("contract_id")
                trial_id = Contract.find_by_contract_id(contract_id).trial_id
                logger.info"+++ tier_id = #{tier_id} and trial_id = #{trial_id} and contract_id = #{contract_id}"
                #   # @tier_id = @current_tier.tier_id
                productIds.each do |product|
                  @tier_product = TierProduct.find_by_tier_id_and_product_id(tier_id, product)

                  if @tier_products.blank?
                    @tier_product = TierProduct.new
                    @tier_product.tier_id = tier_id
                    @tier_product.trial_id = trial_id
                    @tier_product.contract_id = contract_id
                    @tier_product.product_id = product
                    @tier_product.save!
                    tier_products_array.push(createProductHash(@tier_product))
                    logger.info"+++tier prodcut saved = #{tier_products_array}"
                  end
                  trProdList = TierProduct.where("tier_id = #{tier_id}")
                  trProdList.each do |row|
                    logger.debug{"----------------#{(row['product_id']).to_s}"}
                    prodList.push(row['product_id'])
                  end
                  productObjects = Api::ProductServices.getProductsByIds(prodList)
                end
                tierHash.store('tier_products_array', tier_products_array)
              end
            end
          end
        end
      end
    end

    # session_contract.store('versions', versionArray.to_json)
    session_contract.store('productList', productObjects)
    session[:contract] = session_contract
    logger.info"+++ session in product with new tier hash next = #{session[:contract]}"
    respond_to do |format|
      format.json { render json: productObjects, status: :ok}
      format.js
    end
  end

  def storeProductGroups
    logger.debug{"---------------in store groups----------"}
    product_groups = ProductGroup.where(:id=>[params[:groupIds]])
    allProductGroups = product_groups
    contract_id = @@contract_id
    localcontract = Contract.find_by_contract_id(contract_id)
    logger.debug{"-------------------------"}

    value = Array.new
    allProductGroups.each{|productGroup|
      productsForGroup = Api::ProductGroupServices.getAllProductsFromProductGroup(productGroup['id'])
      if productsForGroup != nil
        productsForGroup.each{|everyList|
          arrayOfPids = everyList['pids']
          arrayOfPids = arrayOfPids.gsub("{","")
          arrayOfPids = arrayOfPids.gsub("}","")
          arrayOfPids = arrayOfPids.split(",").map {|s| s.to_i}
          productsArrayReturned = Api::ProductServices.getProductsByIds(arrayOfPids)
          productsArrayReturned.each{|everyRow|
            value.push(everyRow)
          }
        }
      end
    }

    @tier_id = @@tier_id
    value.each{|product|
      @tier_product = TierProduct.find_by_tier_id_and_product_id(@tier_id,product['product_id'])
      if @tier_product.blank?
        @tier_product = TierProduct.new
        @tier_product.tier_id = @@tier_id
        @tier_product.trial_id = localcontract.trial_id
        @tier_product.contract_id = localcontract.contract_id
        @tier_product.product_id = product['product_id']
      @tier_product.save!
      else
      end
    }

    productGroupObjects = Api::ProductGroupServices.getProductGroupByIds(params[:groupIds])
    respond_to do |format|
      format.json { render json: productGroupObjects, status: :ok}
      format.js
    end

  end

  def saveDate
    contract_id = params[:contract_id]
    promotionDate = params[:promotionDate]
    dates = promotionDate.split("-")
    puts dates
    start_date= Date.strptime(dates[0].gsub(" ",""), '%m/%d/%Y')
    end_date= Date.strptime(dates[1].gsub(" ",""), '%m/%d/%Y')
    promotion_window = "[#{start_date},#{end_date})"
    localContract = Contract.find_by_contract_id(contract_id)
    localContract.promotion_window = promotion_window
    localContract.save!
    logger.debug{localContract.promotion_window}
    # @trial_id=@current_contract.trial_id
    # logger.info { "contract trial id is =#{@trial_id}" }
    @tier_id=@@tier_id
    @product_data=Contract.get_tier_product_info_byTierId(@tier_id)
    logger.info { "contract @product_data =#{@product_data}" }
    session_contract = session[:contract]
    session_contract.store('contract_promotion_window', promotion_window)

    session_contract = session[:contract]
    if session_contract.has_key?"versions"
      versionArray = session_contract.fetch("versions")
      logger.info"++++versionArray is = #{versionArray.size}"
      if !versionArray.empty?
        versionArray.each do |version|
          versionHash = version
          logger.info"++++versionHash is = #{versionHash}"
          if versionHash.has_key?"version_tiers"
            tierArray = versionHash.fetch("version_tiers")
            logger.info"++++tierArray is = #{tierArray}"
            tier_products_array = Array.new
            prodList = Array.new
            if !tierArray.empty?
              tierArray.each { |tier|
                tier.store("tier_dates", promotion_window)
              }
            end
          end
        end
      end
    end
    session[:contract] = session_contract
    logger.info"+++ session in dates next = #{session[:contract]}"
    respond_to do |format|
      format.json { render json: @product_data, status: :ok}
      format.js
    end
  end

  def saveMarketingVehicle
    contract_id = @@contract_id
    localcontract = Contract.find_by_contract_id(contract_id)
    trial_id = localcontract.trial_id
    @versions = Version.where("trial_id=#{trial_id}")
    logger.debug{"------------#{@versions}"}
    @versions[0].marketing_vehicle_id = params[:mkVehicleId].to_i
    @versions[0].save!

  end

  def addNote
    @note = Note.new
    @note.contract_id = params[:contract_id]

    @note.user_id = params[:user_id]

    @note.description = params[:description]

    @note.save!
  end

  def getAllNote
    @notes = Api::ContractServices.getAllNotes(params[:contract_id])
    logger.debug{"-----Notes----#{@notes}"}
    respond_to do |format|
      format.json { render json: @notes, status: :ok}
      format.js
    end
  end

  def add_location_group
    @location_group = LocationGroup.new(:name => params[:name], :description => params[:description])
    custom_attr_id = params[:custom_attr_id] unless params[:custom_attr_id].blank?
    custom_attr_val = params[:custom_attr_val] unless params[:custom_attr_val].blank?
    location_attribute = Hash.new
    location_attribute.store(custom_attr_id, custom_attr_val)
    @location_group.location_trail = Api::LocationGroupServices.createLocationTrail(params[:location_id]).to_json unless params[:location_id].blank?
    @location_group.location_attribute = location_attribute unless location_attribute.blank?
    @location_group.created_by = current_user.id
    @location_group.lids = params[:storeIdArray] unless params[:storeIdArray].blank?
    respond_to do |format|
      if Api::LocationGroupServices.addLocationGroup(@location_group)
        format.json { render json: @location_group, status: :ok}
      format.js
      else
        format.json { render json: @location_group.errors, status: :unprocessable_entity }
      format.js
      end
    end
  end

  def getStores
    @stores = Api::LocationServices.getStoresByBanner(params[:banner])
    respond_to do |format|
      format.json { render json: @stores, status: :ok}
      format.js
    end
  end

  def storeLocations
    logger.debug{"---------------in store locations----------"}
    # locationIds = params[:locationIds]
    # # contract_id = @current_contract.contract_id
    # localcontract = Contract.find_by_contract_id(@current_contract.contract_id)
    # logger.debug{"-------------------------"}
    # localversion = Version.find_by_trial_id(localcontract.trial_id)
    # locationIds.each{|location|
    #   @version_store = VersionStore.find_by_version_id_and_store_id(localversion.version_id, location)
    #    if @version_store.blank?
    #     @version_store = VersionStore.new
    #     @version_store.trial_id = localcontract.trial_id
    #     @version_store.version_id = localversion.version_id
    #     @version_store.store_id = location
    #     @version_store.save!
    #   else
    #     # flash[:error] ="Version store combination alredy exist for version #{localversion.version_id}"
    #   end
    # }

    # storeArr = []
    # verStrList = VersionStore.where(:version_id => localversion.version_id)
    # verStrList.each{|row|
    #   storeArr << row['store_id']
    # }
    # logger.info"+++ ids in storeArr is #{storeArr}"
    # localcontract.update_attributes(:store_ids => storeArr)
    # locationObjects = Api::LocationServices.getStoresByIds(storeArr)
    locationObjects = nil
    session_contract = session[:contract]
    contract = nil
    if session_contract.has_key?"contract_id"
      contract_id = session_contract.fetch("contract_id")
      contract = Contract.find_by_contract_id("#{contract_id}")
      logger.info"contract is true #{contract}"
    end
    if session_contract.has_key?"versions"
      versionArray = session_contract.fetch("versions")
      version_store_array = []
      storeArr = []
      logger.info"++++versionArray is = #{versionArray.size}"
      if !versionArray.empty?
        versionArray.each do |version|
          versionHash = version
          logger.info"++++versionHash is = #{versionHash}"
          if versionHash.has_key?"version_id"
            version_id = versionHash.fetch("version_id")
            version_stores = Api::VersionStoreServices.storeLocationAndGroup(contract, version_id, params)
            version_store_array << version_stores
            verStrList = VersionStore.where(:version_id => version_id)
            verStrList.each do |row|
              storeArr << row['store_id']
            end
            logger.info"+++ ids in storeArr is #{storeArr}"
            contract.update_attributes(:store_ids => storeArr)
            locationObjects = Api::LocationServices.getStoresByIds(storeArr)
          end
        end
        session_contract.store('version_stores', version_store_array)
      end
    end

    # locationObjects = Api::VersionStoreServices.storeLocationAndGroup(@current_contract, params)
    session_contract.store('storeList', storeArr)
    session[:contract] = session_contract
    logger.info"+++ session in location next = #{session[:contract]}"
    respond_to do |format|
      flash.now[:success] = "Location Saved"
      format.json { render json: locationObjects, status: :ok}
      format.js
    end
  end

  def removeStore
    storeIds = params[:storeIds]
    contract_id = @@contract_id
    localcontract = Contract.find_by_contract_id(contract_id)
    version = Version.find_by_trial_id(localcontract.trial_id)
    Api::LocationServices.removeStorefromContract(storeIds, version.version_id)
    storeArr = []
    verStrList = VersionStore.where(:version_id => version.version_id)
    verStrList.each{|row|
      storeArr << row['store_id']
    }
    logger.info"+++ ids in storeArr is #{storeArr}"
    localcontract.update_attributes(:store_ids => storeArr)
    locationObjects = Api::LocationServices.getStoresByIds(storeArr)
    respond_to do |format|
      format.json { render json: locationObjects, status: :ok}
      format.js
    end
  end

  def storeLocationGroups
    logger.debug{"---------------in store locations----------"}
    location_groups = LocationGroup.where(:id=>[params[:groupIds]])
    contract_id = @@contract_id
    localcontract = Contract.find_by_contract_id(contract_id)
    logger.debug{"-------------------------"}
    localversion = Version.find_by_trial_id(localcontract.trial_id)
    location_groups.each do |lg|
      if !lg.lids.blank?
        lg.lids.each do |v|
          @version_store = VersionStore.find_by_version_id_and_store_id(localversion.version_id, v)
          if @version_store.blank?
            @version_store = VersionStore.new
          @version_store.trial_id = localcontract.trial_id
          @version_store.version_id = localversion.version_id
          @version_store.store_id = v
          @version_store.save!
          else
          end
        end
      end
    end
    storeArr = []
    verStrList = VersionStore.where(:version_id => localversion.version_id)
    verStrList.each do |row|
      storeArr << row['store_id']
    end
    logger.info"+++ ids in storeArr is #{storeArr}"
    localcontract.update_attributes(:store_ids => storeArr)
    locationGroupObjects = Api::LocationGroupServices.getLocationGroupByIds(params[:groupIds])
    respond_to do |format|
      format.json { render json: locationGroupObjects, status: :ok}
      format.js
    end

  end

  def getStoresByCustAttr
    @stores = Api::LocationServices.getStoresByCustomAttr(params[:custattrid], params[:custattrval])
    respond_to do |format|
      format.json { render json: @stores, status: :ok}
      format.js
    end
  end

  def delete

  end

  def priceVehicleProducts
    contract_id = @@contract_id
    if contract_id != nil
      localcontract = Contract.find_by_contract_id(contract_id)
      trial_id = localcontract.trial_id
      finalObjectsArray = Array.new
      @tier_products = TierProduct.where("tier_id=#{@@tier_id}")
      logger.debug{"=====yrdydrydr=====#"}
      @tier_products.each do |tier_product|
        @product_info=Hash.new()
        @product = Product.where("product_id=#{tier_product.product_id}")
        @product.each{|prod|
          @product_info.store('product',prod.product_id)
          @product_info.store('product_name',prod.upc_desc)
        }
        @product_info.store('product_base_cost',"")
        @product_info.store('product_base_price',"")
        @product_info.store('product_unit_funding',tier_product.unit_funding)
        @product_info.store('product_net_cost',tier_product.cost)
        finalObjectsArray.push(@product_info)
      end

      respond_to do |format|
        format.json { render json: finalObjectsArray, status: :ok}
        format.js
      end
    end
  end

  def getProductsGroupsHash
    @productGroupsHash = Api::ProductGroupServices.getAllProductsGroupsInHash
    respond_to do |format|
      format.json { render json: @productGroupsHash, status: :ok}
      format.js
    end
  end

  def createTierForVendor
    logger.debug{"=====yrdydrydr=====#"}
    #copy main tier info to new tier in tier table
    if(params[:version_id]!="")
      @version_id = params[:version_id]
    else
      @version_id = @@version_id
    end
    @tier = Contract.get_tier0(@version_id)
    @newtier = Tier.new
    @newtier.version_id = @tier.version_id
    @newtier.tier_dates = @tier.tier_dates
    @newtier.price_vehicle_id = @tier.price_vehicle_id
    @newtier.funding = @tier.funding
    @newtier.vendor_info = @tier.vendor_info
    @newtier.contract_id = @tier.contract_id
    @newtier.unit_funding = @tier.unit_funding
    @newtier.lumpsum_funding = @tier.lumpsum_funding
    @sequence =  Api::ContractServices.getMaxTierSequenceNo(@version_id)
    logger.info"+++++++#{@sequence.max['max']}"
    @newtier.sequence = @sequence.max['max'].to_i + 1
    @newtier.save!

    #copy main tier product info to new tier product info in tier_product table
    @tier_products=Contract.get_tier_product_byTierId(@tier.tier_id)
    @tier_products.each do |tier_products|
      @tierPro = TierProduct.new
      @tierPro.tier_id = @newtier.tier_id
      @tierPro.trial_id = tier_products.trial_id
      @tierPro.product_id = tier_products.product_id
      @tierPro.cost = tier_products.cost
      @tierPro.contract_id = tier_products.contract_id
      @tierPro.unit_funding = tier_products.unit_funding
      @tierPro.save!
    end

    @product_data = Contract.get_tier_product_info_byTierId(@newtier.tier_id)

    @productDataHash = Hash.new

    @productDataHash.store("product_data",@product_data)
    @productDataHash.store("sequence",@newtier.sequence)
    @productDataHash.store("vendor_info", @newtier.vendor_info)
    @productDataHash.store("unit_funding", @newtier.unit_funding)
    @productDataHash.store("lumpsum_funding", @newtier.lumpsum_funding)
    @productDataHash.store("tier_id", @newtier.tier_id)
    @productDataHash.store("start_dates",@newtier.tier_dates.first)
    @productDataHash.store("end_dates",@newtier.tier_dates.end)
    logger.debug{"end of createTierForVendor#{@productDataHash}"}

    render partial: 'tierForm',
           format: :js,
           locals: {:c => @productDataHash}
  end

  def update_tier
    @parmeter=0
    @parmeterCount = 0
    session_contract = session[:contract]
    if session_contract.has_key?"versions"
      versionArray = session_contract.fetch("versions")
      logger.info"++++versionArray is = #{versionArray.size}"
      if !versionArray.empty?
        versionArray.each do |version|
          versionHash = version
          logger.info"++++versionHash is = #{versionHash}"
          if versionHash.has_key?"version_tiers"
            tierArray = versionHash.fetch("version_tiers")
            logger.info"++++tierArray is = #{tierArray}"
            tier_products_array = Array.new
            prodList = Array.new
            if !tierArray.empty?
              tierArray.each { |tier|

                tier_id=params[:tierData][@parmeter][:tier_id]
                logger.info { "finally in the update_tier =#{tier_id}" }

                #converting dates into date range

                vender_funding_dates=params[:tierData][@parmeter][:vender_funding_dates]

                puts vender_funding_dates
                dates = vender_funding_dates.split("-")
                logger.info { "finally dates =#{dates[0].gsub(" ","")}" }
                start_date= Date.strptime(dates[0].gsub(" ",""), '%m/%d/%Y')
                end_date= Date.strptime(dates[1].gsub(" ",""), '%m/%d/%Y')
                tier_dates=("[#{start_date},#{end_date})")
                logger.info { "finally in the loggers =#{start_date},#{end_date}" }

                #creating vendor information hash
                name=params[:tierData][@parmeter][:vendor_name]
                id=params[:tierData][@parmeter][:vendor_id]
                description= params[:tierData][@parmeter][:vendor_description]
                unit_funding = params[:tierData][@parmeter][:unit_funding]
                lumpsum_funding = params[:tierData][@parmeter][:lumpsum_funding]
                vendor_info=Hash.new()
                vendor_info.store('vendor_name', name)
                vendor_info.store('vendor_id', id)
                vendor_info.store('vendor_description', description)

                #updating contract
                #@contract=Contract.where("contract_id=#{contract_id}")
                #@contract.each do |contract|
                #  contract.description=contract_description
                #  contract.save
                #end

                #updating tier
                logger.info { "finally in the logger =#{tier}" }

                #tier.store()

                
                tier.store("tier_dates", tier_dates)
                 # tier.tier_dates=tier_dates
                tier.store("vendor_info",vendor_info) 
                #tier.vendor_info=vendor_info.to_json
                tier.store("unit_funding",unit_funding)
                #tier.unit_funding=unit_funding.to_f
                tier.store("lumpsum_funding",lumpsum_funding.to_f)
                #tier.lumpsum_funding=lumpsum_funding.to_f
                #tier.save
                @tier_products  = tier.fetch("tier_products_array")
                #@tier_products=Contract.get_tier_product_byTierId(tier_id)
                #@tier_products = ActiveSupport::JSON.decode(@tier_products)
                tier_products_updated_array = Array.new
                @tier_products.each do |tier_product|

                  tier_product_js = tier_product

                  base_cost = params[:product_funding][@parmeterCount][:base_cost]
                  unit_funding = params[:product_funding][@parmeterCount][:unit_funding]

                  #tier_products.unit_funding = unit_funding

                  tier_product_js["unit_funding"] = unit_funding

                  logger.debug{"tier_product unit funding #{tier_product_js}"}

                  #creating cost for tier_products
                  if(base_cost != "" && unit_funding!="")
                    #tier_products.cost =base_cost.to_f - unit_funding.to_f
                    tier_product_js["cost"] =(base_cost.to_f - unit_funding.to_f).to_s
                  end
                  tier_product = tier_product_js.to_json
                  tier_product_hash = JSON.parse(tier_product)

                  logger.debug{"-------------------tier product#{tier_product}"}
                  #tier_products.save
                  @parmeterCount=@parmeterCount+1
                  tier_products_updated_array.push(tier_product_hash)
                end
                tier.store("tier_products_array",tier_products_updated_array)
                # @tier_products = @tier_products.to_json
                logger.debug{"----------------in jason--------------------------"}
                @parmeter=@parmeter+1

              }
            end
          end
        end
      end
    end

    # session_contract.store('versions', versionArray.to_json)
    #session_contract.store('productList', productObjects)
    session[:contract] = session_contract
    logger.info"---------------- session in vendor funding with new tier hash next = #{session[:contract]}"

    #------------------------------------------
    contract_id = @@contract_id
    @parmeter=0
    @parmeterCount = 0
    # @tier=Contract.get_tier(contract_id)
    #logger.info { "finally in the logger =#{@tier}" }

    params[:tierData].each do |tierData|
    # logger.info { "finally in the logger =#{tier}" }
      tier_id=tierData[:tier_id]
      tier=Contract.get_tier_byTierId(tier_id)
      logger.info { "finally in the update_tier =#{tier_id}" }

      #converting dates into date range

      vender_funding_dates=params[:tierData][@parmeter][:vender_funding_dates]

      puts vender_funding_dates
      dates = vender_funding_dates.split("-")
      logger.info { "finally dates =#{dates[0].gsub(" ","")}" }
      start_date= Date.strptime(dates[0].gsub(" ",""), '%m/%d/%Y')
      end_date= Date.strptime(dates[1].gsub(" ",""), '%m/%d/%Y')
      tier_dates=("[#{start_date},#{end_date})")
      logger.info { "finally in the loggers =#{start_date},#{end_date}" }

      #creating vendor information hash
      name=params[:tierData][@parmeter][:vendor_name]
      id=params[:tierData][@parmeter][:vendor_id]
      description= params[:tierData][@parmeter][:vendor_description]
      unit_funding_contract = params[:tierData][@parmeter][:unit_funding]
      lumpsum_funding = params[:tierData][@parmeter][:lumpsum_funding]
      vendor_info=Hash.new()
      vendor_info.store('vendor_name', name)
      vendor_info.store('vendor_id', id)
      vendor_info.store('vendor_description', description)

      #updating tier
      logger.info { "finally in the logger =#{tier}" }
      tier.tier_dates=tier_dates
      tier.vendor_info=vendor_info.to_json
      tier.unit_funding=unit_funding_contract.to_f
      tier.lumpsum_funding=lumpsum_funding.to_f
      tier.save

      @tier_products=Contract.get_tier_product_byTierId(tier_id)
      @tier_products.each do |tier_product|

        base_cost = params[:product_funding][@parmeterCount][:base_cost]
        unit_funding = params[:product_funding][@parmeterCount][:unit_funding]
        base_price = params[:product_funding][@parmeterCount][:base_price]

        tier_product.unit_funding=unit_funding

        #creating cost for tier_products
        if(!base_cost.blank? && !unit_funding.blank?)
        tier_product.cost =base_cost.to_f - unit_funding.to_f
        elsif(!base_cost.blank? && !unit_funding_contract.blank?)
        tier_product.cost =base_cost.to_f - unit_funding_contract.to_f
        elsif(base_cost.blank?)
          note = Note.new
          note.contract_id = tier_product.contract_id
          note.user_id = current_user.id
          note.description = "For product '#{tier_product["product_name"]}' best cost is not Available"
        note.save!
        end
        tier_product.base_cost = base_cost
        tier_product.base_price = base_price

        tier_product.save
        @parmeterCount=@parmeterCount+1

      end
      @parmeter=@parmeter+1
    end

    #updating contract
    @contract=Contract.where("contract_id=#{@tier_products[0].contract_id}")
    @contract.each do |contract|
      contract.description=params[:contract_description]
      contract.save
    end

    render :nothing => true
  end

  def createTrial
    logger.debug{"=====yrdydrydr=====#"}
    #copy main tier info to new tier in tier table
    contract_id = params[:contract_id]
    new_trial_id = Api::ContractServices.createTrial(contract_id)
    @contract_object = Api::ContractServices.getContrctObject(contract_id)
    @price_vehicles = PriceVehicle.all
    logger.info"Trail_object=#{@contract_object}"
  # render :nothing => true
  end

  def breadcrumbCategory
    product_id = params[:id]
    product_trail = Api::ProductGroupServices.getProductTrail(product_id)
    product_name_array = Api::ProductServices.getCategoryNameArrayFromCategoryTrail(product_trail)
    render partial: 'breadcrumb',
           format: :js,
           locals: {:array => product_name_array}
  end

  def breadcrumbBanner
    banner_id = params[:id]
    location_trail = Api::LocationGroupServices.createLocationTrail(banner_id)
    location_name_array = Api::LocationServices.getLocationNameArrayFromLocationTrail(location_trail)
    render partial: 'breadcrumb',
           format: :js,
           locals: {:array => location_name_array}
  end

  def modifyContractAttr
    @contract_id = params[:contract_id]
    @contract_attributes = Api::ContractServices.getAllAttributeFromCustomAttributesByTableName("contract")
    @contract_product_attributes = Api::ContractServices.getAllAttributeFromCustomAttributesByTableName("contract_product")
    @product_array = Api::ProductServices.getProductInfoByContract_id(@contract_id)
  end

  def addContractAttribute

    Api::ContractServices.addContractAttribute(params)
  end

  def updatePriceVehical
    Api::ContractServices.addPriceVehicalInfoToContract(params)
  end

  def contractsMain
    contract_id = params[:contract_id]
    @contract_object_view = Api::ContractServices.getContrctObject(contract_id)
    logger.info"Contract=#{@contract_object_view}"

  end

  def createVersion
    logger.debug{"-----in create version----------"}
    zone_type = params[:versionBy]
    storeList = "";
    returnVersionIdsList = "";
    session_contract = session[:contract]
    if session_contract.has_key?"storeList"
      storeList = session_contract.fetch("storeList")
    end
    if session_contract.has_key?"versions"
      versionArray = session_contract.fetch("versions")
      logger.info"++++versionArray is = #{versionArray.size}"
      if !versionArray.empty?
        versionArray.each { |version|
          versionHash = version
          versionId = versionHash.fetch("version_id")
          @version = Version.find_by_version_id(versionId)
          returnVersionIdsList = Version.createVersions(zone_type ,storeList , @version)
        }
      end
    end

    @marketingVehicles = Api::MarketingVehicleServices.getAllMarketingVehicles

    @temp_hash =  Hash.new
    @temp_hash.store("returnVersionIdsList",returnVersionIdsList)
    @temp_hash.store("marketingVehicles",@marketingVehicles)
    render partial: 'marketing_vehicle_mutiple',
           format: :js,
           locals: {:temp => @temp_hash}

  end

  private

  def location_group_params
    params.require(:location_group).permit(:name, :description, :location_trail, :location_attribute, :created_by, :updated_by)
  end

  def createProductHash(tier_product)
    tier_product_hash = Hash.new
    tier_product_hash.store("tier_product_id",tier_product.product_id);
    tier_product_hash.store( "effective_price",tier_product.effective_price );
    tier_product_hash.store( "unit_funding",tier_product.unit_funding );
    tier_product_hash.store( "price_candidate_list",tier_product.price_candidate_list );
    tier_product_hash.store( "base_cost",tier_product.base_cost );
    tier_product_hash.store( "base_price",tier_product.base_price );
    return tier_product_hash
  end

  def createStoreHash(version_store)
    version_store_hash = Hash.new
    version_store_hash.store("store_id", version_store.store_id)
    return version_store_hash
  end

end
