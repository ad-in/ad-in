# require '../AdInServices/app/controllers/api/users_services_controller.rb'
class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy, :set_preference, :modify]
  skip_before_action :verify_authenticity_token
  def index
    @users = Api::UserServices.getAllUsers
  end

  def show
  end

  def new
    @user = User.new
    @allCategoriesHierarchy = ALL_CATEGORIES_IN_HASH
    @allBannersHierarchy = ALL_BANNERS_IN_HASH
  end

  def edit
    @allCategoriesHierarchy = ALL_CATEGORIES_IN_HASH
    @allBannersHierarchy = ALL_BANNERS_IN_HASH
  end
  def modify
    @allCategoriesHierarchy = ALL_CATEGORIES_IN_HASH
    @allBannersHierarchy = ALL_BANNERS_IN_HASH
    @user_categories = Api::UserServices.getAllUserCategories(@user.id)
    @user_locations = Api::UserServices.getAllUserLocations(@user.id)
  end

  def create
    @user = User.new(user_params)
    respond_to do |format|
      if Api::UserServices.addUser(@user, params)
        format.html { redirect_to users_path, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        @allCategoriesHierarchy = ALL_CATEGORIES_IN_HASH
        @allBannersHierarchy = ALL_BANNERS_IN_HASH
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if Api::UserServices.updateUser(@user, user_params, params)
        format.html { redirect_to users_path, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        @allCategoriesHierarchy = ALL_CATEGORIES_IN_HASH
        @allBannersHierarchy = ALL_BANNERS_IN_HASH
        @user_categories = Api::UserServices.getAllUserCategories(@user.id)
        @user_locations = Api::UserServices.getAllUserLocations(@user.id)
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    respond_to do |format|
      if Api::UserServices.removeUser(@user)
       format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
        format.json { head :no_content }
      end
    end
  end
  def set_preference
    user = User.find(params[:id])
    user.update_attributes(:user_preference => params[:preference])
    respond_to do |format|
      format.json { render json: user.user_preference, status: :ok}
      format.js
    end
  end  

  def get_location_hier
    location_trail = Api::LocationGroupServices.createLocationTrail(params[:location_id]) unless params[:location_id].blank?
    location_name = Api::LocationServices.getLocationNameFromLocationTrail(location_trail)
    respond_to do |format|
      format.json {render json: location_name, status: :ok}
      format.js
    end
  end
  def get_product_hier
    product_trail = Api::ProductGroupServices.getProductTrail(params[:product_id]) unless params[:product_id].blank?
    product_name = Api::ProductServices.getCategoryNameStringFromCategoryTrail(product_trail)
    respond_to do |format|
      format.json {render json: product_name, status: :ok}
      format.js
    end
  end

  def reset_password
    @user = User.find_by_password_reset_token!(params[:id])
    if @user.last_password_change_time < 2.hours.ago
      redirect_to root_url, :alert => "Password reset url has expired."
    elsif @user.update_attributes(params[:user])
      redirect_to root_url, :notice => "Password has been reset."
    else
      render :change_password, :notice => "something went wrong"
    end
  end
  def change_password
    @user = User.find_by_password_reset_token!(params[:id])
  end
  def forget_password
  end
   def send_password_reset
    user = User.find_by_email(params[:email])
    if user
      user.generate_token(:password_reset_token)
      user.update_attribute(:last_password_change_time, Time.zone.now)
      Email.recovery(user, user.email).deliver
      redirect_to root_url, :notice => "Email sent with password reset instructions."
    else
      redirect_to :back, :notice => "Your account not found"
    end
  end
 
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:username, :first_name, :last_name, :email, :parent_id, :department, :role, :last_login_time, :last_password_changed_time, :password, :password_confirmation, :status, :user_preference)
    end
end

