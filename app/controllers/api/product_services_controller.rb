class Api::ProductServicesController < ApplicationController
  
  def getAllDepartments
    @departments = Api::ProductServices.getAllDepartments
    render :json=> @departments
  end

  def getCategoriesByDepartment
    @departments = Api::ProductServices.getCategoriesByDepartment(params[:dept_desc])
    render :json=> @departments
  end

  def getSubCategoriesForCategory
    @categories = Api::ProductServices.getAllSubCategoriesForCategory(params[:cat_desc])
    render :json=> @categories
  end

  def getSegmentsForSubCategory
    @departments = Api::ProductServices.getSegmentsForSubCategory(params[:scat_desc])
    render :json=> @departments
  end

  def getProductsByDepartment
    @departments = Api::ProductServices.getProductsByDepartment(params[:dept_desc])
    render :json=> @departments
  end

  def getProductsByCategory
    @departments = Api::ProductServices.getProductsByCategory(params[:category])
    render :json=> @departments
  end

  def getProductsBySubCategory
    @departments = Api::ProductServices.getProductsBySubCategory(params[:scat_desc])
    render :json=> @departments
  end

  def getProductsBySegment
    @departments = Api::ProductServices.getProductsBySegment(params[:seg_desc])
    render :json=> @departments
  end
  
  def getProductsByCustomAttributes
    @departments = Api::ProductServices.getProductsByCustomAttributes((params[:custom_attr_ids]))
    render :json=> @departments
  end
  
  def getAllattributes
    logger.debug {" finally in ids #{params[:ids]}"}
    @departments = Api::ProductServices.getDistinctProductAttributesForCategories((params[:ids]))
    logger.debug {" finally departments #{@departments}"}
    render :json=> @departments
  end
end
 