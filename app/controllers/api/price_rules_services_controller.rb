class Api::PriceRulesServicesController < ApplicationController
   before_action :set_price_rule, only: [ :updatePriceRule, :removePriceRule]
  
  def addPriceRule
    @price_rule = PriceRule.new(price_rule_params)
    price_rule = Api::PriceRulesServices.addPriceRule(@price_rule)
    render :json=> price_rule
  end

  def updatePriceRule
    price_rule = Api::PriceRulesServices.updatePriceRule(@price_rule)
    render :json=> price_rule
  end

  def removePriceRule
    price_rule = Api::PriceRulesServices.removePriceRule(@price_rule)
    render :json=> price_rule
  end

  def getAllPriceRulesForCategory
    @price_rule = Api::PriceRulesServices.getAllPriceRulesForCategory(params[:category])
    render :json=> @price_rule
  end

  def getAllPriceRuleForProductIdAndLocationId
    price_rule = 
    rendor :json=> price_rule
  end

  def getAllPriceRulesForLocationsAndAttributes
     price_rule = 
    rendor :json=> price_rule
  end
  
  def getAllPriceRulesForCategoriesAndAttributes
    price_rule = 
    rendor :json=> price_rule
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_price_rule
      @price_rule = PriceRule.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def price_rule_params
      params.require(:price_rule).permit(:name, :description, :marketing_vehicle, :min_margin, :max_margin, :min_price_change, :max_price_change, :min_passthrough, :max_passthrough, :optimization_goal )
    end
end
