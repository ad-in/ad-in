class Api::ContractServicesController < ApplicationController
  def addContract
  end

  def modifyContract
  end

  def sendForOptimize
  end

  def approveContract
  end

  def rejectContract
  end

  def getAllContracts
  end
end
