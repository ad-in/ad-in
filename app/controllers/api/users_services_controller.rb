class Api::UsersServicesController < ApplicationController
   before_action :set_user, only: [:show, :edit, :add_user, :updateUser, :removeUser]
  
  def addUser
    @user = User.new(user_params)
    user = Api::UserServices.addUser(@user)
    render :json=> user
  end

  def updateUser
    user = Api::UserServices.updateUser(@user, user_params)
    render :json=> user
  end

  def removeUser
    user = Api::UserServices.removeUser(@user)
    render :json=> user
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
     def user_params
      params.require(:user).permit(:username, :first_name, :last_name, :email, :parent_id, :department, :role, :last_login_time, :last_password_changed_time, :password_digest, :salt, :status, :user_preference)
    end
end
