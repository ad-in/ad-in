class GeoGroupsController < ApplicationController
  before_action :set_geo_group, only: [:show, :edit, :update, :destroy]

  # GET /geo_groups
  # GET /geo_groups.json
  def index
    @geo_groups = GeoGroup.all
  end

  # GET /geo_groups/1
  # GET /geo_groups/1.json
  def show
  end

  # GET /geo_groups/new
  def new
    @geo_group = GeoGroup.new
  end

  # GET /geo_groups/1/edit
  def edit
  end

  # POST /geo_groups
  # POST /geo_groups.json
  def addGeoGroup
    @geo_group = GeoGroup.new(geo_group_params)
     geo_group= Api::GeoGroupServices.addGeoGroup(@geo_group)
    render :json=> geo_group
  end

  # PATCH/PUT /geo_groups/1
  # PATCH/PUT /geo_groups/1.json
  def updateGeoGroup
     geo_group = Api::GeoGroupServices.updateGeoGroup(@geo_group)
     render :json=> geo_group
  end

  # DELETE /geo_groups/1
  # DELETE /geo_groups/1.json
  def removeGeoGroup
    @geo_group.destroy
    geo_group = Api::EventManagementServices.removeEvent(@event_management)
    render :json=> geo_group
    
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_geo_group
      @geo_group = GeoGroup.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def geo_group_params
      params.require(:geo_group).permit(:id,:name, :description, :geo_trail, :custom_attribute, :lid)
    end
end
