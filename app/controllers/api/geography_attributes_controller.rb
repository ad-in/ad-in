class GeographyAttributesController < ApplicationController
  before_action :set_geography_attribute, only: [:show, :edit, :update, :destroy]

  # GET /geography_attributes
  # GET /geography_attributes.json
  def index
    @geography_attributes = GeographyAttribute.all
  end

  # GET /geography_attributes/1
  # GET /geography_attributes/1.json
  def show
  end

  # GET /geography_attributes/new
  def new
    @geography_attribute = GeographyAttribute.new
  end

  # GET /geography_attributes/1/edit
  def edit
  end

  # POST /geography_attributes
  # POST /geography_attributes.json
  def create
    @geography_attribute = GeographyAttribute.new(geography_attribute_params)

    respond_to do |format|
      if @geography_attribute.save
        format.html { redirect_to @geography_attribute, notice: 'Geography attribute was successfully created.' }
        format.json { render :show, status: :created, location: @geography_attribute }
      else
        format.html { render :new }
        format.json { render json: @geography_attribute.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /geography_attributes/1
  # PATCH/PUT /geography_attributes/1.json
  def update
    respond_to do |format|
      if @geography_attribute.update(geography_attribute_params)
        format.html { redirect_to @geography_attribute, notice: 'Geography attribute was successfully updated.' }
        format.json { render :show, status: :ok, location: @geography_attribute }
      else
        format.html { render :edit }
        format.json { render json: @geography_attribute.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /geography_attributes/1
  # DELETE /geography_attributes/1.json
  def destroy
    @geography_attribute.destroy
    respond_to do |format|
      format.html { redirect_to geography_attributes_url, notice: 'Geography attribute was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_geography_attribute
      @geography_attribute = GeographyAttribute.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def geography_attribute_params
      params.require(:geography_attribute).permit(:geography_id, :custom_attr_id, :value)
    end
end
