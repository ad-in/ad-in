class Api::PriceGridsServicesController < ApplicationController
  before_action :set_price_grid, only: [:updatePriceGrid , ]
  
  def addPriceGrid
    @price_grid = PriceGrid.new(price_grid_params)
    price_grid = Api::PriceGridsServices.addPriceGrid(@price_grid)
    render :json=> price_rule
  end

  def updatePriceGrid
    price_grid = Api::PriceGridsServices.updatePriceGrid(@price_grid)
    render :json=> price_rule
  end

  def removePriceGrid
    
  end

  def getPriceGridByCategories
    @price_grid = Api::PriceGridsServices.getAllPriceGridForCategory(params[:category])
    render :json=> @price_grid
  end

  def getPriceGridByLocation
    @price_grid = Api::PriceGridsServices.getAllPriceGridsForLocation(params[:location])
    render :json=> @price_grid
  end

  def getPriceGridByProductIdAndLocationId
    @price_grid = Api::PriceGridsServices.getPriceGridByProductIdAndLocationId(params[:category],params[:location])
    render :json=> @price_grid
  end
  
  
 private
    # Use callbacks to share common setup or constraints between actions.
    def set_price_grid
      @price_grid = PriceGrid.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def price_grid_params
      params.require(:price_grid).permit(:name, :description, :price_vehicle_id, :price_rule_id, :from_price, :to_price, :price_candidates, :multiples, :created_by, :updated_by)
    end
end
