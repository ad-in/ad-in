class EventManagementsController < ApplicationController
  before_action :set_event_management, only: [:show, :edit, :addEvent, :removeEvent, :updateEvent]
  # GET /event_managements
  # GET /event_managements.json
  def index
    @event_managements = EventManagement.all
  end

  # GET /event_managements/1
  # GET /event_managements/1.json
  def show
  end

  # GET /event_managements/new
  def new
    @event_management = EventManagement.new
  end

  # GET /event_managements/1/edit
  def edit
  end

  # POST /event_managements
  # POST /event_managements.json
  def addEvent
    @event_management = EventManagement.new(event_management_params)
    event_management= Api::EventManagementServices.addEvent(@event_management)
    render :json=> event_management
  end

  # PATCH/PUT /event_managements/1
  # PATCH/PUT /event_managements/1.json
  def updateEvent
    event_management = Api::EventManagementServices.updateEvent(@event_management)
    render :json=> event_management
  end

  # DELETE /event_managements/1
  # DELETE /event_managements/1.json
  def removeEvent
    event_management = Api::EventManagementServices.removeEvent(@event_management)
    render :json=> event_management
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_event_management
    @event_management = EventManagement.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def event_management_params
    params.require(:event_management).permit(:name, :description, :start_date, :end_date, :created_by, :product_scope_trail, :location_scope_trail)
  end
end
