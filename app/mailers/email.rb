class Email < ActionMailer::Base
  default from: "ravindra.thakur@kanakaconsulting.in"
  
  def recovery(user, email)
    @user = user
    @email = email
    mail(:to => email, :subject => "Password reset")
  end
end
