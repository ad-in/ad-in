$(document).ready(function() 
    {
		
		/* Common for all Modals */				
			$( " #worklistResetFilterDialog, #worklistCustomizeTable" ).dialog({
						autoOpen: false,
						width: "auto",
						height: "auto"
			});
			
			
			
			$(".resetAll").click(function(e) {
    
				$( "#worklistResetFilterDialog" ).dialog("option", "position", {
					my: "left",
					at: "left",
					of: e,
					offset: "5 70"
				  });
				
				$( "#worklistResetFilterDialog" ).dialog( "open");
				var offset = $(".resetAll").offset();
							  
			  	$( "#worklistResetFilterDialog" ).parent().css("top", offset.top-30+"px");
				

			  });	
			  
			  
			   $(".worklistCustTable").click(function(e) {
    
				$( "#worklistCustomizeTable" ).dialog("option", "position", {
					my: "left",
					at: "left",
					of: e,
					offset: "5 70"
				  });
				
				$( "#worklistCustomizeTable" ).dialog( "open").width("600px");
				var offset = $(this).offset();
							  
			  	$( "#worklistCustomizeTable").parent().css("left", offset.left-500+"px");
				

			  });
			  
	
	});
	