  $(document).ready(function(){
    $('.attrcheck').click(function() {
      var loc_id = this.id
      var loc_text = this.value 
      fetchLocationAttr(loc_id, loc_text)      
   });
});

function disableList() {
	$('#vendorLi').prop('disabled', true);
	$('#vendorA').prop('disabled', true);
	$("#vendorA").css("background", "#dddddd");
	$('#versionLi').prop('disabled', true);
	$('#versionA').prop('disabled', true);
	$("#versionA").css("background", "#dddddd");
	$('#marketingLi').prop('disabled', true);
	$('#marketingA').prop('disabled', true);
	$("#marketingA").css("background", "#dddddd");
	$('#pricingLi').prop('disabled', true);
	$('#pricingA').prop('disabled', true);	
	$("#pricingA").css("background", "#dddddd");
}
	
function enableList() {
	$("#dateNext").attr("href","#vendor");
	/*
	$('#dates').hide();
		$('#dates').removeProp("display");
		$('#vendor').show();*/
	
	$('#vendorLi').prop('disabled', false);
	$('#vendorA').attr('href', "#vendor");
	$('#vendorA').prop('disabled', false);	
	$("#vendorA").css("background", "");
	$('#versionLi').prop('disabled', false);
	$('#versionA').attr('href', "#version");
	$('#versionA').prop('disabled', false);
	$("#versionA").css("background", "");
	$('#marketingLi').prop('disabled', false);
	$('#marketingA').attr('href', "#marketing");
	$('#marketingA').prop('disabled', false);
	$("#marketingA").css("background", "");
	$('#pricingLi').prop('disabled', false);
	$('#pricingA').attr('href', "#pricing");
	$('#pricingA').prop('disabled', false);	
	$("#pricingA").css("background", "");	
}


 $(document).ready(function() {
 	
 	if($('#productsOk').hasClass('glyphicon-ok') && $('#locationsOk').hasClass('glyphicon-ok') && $('#datesOk').hasClass('glyphicon-ok'))
	{
		enableList();
	} else {
		disableList();
	}	
 	
    // function to store version store on location tab by "Ad to contract" btn
  $("#addLoc").click(function(){
    // alert("hi");
    var locationIds = $("input:checkbox:checked", $("#locationTable").dataTable().fnGetNodes()).map(function(){
    return ($(this).val());
   }).get();

  if(locationIds==null || locationIds==''){
    alert("Please select store first");
  }else{
   // alert(locationIds);
   $("#hiddenDiv").load("storeLocations",{locationIds: locationIds},function(rt){
    $("#locationsOk").addClass("glyphicon-ok");
    if($('#productsOk').hasClass('glyphicon-ok') && $('#locationsOk').hasClass('glyphicon-ok') && $('#datesOk').hasClass('glyphicon-ok'))
    {
      enableList();
    }
    var jsonString = JSON.parse(rt);
       var temp = "";
        $.each(jsonString, function(key, value) { 
         
      temp += '<tr><td><input type="checkbox" id="'+value.store_id+'" class="checkbox1"></td><td>'+value.description+'</td><td>'+value.store_pos_id+'</td></tr>';
         });
    $("#allLocationForContact").html(temp);
   });
   $('.addLocContract').show();
   }

   });

  // function to delete selected stores from contract
  $("#removeStore").click(function(){
    var storeIds = $("input:checkbox:checked", $("#selStoreTable").dataTable().fnGetNodes()).map(function(){
      return ($(this).attr("id"));
    }).get();
    if(storeIds==null || storeIds==''){
        alert("Please select store a first");
      }else{
       // alert(storeIds);
       $("#hiddenDiv").load("removeStore",{storeIds: storeIds},function(rt){
        var jsonString = JSON.parse(rt);
           var temp = "";
            $.each(jsonString, function(key, value) { 
          temp += '<tr><td><input type="checkbox" id="'+value.store_id+'" class="checkbox1"></td><td>'+value.description+'</td><td>'+value.store_pos_id+'</td></tr>';
             });
       $("#allLocationForContact").html(temp);
       });
       $('.addLocContract').show();
       }
  });

    // function to store version store by location group tab
   $("#addLGrouptoContracts").click(function(){
     var groupIds = $("#locationGrpTable input:checkbox:checked").map(function(){
            return ($(this).attr("id"));
        }).get();
     if(groupIds==null || groupIds==''){
        alert("Please select location group first");
      }else{
       // alert(groupIds);
       $("#hiddenDiv").load("storeLocationGroups",{groupIds: groupIds},function(rt){
        $("#locationsOk").addClass("glyphicon-ok");
        if($('#productsOk').hasClass('glyphicon-ok') && $('#locationsOk').hasClass('glyphicon-ok') && $('#datesOk').hasClass('glyphicon-ok'))
        {
          enableList();
        }
        var jsonString = JSON.parse(rt);
           var temp = "";
            $.each(jsonString, function(key, value) { 
          temp += '<tr><td><input type="checkbox" id="'+value.id+'" class="checkbox1"></td><td>'+value.name+'</td><td>'+value.description+'</td></tr>';
             });
        $("#allGroupForContract").html(temp);
       });
       $('.addLocGrpContract').show();
       }
  });

// function to add location group from dialog overlay
  $("#addlocGrpOverlay").click(function(){
    var storeIds = $("input:checkbox:checked", $("#locationTable").dataTable().fnGetNodes()).map(function(){
    return ($(this).val());
   }).get();
    var name = document.getElementById("group_name").value;
    var description = document.getElementById("group_description").value;
    var loc_id = document.getElementById("location_id").value;
    var cstmatrId = document.getElementById("custom_attr_id").value;
    var cstmatrVal = document.getElementById("custom_attr_val").value;
  
  if(name==null || name==''){
        alert("Please enter group name");
      }else{
    $("#hiddenDiv").load("add_location_group",{
    name: name,
    description: description,
    location_id : loc_id,
    custom_attr_id : cstmatrId,
    custom_attr_val : cstmatrVal,
    storeIdArray : storeIds
  },function(rt){
       $("#locationsOk").addClass("glyphicon-ok");
      if($('#productsOk').hasClass('glyphicon-ok') && $('#locationsOk').hasClass('glyphicon-ok') && $('#datesOk').hasClass('glyphicon-ok'))
      {
        enableList();
      }
  });
    $("#addLocationDialog").dialog("close")
    $('.addLocContract').show();
  }
  });  
 
  
  // function to add product group from dialog overlay
  $("#addlocGrpOverlay").click(function(){
    var name = document.getElementById("group_name").value;
    var loc_id = document.getElementById("location_id").value;
    var cstmatrId = document.getElementById("custom_attr_id").value;
    var cstmatrVal = document.getElementById("custom_attr_val").value;
  
  if(name==null || name==''){
        alert("Please enter group name");
      }else{
    $("#hiddenDiv").load("add_location_group",{
    name: name,
    location_id : loc_id,
    custom_attr_id : cstmatrId,
    custom_attr_val : cstmatrVal
  },function(rt){
    // alert("Hi, you are on right way...loc_id="+ location_id + "and cstmatrId =" + custom_attr_id + "and cstmatrVal=" + custom_attr_val );
  });
    $("#addLocationDialog").dialog("close");
    $('.addLocContract').show();
  }
  });  
  
  $("#submit_button").click(function(){
		$("#vendorOk").addClass("glyphicon-ok");
		/*$('#vendor').hide();*/
		
	});
	
	$("#addversion").click(function(){
		$("#versionOk").addClass("glyphicon-ok");
	});
	
	$("#savePricingVehicle").click(function(){
		$("#pricingOk").addClass("glyphicon-ok");
		
			$(".buyProduct option").prop("selected",true);
  			$(".getProduct option").prop("selected",true);
  			$("#savePricingVehicleSumbmit").click();
	});
  
  $("#addProdGrouptoContract").click(function(){
     var groupIds = $("#productGroupdataTable input:checkbox:checked").map(function(){
            return ($(this).attr("id"));
        }).get();
     if(groupIds==null || groupIds==''){
        alert("Please select Product group first");
      }else{
       //alert(groupIds);
       $("#hiddenDiv").load("storeProductGroups",{groupIds: groupIds},function(rt){
       	$("#productsOk").addClass("glyphicon-ok");
       	if($('#productsOk').hasClass('glyphicon-ok') && $('#locationsOk').hasClass('glyphicon-ok') && $('#datesOk').hasClass('glyphicon-ok'))
		{
			enableList();
		}
        var jsonString = JSON.parse(rt);
           var temp = "";
            $.each(jsonString, function(key, value) { 
          temp += '<tr><td><input type="checkbox" id="'+value.id+'" class="checkbox1"></td><td>'+value.name+'</td><td>'+value.description+'</td></tr>';
             });
        $("#productGroupsData").html(temp);
       });
      //
       }
  });
  
 // function to add selected location category in user form
  $("#locationId").click(function(){
    var locationId = $(this).attr('value');
    if(locationId == null || locationId==''){
      alert('Please select location first');
    }else{
       // alert("location id is"+ + locationId);
      $("#hiddenDiv").load("get_location_hier",{location_id: locationId},function(rt){
        var jsonString = rt;
           var temp = "";
          temp += '<tr><td><input type="checkbox" name="selectedLocHierArray[]" class="checkbox2" id="'+locationId+'" value="'+locationId+'"></td><td>'+rt+'</td><td><div class="form-group"><label class="radio-inline"><input type="radio" name="locRadio" value="option1"> Read Only</label><label class="radio-inline"><input type="radio" name="locRadio" id="inlineRadio2" value="option2"> Full Access</label></div></td></tr>';
            
        $("#locationHierselected").append(temp);
       });
    }
  });
  
  // function to add selected Product category in user form
  $("#categoryId").click(function(){
    var categoryId = $(this).attr('value');
    if(categoryId == null || categoryId==''){
      alert('Please select category first');
    }else{
       // alert("category id is"+ + categoryId);
      $("#hiddenDiv").load("get_product_hier",{product_id: categoryId},function(rt){
        var jsonString = rt;
           var temp = "";
          temp += '<tr><td><input type="checkbox" name="selectedCateHierArray[]" class="checkbox1" id="'+categoryId+'" value="'+categoryId+'"></td><td>'+rt+'</td><td><div class="form-group"><label class="radio-inline"><input type="radio" name="locRadio" value="option1"> Read Only</label><label class="radio-inline"><input type="radio" name="locRadio" id="inlineRadio2" value="option2"> Full Access</label></div></td></tr>';
            
        $("#productHierselected").append(temp);
       });
    }
  });
 });


function getProductsForId(id) {
     // alert("hello"+ id);
     
     var temp = $(this).parentsUntil( "ul #productHeirarchyTree" ).html();
     console.log(temp);
     
      $("#hiddenDiv").load("getProducts",{category: id},function(rt){
       var jsonString = JSON.parse(rt);
       var temp = "";
        $("#productdataTable").dataTable().fnClearTable();
      $("#productdataTable").dataTable().fnDestroy();
     $("#product_id").val(id);
    $('#productdataTable').DataTable( {
     //bStateSave : true,  
     data:jsonString,
     "columns": [
               {"data" : "product_id"},
                { "data": "upc_desc" },
                { "data": "item_desc" }
            ],
            "fnCreatedRow": function( nRow, jsonString, iDataIndex ) {
                                 $('td:eq(0)', nRow).html( '<input type="checkbox" name="productIdArray[]" class="checkbox" value="'+ jsonString.product_id+'" id="'+ jsonString.product_id+'">');

                         } ,
     paging:  true,
     searching: false,
     info:false  
    } );
     });  
 
     }
     

$(document).ready(function() {
	
	$("#selectAll").toggle(function () {
		
       $("input:checkbox", $("#productdataTable").dataTable().fnGetNodes()).attr("checked", true); }
     , function () {
     	
        $("input:checkbox", $("#productdataTable").dataTable().fnGetNodes()).attr("checked", false);
     }
 	); 
	
	
	$("#addProdToContractButton").click(function(){
		var productIds = $("input:checkbox:checked", $("#productdataTable").dataTable().fnGetNodes()).map(function(){
    return ($(this).val());
   }).get();
   //alert(productIds);
   $("#hiddenDiv").load("storeProducts",{productIds: productIds},function(rt){
   	$("#productsOk").addClass("glyphicon-ok");
   	if($('#productsOk').hasClass('glyphicon-ok') && $('#locationsOk').hasClass('glyphicon-ok') && $('#datesOk').hasClass('glyphicon-ok'))
		{
			enableList();
		}
   	//alert(rt);
   	 var jsonString = JSON.parse(rt);
       var temp = "";
        $.each(jsonString, function(key, value) { 
         
      temp += '<tr><td><input type="checkbox" id="'+value.product_id+'" class="checkbox1"></td><td>'+value.upc_desc+'</td><td>'+value.item_desc+'</td></tr>';
         });
    $("#allProductForContact").html(temp);
  });
  });
  
  
  $("#nextButtonForMarketingVehicle").click(function(){
  	//alert($("#selectMarketingVehicle").val());
  	$("#hiddenDiv").load("saveMarketingVehicle",{mkVehicleId: $("#selectMarketingVehicle").val()},function(rt){
  		$("#marketingOk").addClass("glyphicon-ok");
   });
   
   $("#hiddenDiv").load("priceVehicleProducts",{hidden: "productsForPriceVehicle" },function(rt){ 
   	//alert(rt);
   	var jsonString = JSON.parse(rt);
       var temp = "";
       var count;
        $.each(jsonString, function(key, value) { 
	      	temp += '<tr><td>'+value.product_name+'</td><td>'+value.product_base_cost+'</td><td>'+value.product_base_price+'</td><td>'+value.product_unit_funding+'</td><td>'+value.product_net_cost+'</td><td> <input type="text" name="flatOptimizeOpt[][flatPrice]" class="form-control" value=""/> </td></tr>' ; 
    	    count = key;
         });
         //alert(temp);
         $("#priceVehicleTableFlatPriceBody").html(temp);
         
         temp = "";
         $.each(jsonString, function(key, value) { 
	      	temp += '<tr><td>'+value.product_name+'</td><td>'+value.product_base_cost+'</td><td>'+value.product_base_price+'</td><td>'+value.product_unit_funding+'</td><td>'+value.product_net_cost+'</td><td> <input type="text" name="percentOffOpt[][percentOff]" class="form-control" /> </td></tr>' ; 
    	 });
         $("#priceVehicleTablePercentOffBody").html(temp);
         
         temp = "";
         $.each(jsonString, function(key, value) { 
	      	temp += '<tr><td>'+value.product_name+'</td><td>'+value.product_base_cost+'</td><td>'+value.product_base_price+'</td><td>'+value.product_unit_funding+'</td><td>'+value.product_net_cost+'</td><td><input type="text" name="AmtOffOpt[][amtOff]" class="form-control" /></td></tr>' ; 
    	 });
         $("#priceVehicleTableAmountOffBody").html(temp);
         
         temp = "";
         $.each(jsonString, function(key, value) { 
	      	temp += '<tr><td>'+value.product_name+'</td><td>'+value.product_base_cost+'</td><td>'+value.product_base_price+'</td><td>'+value.product_unit_funding+'</td><td>'+value.product_net_cost+'</td><td> <input type="text" value="" class="form-control" name="custPricingOpt[][price]"> </td></tr>' ; 
    	 });
         $("#priceVehicleTableCustomPricingBody").html(temp);
         
         
         count = count+1;
         $("#product_count").val(count);
         
         var tempMultiple = "";
         $.each(jsonString, function(key, value) { 
         tempMultiple += '<tr><td>'+value.product_name+'</td><td>'+value.product_base_cost+'</td><td>'+value.product_base_price+'</td><td>'+value.product_unit_funding+'</td><td>'+value.product_net_cost + '</td><td><input type="text" name="priceMultipleOpt[][qty]" class="form-control" /><input type="text" name="priceMultipleOpt[][price]" class="form-control" /></td></tr>' ; 
         });
         
         $("#priceVehilcePriceMultipleBody").html(tempMultiple);
         //alert(tempMultiple);
         var tempProgressive = "";
         $.each(jsonString, function(key, value) { 

         tempProgressive += '<tr><td>'+value.product_name+'</td><td>'+value.product_base_cost+'</td><td>'+value.product_base_price+'</td><td>'+value.product_unit_funding+'</td><td>'+value.product_net_cost+'</td><td><div id="divContainer'+key+'" class="divContainer"><div class="qtyPriceDiv"><input type="text" class="form-control" name="progressiveOpt0'+key+'[][qty]" />$<input type="text" class="form-control" value="" name="progressiveOpt0'+key+'[][price]" /></div></div><span class="glyphicon glyphicon-plus-sign addQtyPrice"></span></td></tr>';
         });
         
         $("#priceVehicleProgressiveTableBody").html(tempProgressive);
         
         //alert(tempProgressive);
         var tempBOGO = "";
         $.each(jsonString, function(key, value) { 

         tempBOGO += '<tr><td>'+value.product_name+'</td><td>'+value.product_base_cost+'</td><td>'+value.product_base_price+'</td><td>'+value.product_unit_funding+'</td><td>'+value.product_net_cost+'</td></tr>' ; 
         });
         $("#priceVehicleBOGOTableBody").html(tempBOGO);
         
         var tempBOGA = "";
         $.each(jsonString, function(key, value) { 

         tempBOGA += '<option value="'+value.product+'">'+value.product_name+'</option>'; 
         });
         $("#allCol").html(tempBOGA);
         
         
        // alert(tempBOGA);
         
   });
  });
  
  $("#productNext").click(function(){
  	//alert($("#selectMarketingVehicle").val());
  	
   $("#hiddenDiv").load("priceVehicleProducts",{hidden: "productsForPriceVehicle" },function(rt){
   	//alert(rt);
   	var jsonString = JSON.parse(rt);
       var temp = "";
        $.each(jsonString, function(key, value) { 

      temp += '<tr><td>'+value.product_name+'</td><td>'+value.product_base_cost+'</td><td>'+value.product_base_price+'</td><td>'+value.product_unit_funding+'</td><td>'+value.product_net_cost+'</td><td><input type=\'text\' class=\"form-control\" /></td></tr>' ; 
         });
        // alert(temp);
         $("#priceVehicleTableFlatPriceBody").html(temp);
         $("#priceVehicleTablePercentOffBody").html(temp);
         $("#priceVehicleTableAmountOffBody").html(temp);
         $("#priceVehicleTableCustomPricingBody").html(temp);
         
         var tempMultiple = "";
         $.each(jsonString, function(key, value) { 
         tempMultiple += '<tr><td>'+value.product_name+'</td><td>'+value.product_base_cost+'</td><td>'+value.product_base_price+'</td><td>'+value.product_unit_funding+'</td><td>'+value.product_net_cost+'</td><td><input type="text" class="form-control" /><input type="text" class="form-control" /></td></tr>' ; 
         });
         
         $("#priceVehilcePriceMultipleBody").html(tempMultiple);
         //alert(tempMultiple);
         var tempProgressive = "";
         $.each(jsonString, function(key, value) { 

         tempProgressive += '<tr><td>'+value.product_name+'</td><td>'+value.product_base_cost+'</td><td>'+value.product_base_price+'</td><td>'+value.product_unit_funding+'</td><td>'+value.product_net_cost+'</td><td><div id="divContainer'+key+'" class="divContainer"><div class="qtyPriceDiv"><input type="text" class="form-control" /><input type="text" class="form-control" /></div></div><span class="glyphicon glyphicon-plus-sign addQtyPrice"></span></td></tr>';
         });
         
         $("#priceVehicleProgressiveTableBody").html(tempProgressive);
         
         //alert(tempProgressive);
         var tempBOGA = "";
         $.each(jsonString, function(key, value) { 

         tempBOGA += '<tr><td>'+value.product_name+'</td><td>'+value.product_base_cost+'</td><td>'+value.product_base_price+'</td><td>'+value.product_unit_funding+'</td><td>'+value.product_net_cost+'</td></tr>' ; 
         });
         $("#priceVehicleBOGOTableBody").html(tempProgressive);
         //alert(tempBOGA);
         
   });
  });
  
  
  
  
  // $("#showProductGroups").click(function(){
  	// alert("in product groups");
  	// $("#hiddenDiv").load("getProductsGroupsHash",{hidden: "showProductGroups"},function(rt){
  		// console.log(rt);
  		// alert(rt);
   // });
  // });
  
  $("input[name='inlineRadioOptions']").change(function(){
  	var op = $(this).val();
  	if(op=="option1"){
  		$(".hideClass").css("display","none");
  	} else if(op=="option2") {
  		$(".hideClass").css("display","block");
  	}
  });
  
 });
     
function saveDate(id, dateValue) {
	var promotionDate = $("#reservation").val();
	$("#hiddenDiv").load("saveDate", {
		contract_id : id,
		promotionDate : promotionDate
	}, function(rt) {
		$("#datesOk").addClass("glyphicon-ok");
		if ($('#productsOk').hasClass('glyphicon-ok') && $('#locationsOk').hasClass('glyphicon-ok') && $('#datesOk').hasClass('glyphicon-ok')) {
			enableList();
		}
		var jsonString = JSON.parse(rt);

		var temp = "";
		$.each(jsonString, function(key, value) {

			temp += '<tr><td>' + value.product + '</td><td>' + value.product_name + '</td><td>' + value.product_desc +'</td>';
			temp += '<td> <input id="product_funding[][base_price]" name="product_funding[][base_price]" type="text" class="form-control vendor-product-form" value="'+value.base_price+'" style="display:none" >'+ value.base_price +'</td>';
       		temp += '<td> <input id="product_funding[][base_cost]" name="product_funding[][base_cost]" type="text" class="form-control vendor-product-form" value="'+value.base_cost+'" style="display:none" >'+ value.base_cost +'</td>';
            temp += '<td> <input id="product_funding[][unit_funding]" name="product_funding[][unit_funding]" type="text" class="form-control vendor-product-form" value="'+value.unit_funding+'" ></td>';
			temp += '<td> <input id="product_funding[][cost]" name="product_funding[][cost]" type="text" class="form-control vendor-product-form" value="'+value.tier_products+'"></td></tr>';

		});
		$("#tier_product_table").html(temp);

		$('#vender_funding_dates').val(promotionDate);
		$('#reservationPriceVehicleDate').val(promotionDate);

	});

}

     
function addNote(id) {
	//alert("finally"+ id);
	 $("#hiddenDiv").load("addNote",{contract_id: id , user_id: 1, description: $("#addNote").val()},function(rt){
     });   
     var lastNote = $("#addNote").val();
     alert(lastNote);
     lastNote = lastNote +" | " + new Date();
     
     alert(lastNote);
     $("label[for='lastNote']").html(lastNote);
     $("#addNote").val(""); 
     }
     

function getAllNote(id) {
	//alert("finally"+ id);
		$("#hiddenDiv").load("getAllNote",{contract_id: id },function(rt){
		var jsonString = JSON.parse(rt);
		console.log(jsonString);
        var temp = "";
        $.each(jsonString, function(key, value) { 
      	temp += '<tr><td>'+value.description+'</td><td>'+value.user_id+'</td><td>'+value.updated_at+'</td></tr>';
         });
			$("#userNotesTable").html(temp);
     });    
     }
	 
	 function fetchBanners(id) {
	$("#hiddenDiv").load("getStores",{banner: id},function(rt){
       var jsonString = JSON.parse(rt);
       var temp = "";
        $("#locationTable").dataTable().fnClearTable();
        $("#locationTable").dataTable().fnDestroy();
        $("#location_id").val(id);
    $('#locationTable').DataTable( {
     //bStateSave : true,  
     data:jsonString,
     "columns": [
               	{"data" : "store_id"},
                { "data": "description" },
                { "data": "store_pos_id" }
            ],
            "fnCreatedRow": function( nRow, jsonString, iDataIndex ) {
                                $('td:eq(0)', nRow).html( '<input type="checkbox" name="storeIdArray[]" class="checkbox" value="'+ jsonString.store_id+'" id="'+ jsonString.store_id+'">');

                         } ,
     paging:  true,
     searching: false,
     info:false  
    } );
     });  
 
     }

    function fetchLocationAttr(id, text) {
	$("#hiddenDiv").load("getStoresByCustAttr",{custattrid: id, custattrval: text},function(rt){
               // alert("hello"+ id);
       var jsonString = JSON.parse(rt);
       var temp = "";
        $("#locationTable").dataTable().fnClearTable();
        $("#locationTable").dataTable().fnDestroy();
        $("#custom_attr_id").val(id);
        $("#custom_attr_val").val(text);
    $('#locationTable').DataTable( {
     //bStateSave : true,  
     data:jsonString,
     "columns": [
               		{"data" : "store_id"},
                { "data": "description" },
                { "data": "store_pos_id" }
            ],
            "fnCreatedRow": function( nRow, jsonString, iDataIndex ) {
                                $('td:eq(0)', nRow).html( '<input type="checkbox" name="checkbox" class="checkbox" value="'+ jsonString.store_id+'" id="'+ jsonString.store_id+'">');

                         } ,
     paging:  true,
     searching: false,
     info:false  
    } );
     });  
 
     } 

	 // function to be used in worklist filter
     function setlocationId(id){
      // alert("on the way for location id "+ id );
        document.getElementById('locationId').value = id ;
        return true;
     }
   
   function setcategoryId(id){
      // alert("on the way for category id "+ id );
        document.getElementById('categoryId').value = id ;
        return true;
     }
     
$(document).ready(function() {
	$(".createTierVendor").click(function() {
		//alert("hii");
		$("#createTierVendorSubmitButton").click();
		version_id = $(this).val();
		$("#hiddenDiv").load("createTierForVendor", {version_id:version_id}, function(rt) {
			var data = rt.split("----------");
			var squNo = data[0];
			

			//$("#ulForVendorTier li").removeClass("active");
			$("#ulForVendorTier").append(data[2]);
			
			//$("#venderTierAdd div").removeClass("active");
			$("#venderTierAdd").append(data[1]);

			$(".tab-content").bind();

			//$("#venderTierAdd div").removeClass("active");
			$("#pricingTierAdd").append(data[3]);

			//$("#ulForVendorTier li").removeClass("active");
			$("#ulForPricingTier").append(data[4]);
	//$("#venderTierAdd").bind();
			$("#vender_funding_dates").live('focus', function() {
				$(this).daterangepicker();
			});
			
			$('.priceSelector').change(function() {
				 var id =	$('#'+$(this).val()).parent().parent().attr("id");
				$('#'+id+' .priceVehicleGroup').hide();
				$('#' + $(this).val()).show();
			});

		});
	});
	
	
	$(".breadcrumbCategory").click(function(){
		id = $(this).attr("id");
		$("#hiddenDiv").load("/contract/breadcrumbCategory",{id:id},function(rt){
			$(".breadcrumbCategoryDiv").html(rt);
		});
	});
	
	$(".breadcrumbBanner").click(function(){
		id = $(this).attr("id");
		$("#hiddenDiv").load("/contract/breadcrumbBanner",{id:id},function(rt){
			$(".breadcrumbBannerDiv").html(rt);
		});
	});

});

// For Event_Managements Screen
$(document).ready(function() { 
	
	$(".eventMngtCategories").click(function() {
		$("#selectedCatId").val($(this).attr("id"));
	});
	
	$(".eventMngtBanner").click(function() {
		$("#selectedBannerId").val($(this).attr("id"));
	});
	
});   

// Version 

$(document).ready(function() { 
$("input[name='inlineRadioOptionsVersion']").change(function(){
  	var op = $(this).val();
  	if(op=="option1"){
  		$(".hideClass1").css("display","block");
  	} else if(op=="option2") {
  		$(".hideClass1").css("display","none");
  	}
  });
  
  $("#addversion").click(function(){
  	alert("in the create version ");
  	alert($("#selectedVersionAttribute").val());
  	
   $("#hiddenDiv").load("createVersion",{versionBy: $("#selectedVersionAttribute").val() },function(rt){
   	$("#marketingVehiclesDiv").html(rt);
   });
  });
  
  	
});   