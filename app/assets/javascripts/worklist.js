$(document).ready(function() 
    { 
        $("#birdEyeViewdataTable").tablesorter({sortList: [[7,1]]}); 
	
		
		$('#productFilterTree').sapling();
		$('#locationFilterTree').sapling();
		$('#productAttrFilterTree').sapling();
		$('#locationAttrFilterTree').sapling();
		
		
		$('.treeLastChild,.sapling-item').click(function() {
		      $('.treeLastChild,.sapling-item').removeClass('selectedFilter');
			  $(this).addClass('selectedFilter');
			  
			  var topCat = $(".sapling-item");
		
			  if(!$(this).hasClass("sapling-expanded")){
				 topCat.removeClass("selectedFilter"); 
			  }
		});
		
		
		/* scrollbars */
		$('#overlayBody').enscroll({
			showOnHover: true,
			verticalTrackClass: 'track3',
			verticalHandleClass: 'handle3'
		});
		
    }); 
    
	$(document).ready(function() {
		var table = $('#worklistTable').DataTable( {
			scrollY:        "300px",
			scrollX:        true,
			scrollCollapse: true,
			paging:         false,
			searching: false,
			info:false		
		} );
		
		new $.fn.dataTable.FixedColumns( table, {
			leftColumns: 6
		} );
	} );

	function clearFilter(filterOpt)
	{
	  document.getElementById(filterOpt).style.display="none";
	}