require 'test_helper'

class Api::AdInServicesControllerControllerTest < ActionController::TestCase
  test "should get getDepartments" do
    get :getDepartments
    assert_response :success
  end

  test "should get getCategoriesByDepartment" do
    get :getCategoriesByDepartment
    assert_response :success
  end

end
