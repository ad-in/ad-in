

Rails.application.routes.draw do
  
  resources :notes

  get "contract_services/addContract"
  get "contract_services/modifyContract"
  get "contract_services/sendForOptimize"
  get "contract_services/approveContract"
  get "contract_services/rejectContract"
  get "contract_services/getAllContracts"
  post 'contract/create_tier'
  get 'contract/create_tier'
   get 'contract/newFromScratch'
  post 'contract/newFromScratch'

AdInServices::Application.routes.draw do
  

  resources :notes

  namespace :api do
  get 'price_rules_services/addPriceRule'
  end

  namespace :api do
  get 'price_rules_services/updatePriceRule'
  end

  namespace :api do
  get 'price_rules_services/removePriceRule'
  end

  namespace :api do
  get 'price_rules_services/getAllPriceRulesForCategory'
  end

  namespace :api do
  get 'price_rules_services/getAllPriceRuleForProductIdAndLocationId'
  end

  namespace :api do
  get 'price_rules_services/getAllPriceRulesForCategoriesAndAttributes'
  end

  namespace :api do
  get 'price_rules_services/getAllPriceRulesForLocationsAndAttributes'
  end

  namespace :api do
  get 'price_grids_services/addPriceGrid'
  end

  namespace :api do
  get 'price_grids_services/updatePriceGrid'
  end

  namespace :api do
  get 'price_grids_services/removePriceGrid'
  end

  namespace :api do
  get 'price_grids_services/getPriceGridByCategories'
  end

  namespace :api do
  get 'price_grids_services/getPriceGridByLocation'
  end

  namespace :api do
  get 'price_grids_services/getPriceGridByCategoryAndLocation'
  end

  namespace :api do
  get 'price_grids_services/getPriceGridByProductIdAndLocationId'
  end

  resources :price_grids

  resources :price_rules

  resources :product_attributes

  resources :custom_attributes
  
  get 'custom_attributes/index'
  post 'custom_attributes/add'
  get 'custom_attributes/new'
  
  namespace :api do
  get 'product_services/getAllDepartments'
  end

  namespace :api do
  get 'product_services/getCategoriesByDepartment'
  end

  namespace :api do
  get 'product_services/getSubCategoriesForCategory'
  end

  namespace :api do
  get 'product_services/getSegmentsForSubCategory'
  end

  namespace :api do
  get 'product_services/getProductsByDepartment'
  end

  namespace :api do
  post 'product_services/getProductsByCategory'
  end

  namespace :api do
  get 'product_services/getProductsBySubCategory'
  end

  namespace :api do
  get 'product_services/getProductsBySegment'
  end
  
  namespace :api do
  get 'product_services/getProductsByCustomAttributes'
 
  end
  
  namespace :api do
  get 'product_services/getAllattributes'
  end

  namespace :api do
  get 'ad_in_services_controller/getDepartments'
  
  get '/getDepartments' => 'ad_in_services_controller#getDepartments' 
  
  get 'ad_in_services_controller/getCategoriesByDepartment'
  end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end


 resources :event_managements
  
end
end